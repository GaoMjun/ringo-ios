//
//  BLEDriven.h
//  iOS-OpenCV-FaceRec
//
//  Created by qq on 30/7/2016.
//  Copyright © 2016 Fifteen Jugglers Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef void (^findDevicesBlock)(NSArray *devices);
typedef void (^failedBlock)(NSString *errorDescription);
typedef void (^successBlock)(CBPeripheral *peripheral);

@interface BLEDriven : NSObject

@property (nonatomic, strong) NSData *framePacket;

@property (copy, nonatomic) findDevicesBlock findDevices;
@property (copy, nonatomic) failedBlock failed;
@property (copy, nonatomic) successBlock success;

@property (assign, nonatomic) BOOL running;

+ (instancetype)sharedInstance;

- (void)scanWithFindDevices:(findDevicesBlock)findDevices failedHandler:(failedBlock)failed;

- (void)connectToDevice:(CBPeripheral *)peripheral success:(successBlock)success failedHandler:(failedBlock)failed;

- (void)disconnectToDevice:(CBPeripheral *)peripheral;

- (void)stopScan;

@end
