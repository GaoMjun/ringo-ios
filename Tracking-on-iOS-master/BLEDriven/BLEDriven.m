//
//  BLEDriven.m
//  iOS-OpenCV-FaceRec
//
//  Created by qq on 30/7/2016.
//  Copyright © 2016 Fifteen Jugglers Software. All rights reserved.
//

#import "BLEDriven.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "NSString+HexString.h"
#import "ReceiveFramePacket.h"
#import "SendFramePacket.h"

//#define CEEWA_SERVICE_UUID                  @"0000fff0-0000-1000-8000-00805f9b34fb"
//#define CEEWA_CHARACTERISTIC_UUID           @"0000fff1-0000-1000-8000-00805f9b34fb"

#define CEEWA_SERVICE_UUID                  @"0000ff00-0000-1000-8000-00805f9b34fb"
#define CEEWA_CHARACTERISTIC_UUID           @"0000ff01-0000-1000-8000-00805f9b34fb"

typedef NS_ENUM(NSUInteger, BleStatus) {
    BLEDISCONNETED,
    BLECONNECT,
    BLECONNECTED
};

@interface BLEDriven ()
<CBCentralManagerDelegate,
CBPeripheralDelegate>

@property (nonatomic, strong) CBCentralManager *mgr;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBService *service;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (nonatomic, strong) NSMutableArray *devices;

@property (nonatomic, assign) BOOL bleReady;

@property (nonatomic, strong) NSTimer *sendTimer;

@end

@implementation BLEDriven

+ (instancetype)sharedInstance {
    static BLEDriven *driven = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        driven = [[BLEDriven alloc] init];
    });
    
    return driven;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _mgr = [[CBCentralManager alloc] init];
        _mgr.delegate = self;
        _devices = [NSMutableArray array];

//        [self addObserver:self forKeyPath:@"framePacket" options:NSKeyValueObservingOptionNew context:nil];
//        [self addObserver:self forKeyPath:@"running" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    return self;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        self.devices = [NSMutableArray array];
        [self.mgr scanForPeripheralsWithServices:nil options:nil];
        
    } else {
#ifdef DEBUG
        NSLog(@"%s state:%ld", __func__, (long)central.state);
#endif
        if (self.failed) {
            self.failed(@"CBCentralManagerStatePoweredNotOn");
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if (peripheral.name.length > 0 && RSSI.intValue > -100) {
        
        if (![_devices containsObject:peripheral]) {
            [_devices addObject:peripheral];
        }
        
        if (self.findDevices) {
            
            self.findDevices(_devices);
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {

    [self.mgr stopScan];
    
    self.peripheral = peripheral;
    self.peripheral.delegate = self;
    [self.peripheral discoverServices:@[[CBUUID UUIDWithString:CEEWA_SERVICE_UUID]]];

}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BLEDisconnected" object:@"BLEDisconnected"];
    
    self.bleReady = NO;
    
    if (!error) {
        
        
    } else {
#ifdef DEBUG
        NSLog(@"%s error:%@", __func__, error);
#endif
    }
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    
    if (!error) {
        
        for (CBService *service in peripheral.services) {
            
            if ([service.UUID isEqual:[CBUUID UUIDWithString:CEEWA_SERVICE_UUID]]) {
                self.service = service;
                
                [self.peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:CEEWA_CHARACTERISTIC_UUID]] forService:self.service];
                
                break;
            }
        }
        
    } else {
#ifdef DEBUG
        NSLog(@"%s error:%@", __func__, error);
#endif
        if (self.failed) {
            
            self.failed(error.description);
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if (!error) {
        
        for (CBCharacteristic *characteristic in service.characteristics) {
            
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:CEEWA_CHARACTERISTIC_UUID]]) {
                self.characteristic = characteristic;
                
                if (self.success) {
                    self.success(peripheral);
                }
                
                [self.peripheral setNotifyValue:YES forCharacteristic:self.characteristic];
                
                _bleReady = YES;
                
                self.sendTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(sendData) userInfo:nil repeats:YES];
            }
        }
        
    } else {
#ifdef DEBUG
        NSLog(@"%s error:%@", __func__, error);
#endif
        if (self.failed) {
            
            self.failed(error.description);
        }
    }
}

//- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
//    if (!error) {
////        [self sendData];
//         NSLog(@"%@", characteristic.value);
//    } else {
//        
//        NSLog(@"%s error:%@", __func__, error);
//    }
//}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (!error) {
        if (characteristic.value.length) {
#ifdef DEBUG
            NSLog(@"%@", characteristic.value);
#endif
            [ReceiveFramePacket sharedInstance].receiveData = characteristic.value;
            
        }
        
    } else {
#ifdef DEBUG
        NSLog(@"%s error:%@", __func__, error);
#endif
    }
}

#pragma mark - observeValueForKeyPath

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    id newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"framePacket"]) {

       
        
//        NSLog(@"%@", self.framePacket);
//        if (self.bleReady) {
//            [self.peripheral writeValue:framePacket forCharacteristic:self.characteristic type:CBCharacteristicWriteWithResponse];
//        }
    } else if ([keyPath isEqualToString:@"running"]) {
        
        if ([newValue boolValue]) {
            if (!self.sendTimer && self.bleReady) {
                self.sendTimer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(sendData) userInfo:nil repeats:YES];
            }
            
        } else {
            
            if (self.sendTimer) {
                [self.sendTimer invalidate];
                self.sendTimer = nil;
            }
        }
    }
}

- (void)scanWithfinish:(void (^)(NSArray *devices))finish {
    NSArray *devices = nil;
    
    finish(devices);
}

- (void)sendData {
#ifdef DEBUG
    NSLog(@"%@", [SendFramePacket sharedInstance].framePacket);
#endif
    
//    if (self.framePacket) {
//        NSLog(@"%@", self.framePacket);
//
        [self.peripheral writeValue:[SendFramePacket sharedInstance].framePacket
                  forCharacteristic:self.characteristic
                               type:CBCharacteristicWriteWithoutResponse];
//    }
}

- (void)scanWithFindDevices:(findDevicesBlock)findDevices failedHandler:(failedBlock)failed{
    self.devices = [NSMutableArray array];
    [self.mgr scanForPeripheralsWithServices:nil options:nil];
    
    self.findDevices = findDevices;
    self.failed = failed;
}

- (void)connectToDevice:(CBPeripheral *)peripheral success:(successBlock)success failedHandler:(failedBlock)failed {

    // connect to this peripheral
    [self.mgr connectPeripheral:peripheral options:nil];
    
    self.success = success;
    self.failed = failed;
}

- (void)disconnectToDevice:(CBPeripheral *)peripheral {
    [self.sendTimer invalidate];
    self.sendTimer = nil;
    
    [self.mgr cancelPeripheralConnection:peripheral];
}

- (void)stopScan {
    [self.mgr stopScan];
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"framePacket"];
}

@end
