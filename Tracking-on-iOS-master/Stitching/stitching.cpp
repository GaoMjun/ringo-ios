//
//  stitching.cpp
//  GimbalTracking
//
//  Created by qq on 14/10/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#include "stitching.h"

#include "opencv2/stitching.hpp"

using namespace std;
using namespace cv;

cv::Mat stitch(vector<Mat> &images)
{
    Mat pano;
    
    vector<Mat> imgs = images;
    
    Stitcher stitcher = Stitcher::createDefault();
    Stitcher::Status status = stitcher.stitch(imgs, pano);
    
    if (status != Stitcher::OK) {
        cout << "Can't stitch images, error code = " << int(status) << endl;
    }
    
    return pano;
}
