//
//  stitching.hpp
//  GimbalTracking
//
//  Created by qq on 14/10/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#ifndef stitching_hpp
#define stitching_hpp

#include <iostream>

#include <opencv2/opencv.hpp>

cv::Mat stitch(std::vector<cv::Mat> &images);

#endif /* stitching_hpp */
