//
//  CameraSettingsDetailTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "CameraSettingsDetailTableViewModel.h"

@implementation CameraSettingsDetailTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    
    [self initKinds];
}

- (void)initKinds {
    _cameraSettingsKinds =     @{@"Video Resolution" : @{@"titles" : @[@"1280x720 30fps",
                                                                      @"1920x1080 30fps",
                                                                      @"1920x1080 60fps"],
                                                        @"images" : @[[UIImage imageNamed:@"advanced_more_videoformat_1280x720_30p"],
                                                                      [UIImage imageNamed:@"advanced_more_videoformat_1920x1080_30p"],
                                                                      [UIImage imageNamed:@"advanced_more_videoformat_1920x1080_60p"]]},
                                 
                                 @"White Balance" : @{@"titles" : @[@"Auto",
                                                                   @"Sunny",
                                                                   @"Cloudy",
                                                                   @"Incandescent",
                                                                   @"Fluorescent"],
                                                     @"images" : @[[UIImage imageNamed:@"advanced_more_whitebalance_auto"],
                                                                   [UIImage imageNamed:@"advanced_more_whitebalance_outdoor"],
                                                                   [UIImage imageNamed:@"advanced_more_whitebalance_inhouse"],
                                                                   [UIImage imageNamed:@"advanced_more_whitebalance_tungsten"],
                                                                   [UIImage imageNamed:@"advanced_more_whitebalance_neon"]]},
                                 
                                 @"Grid" : @{@"titles" : @[@"None",
                                                           @"Sudoku",
                                                           @"Diagonal",
                                                           @"Center"],
                                             @"images" : @[[UIImage imageNamed:@"advanced_more_none"],
                                                           [UIImage imageNamed:@"advanced_more_grid_sudoku"],
                                                           [UIImage imageNamed:@"advanced_more_grid_diagonal"],
                                                           [UIImage imageNamed:@"advanced_more_grid_center"]]},
                                 
                                 @"Flash" : @{@"titles" : @[@"OFF",
                                                            @"ON",
                                                            @"Auto",
                                                            @"Torch"],
                                              @"images" : @[[UIImage imageNamed:@"lp_camera_flash_off"],
                                                            [UIImage imageNamed:@"lp_camera_flash_on"],
                                                            [UIImage imageNamed:@"lp_camera_flash_auto"],
                                                            [UIImage imageNamed:@"lp_camera_flash_torch"]]}};
}

@end
