//
//  GimbalSettingsTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSettingsTableViewModel.h"

@implementation GimbalSettingsTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    _titles = @[@"Scene Mode",
                @"Pitch Lock"];
}

@end
