//
//  GimbalSettingsTableViewCell2.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSettingsTableViewCell2.h"

@implementation GimbalSettingsTableViewCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.switcher.transform = CGAffineTransformMakeScale(0.8, 0.8);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
