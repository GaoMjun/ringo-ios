//
//  GimbalSettingsTableViewCell.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSettingsTableViewCell.h"

@implementation GimbalSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    __weak typeof(self) weakSelf = self;
    self.stateChanged = ^(NSString *state) {
        weakSelf.state.text = state;
        
        if (weakSelf.gimbalSceneChangedBlock) {
            weakSelf.gimbalSceneChangedBlock(state);
        }
    };
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
