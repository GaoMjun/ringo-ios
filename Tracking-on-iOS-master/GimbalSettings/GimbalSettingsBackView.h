//
//  GimbalSettingsBackView.h
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GimbalSettingsBackView : UIView

@property (copy, nonatomic) void (^gimbalSceneChangedBlock)(NSString *scene);

@end
