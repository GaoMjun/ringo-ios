//
//  GimbalSettingsBackView.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSettingsBackView.h"
#import "GimbalSettingsTableViewModel.h"
#import "GimbalSettingsTableViewCell.h"
#import "GimbalSettingsTableViewCell2.h"
#import "GimbalSceneModesBackView.h"
#import "Masonry.h"

@interface GimbalSettingsBackView ()

@property (strong, nonatomic) GimbalSettingsTableViewModel *model;

@property (weak, nonatomic) IBOutlet UITableView *gimbalSettingsTableView;

@property (assign, nonatomic) BOOL opened;

@property (weak, nonatomic) IBOutlet GimbalSceneModesBackView *gimbalSceneModesBackView;

@end

@implementation GimbalSettingsBackView

- (GimbalSettingsTableViewModel *)model {
    if (!_model) {
        _model = [[GimbalSettingsTableViewModel alloc] init];
    }
    
    return _model;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gimbalSettingsTableView.rowHeight = UITableViewAutomaticDimension;
    self.gimbalSettingsTableView.estimatedRowHeight = 44.f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            GimbalSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
            cell.title.text = self.model.titles[indexPath.row];
            self.gimbalSceneModesBackView.stateChangedBlock = cell.stateChanged;
            cell.gimbalSceneChangedBlock = self.gimbalSceneChangedBlock;
            
            return cell;
        }
            
            break;
            
        case 1: {
            GimbalSettingsTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID2" forIndexPath:indexPath];
            cell.title.text = self.model.titles[indexPath.row];
            
            return cell;
        }
            
            break;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        
        GimbalSettingsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (self.opened) {
            self.opened = NO;
            
            cell.more.transform = CGAffineTransformMakeRotation(0);
            
            [self.gimbalSceneModesBackView removeFromSuperview];
            
            [cell.container mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(0);
                
                [tableView reloadData];
                
            }];
            
        } else {
            self.opened = YES;
            
            cell.more.transform = CGAffineTransformMakeRotation(M_PI_2);
            
            [cell.container addSubview:self.gimbalSceneModesBackView];
            [self.gimbalSceneModesBackView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.container).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
                
                [cell.container mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(44*self.gimbalSceneModesBackView.model.titles.count);
                    
                    [tableView reloadData];

                }];
                
            }];
        }
    }
}

@end
