//
//  GimbalSceneModesTableViewCell2.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSceneModesTableViewCell2.h"

@implementation GimbalSceneModesTableViewCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.switcher.transform = CGAffineTransformMakeScale(0.8, 0.8);
}

@end
