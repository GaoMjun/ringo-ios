//
//  GimbalSceneModesBackView.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSceneModesBackView.h"
#import "GimbalSceneModesTableViewCell.h"
#import "GimbalSceneModesTableViewCell2.h"

typedef NS_ENUM(NSUInteger, SCENEMODES) {
    SCENEMODES_WALK,
    SCENEMODES_SPORT,
    SCENEMODES_CUSTOM
};

@interface GimbalSceneModesBackView ()

@property (weak, nonatomic) IBOutlet UITableView *gimbalSceneModesTableView;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (assign, nonatomic) SCENEMODES mode;

@property (strong, nonatomic) UISwitch *switcher;

@end

@implementation GimbalSceneModesBackView

- (GimbalSceneModesTableViewModel *)model {
    if (!_model) {
        _model = [[GimbalSceneModesTableViewModel alloc] init];
    }
    
    return _model;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.gimbalSceneModesTableView.rowHeight = UITableViewAutomaticDimension;
    self.gimbalSceneModesTableView.estimatedRowHeight = 44.f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 1) {
        GimbalSceneModesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        cell.title.text = self.model.titles[indexPath.row];
        if (!self.selectedIndexPath && indexPath.row == 0) {
            cell.checkmark.hidden = NO;
            
            self.selectedIndexPath = indexPath;
        }
        
        if (self.selectedIndexPath && self.selectedIndexPath == indexPath) {
            cell.checkmark.hidden = NO;
        } else {
            cell.checkmark.hidden = YES;
        }
        
        return cell;
        
    } else if (indexPath.row == 2) {
        GimbalSceneModesTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID2" forIndexPath:indexPath];
        cell.title.text = self.model.titles[indexPath.row];
        self.switcher = cell.switcher;
        
        return cell;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 1) return;
    
    
    if (!self.selectedIndexPath) {
        self.selectedIndexPath = indexPath;
    }
    
    if (self.selectedIndexPath != indexPath && self.mode != SCENEMODES_CUSTOM) {
        GimbalSceneModesTableViewCell *cell = [tableView cellForRowAtIndexPath:self.selectedIndexPath];
        
        cell.checkmark.hidden = YES;
        
        cell = [tableView cellForRowAtIndexPath:indexPath];;
        
        cell.checkmark.hidden = NO;
        
        self.selectedIndexPath = indexPath;
    }
    
    if (self.selectedIndexPath.row == 0) {
        self.mode = SCENEMODES_WALK;
        if (self.stateChangedBlock) {
            self.stateChangedBlock(@"Walk");
        }
        
    } else if (self.selectedIndexPath.row == 1) {
        self.mode = SCENEMODES_SPORT;
        if (self.stateChangedBlock) {
            self.stateChangedBlock(@"Sport");
        }
        
    }
    
    if (self.mode == SCENEMODES_CUSTOM) {
        
        self.selectedIndexPath = indexPath;
        
        self.switcher.on = NO;
        [self.switcher sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (IBAction)openCustomScene:(UISwitch *)sender {
    if (sender.on) {
        self.selectedIndexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        self.mode = SCENEMODES_CUSTOM;
        if (self.stateChangedBlock) {
            self.stateChangedBlock(@"Custom");
        }
        
    } else {
        
        if (self.selectedIndexPath.row > 1) {
            self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        
        self.mode = SCENEMODES_WALK;
        if (self.stateChangedBlock) {
            self.stateChangedBlock(@"Walk");
        }
    }
    
    [self.gimbalSceneModesTableView reloadData];
}
    
@end
