//
//  GimbalSceneModesTableViewModel.h
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GimbalSceneModesTableViewModel : NSObject

@property (strong, nonatomic, readonly) NSArray *titles;

@end
