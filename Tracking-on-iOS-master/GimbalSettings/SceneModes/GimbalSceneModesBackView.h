//
//  GimbalSceneModesBackView.h
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GimbalSceneModesTableViewModel.h"

@interface GimbalSceneModesBackView : UIView

@property (strong, nonatomic) GimbalSceneModesTableViewModel *model;

@property (copy, nonatomic) void (^stateChangedBlock)(NSString *state);

@end
