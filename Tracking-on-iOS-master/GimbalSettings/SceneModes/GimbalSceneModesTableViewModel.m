//
//  GimbalSceneModesTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GimbalSceneModesTableViewModel.h"

@implementation GimbalSceneModesTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    _titles = @[@"Walk",
                @"Sport",
                @"Custom"
                ];
}

@end
