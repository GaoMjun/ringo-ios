//
//  GimbalSettingsTableViewCell.h
//  Ringo-iOS
//
//  Created by qq on 19/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GimbalSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UIImageView *more;
@property (weak, nonatomic) IBOutlet UIView *container;

@property (copy, nonatomic) void (^stateChanged)(NSString *state);

@property (copy, nonatomic) void (^gimbalSceneChangedBlock)(NSString *scene);

@end
