//
//  FilterTableViewCell.h
//  Ringo-iOS
//
//  Created by qq on 14/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *filterName;
@property (weak, nonatomic) IBOutlet UIImageView *filtedImage;

@end
