//
//  BlockButton.m
//  Tracking
//
//  Created by qq on 28/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "BlockButton.h"

typedef void (^ActionBlock)();

@interface BlockButton() {
    ActionBlock _actionBlock;
}

-(void) handleControlEvent:(UIControlEvents)event
                 withBlock:(ActionBlock) action;

@end

@implementation BlockButton

-(void) handleControlEvent:(UIControlEvents)event
                 withBlock:(ActionBlock) action
{
    _actionBlock = action;
    [self addTarget:self action:@selector(callActionBlock:) forControlEvents:event];
}

-(void) callActionBlock:(id)sender{
    _actionBlock();
}

@end
