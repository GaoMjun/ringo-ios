//
//  BleDevicesListTableViewCell.h
//  Tracking
//
//  Created by qq on 21/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BleDevicesListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bleDevicesName;

@end
