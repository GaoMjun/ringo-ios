//
//  CameraSettingsTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "CameraSettingsTableViewModel.h"

@implementation CameraSettingsTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    [self initTitles];
}

- (void)initTitles {
    self.titles = @[@"Video Resolution",
                    @"White Balance",
                    @"Grid",
                    @"Flash",
                    @"Manule"];
}

@end
