//
//  CameraSettingsTableViewModel.h
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CameraSettingsTableViewModel : NSObject

@property (nonatomic, strong) NSArray *titles;

@end
