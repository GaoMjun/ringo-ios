//
//  CameraManuleSettingTableViewCell.h
//  Ringo-iOS
//
//  Created by qq on 20/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraManuleSettingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIPickerView *cameraManuleSettingPickerView;

@end
