//
//  CameraSettingsDetailTableViewModel.h
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CameraSettingsDetailTableViewModel : NSObject

@property (nonatomic, strong) NSDictionary<NSString *, NSArray *> *settingsKind;

@property (nonatomic, strong, readonly) NSDictionary<NSString *, NSDictionary *> *cameraSettingsKinds;

@end
