//
//  AlbumCollectionViewCell.h
//  Tracking
//
//  Created by qq on 23/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
