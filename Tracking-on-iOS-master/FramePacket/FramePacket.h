//
//  FramePacket.h
//  iOS-OpenCV-FaceRec
//
//  Created by qq on 30/7/2016.
//  Copyright © 2016 Fifteen Jugglers Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FramePacket : NSObject

@property (nonatomic, strong) NSData *header;
@property (nonatomic, strong) NSData *payload;
@property (nonatomic, strong) NSData *offset;
@property (nonatomic, strong) NSData *crc;

+ (instancetype)sharedInstance;

@end
