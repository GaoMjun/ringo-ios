//
//  ReceiveFramePacket.h
//  Tracking
//
//  Created by qq on 29/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiveFramePacket : NSObject

@property (strong, nonatomic) NSData *receiveData;

@property (nonatomic, strong) NSData *header;

@property (nonatomic, strong) NSData *command;

@property (nonatomic, strong) NSData *gimbalStatus;

@property (nonatomic, strong) NSData *gimbalMode;

@property (nonatomic, strong) NSData *deviceMode;

@property (nonatomic, strong) NSData *firmwareVersion;

@property (nonatomic, strong) NSData *gimbalRollAngle;

@property (nonatomic, strong) NSData *gimbalPitchAngle;

@property (nonatomic, strong) NSData *gimbalHeaderAngle;

@property (nonatomic, strong) NSData *gimbalBattery;

@property (nonatomic, strong) NSData *zoom;

@property (nonatomic, strong) NSData *offset;

+ (instancetype)sharedInstance;

@end
