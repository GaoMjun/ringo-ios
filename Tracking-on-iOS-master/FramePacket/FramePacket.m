//
//  FramePacket.m
//  iOS-OpenCV-FaceRec
//
//  Created by qq on 30/7/2016.
//  Copyright © 2016 Fifteen Jugglers Software. All rights reserved.
//

#import "FramePacket.h"
#import "BLEDriven.h"
#import "NSString+HexString.h"

@interface FramePacket ()

@end

@implementation FramePacket

+ (instancetype)sharedInstance {
    static FramePacket *framePacket = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        framePacket = [[FramePacket alloc] init];
    });
    
    return framePacket;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _header = [NSString dataWithHexString:@"abcd"];
        _offset = [NSString dataWithHexString:@"00000000000000"];
        
        [self addObserver:self forKeyPath:@"payload" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    id newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"payload"]) {
        NSData *payload = (NSData *)newValue;
        
        NSMutableData *packet = [NSMutableData data];
        [packet appendData:self.header];
        [packet appendData:payload];
        [packet appendData:self.offset];
        
        NSData *framePacket = [self attachCRC16WithData:packet];
        
//        NSLog(@"%@", framePacket);
        [BLEDriven sharedInstance].framePacket = framePacket;
    }
}

- (NSData *)attachCRC16WithData:(NSData *)data {
    Byte *data_c = (Byte *)data.bytes;
    
    char *crc_c = [self calculateCRC16WithBuffer:data_c andLength:data.length];
    
    NSData *crc = [NSData dataWithBytes:crc_c length:2];
    
    NSMutableData *resultData = [NSMutableData data];
    [resultData appendData:data];
    [resultData appendData:crc];
    
    return resultData;
}

- (char *)calculateCRC16WithBuffer:(Byte *)tx andLength:(NSInteger) len {
    short crc = 0x0;
    short crc_ta[] = {0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef};
    
    char da;
    int i = 0;
    while (len-- != 0 ) {
        da = (char)(((crc & 0xffff) >> 12));
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (((tx[i] & 0xff) >> 4) & 0x0f)]));
        da = (char)((crc & 0xffff) >> 12);
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (tx[i] & 0x0F)]));
        i++;
    }
    
    char *bytes = malloc(sizeof(char *) * 3);
    bytes[0] = (char)((crc >> 8) & 0xff);
    bytes[1] = (char)(crc & 0xff);
    bytes[2] = '\0';
    
    return bytes;
}

@end
