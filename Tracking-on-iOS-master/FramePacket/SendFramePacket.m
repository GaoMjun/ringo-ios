//
//  SendFramePacket.m
//  Tracking
//
//  Created by qq on 29/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "SendFramePacket.h"
#import "BLEDriven.h"
#import "NSString+HexString.h"

@implementation SendFramePacket

-(NSData *)header {
    if (!_header) {
        _header = [NSString dataWithHexString:@"abcd"];
    }
    
    return _header;
}

-(NSData *)commandback {
    if (!_commandback) {
        _commandback = [NSString dataWithHexString:@"00"];
    }
    
    return _commandback;
}

-(NSData *)trackingFlag {
    if (!_trackingFlag) {
        _trackingFlag = [NSString dataWithHexString:@"00"];
    }
    
    return _trackingFlag;
}

-(NSData *)trackingQuailty {
    if (!_trackingQuailty) {
        _trackingQuailty = [NSString dataWithHexString:@"00"];
    }
    
    return _trackingQuailty;
}

-(NSData *)xoffset {
    if (!_xoffset) {
        _xoffset = [NSString dataWithHexString:@"00000000"];
    }
    
    return _xoffset;
}

-(NSData *)yoffset {
    if (!_yoffset) {
        _yoffset = [NSString dataWithHexString:@"00000000"];
    }
    
    return _yoffset;
}

-(NSData *)sceneMode {
    if (!_sceneMode) {
        _sceneMode = [NSString dataWithHexString:@"00"];
    }
    
    return _sceneMode;
}

-(NSData *)offset {
    if (!_offset) {
        _offset = [NSString dataWithHexString:@"00000000"];
    }
    
    return _offset;
}

-(NSData *)framePacket {
    NSMutableData *framePacket = [NSMutableData data];
    
    [framePacket appendData:self.header];
    [framePacket appendData:self.commandback];
    [framePacket appendData:self.trackingFlag];
    [framePacket appendData:self.trackingQuailty];
    [framePacket appendData:self.xoffset];
    [framePacket appendData:self.yoffset];
    [framePacket appendData:self.sceneMode];
    [framePacket appendData:self.offset];
    
    return [self attachCRC16WithData:framePacket];
}

+ (instancetype)sharedInstance {
    static SendFramePacket *framePacket = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        framePacket = [[SendFramePacket alloc] init];
    });
    
    return framePacket;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
//        _header = [NSString dataWithHexString:@"abcd"];
//        _offset = [NSString dataWithHexString:@"00000000000000"];
        
//        [self addObserver:self forKeyPath:@"payload" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    id newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"payload"]) {
        NSData *payload = (NSData *)newValue;
        
        NSMutableData *packet = [NSMutableData data];
        
        // header  2byte  0xabcd
        [packet appendData:self.header];
        
        // commandback  1byte  capture:0x11 record:0x12
        
        // tracking flag  1byte on:0x01 off:0x00
        
        // tracking quality  1byte  good:0x01 weak:0x00
        
        // 4byte int xoffset 4byte int yoffset
        
        // offset
        
        // crc 2byte
        
        [packet appendData:payload];
        [packet appendData:self.offset];
        
        NSData *framePacket = [self attachCRC16WithData:packet];
        
        //        NSLog(@"%@", framePacket);
        [BLEDriven sharedInstance].framePacket = framePacket;
    }
}

- (NSData *)attachCRC16WithData:(NSData *)data {
    Byte *data_c = (Byte *)data.bytes;
    
    char *crc_c = [self calculateCRC16WithBuffer:data_c andLength:data.length];
    
    NSData *crc = [NSData dataWithBytes:crc_c length:2];
    
    NSMutableData *resultData = [NSMutableData data];
    [resultData appendData:data];
    [resultData appendData:crc];
    
    return resultData;
}

- (char *)calculateCRC16WithBuffer:(Byte *)tx andLength:(NSInteger) len {
    short crc = 0x0;
    short crc_ta[] = {0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef};
    
    char da;
    int i = 0;
    while (len-- != 0 ) {
        da = (char)(((crc & 0xffff) >> 12));
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (((tx[i] & 0xff) >> 4) & 0x0f)]));
        da = (char)((crc & 0xffff) >> 12);
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (tx[i] & 0x0F)]));
        i++;
    }
    
    char *bytes = malloc(sizeof(char *) * 3);
    bytes[0] = (char)((crc >> 8) & 0xff);
    bytes[1] = (char)(crc & 0xff);
    bytes[2] = '\0';
    
    return bytes;
}

@end
