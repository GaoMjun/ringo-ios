//
//  ReceiveFramePacket.m
//  Tracking
//
//  Created by qq on 29/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "ReceiveFramePacket.h"
#import "NSString+HexString.h"
#import "NSData+HexString.h"
#import "SendFramePacket.h"

@interface ReceiveFramePacket()

@property (nonatomic, strong) NSData *preCommand;
@property (nonatomic, strong) NSData *preGimbalMode;

@end

@implementation ReceiveFramePacket

-(NSData *)preGimbalMode {
    if (!_preGimbalMode) {
        _preGimbalMode = [NSString dataWithHexString:@"00"];
    }
    
    return _preGimbalMode;
}

-(NSData *)preCommand {
    if (!_preCommand) {
        _preCommand = [NSString dataWithHexString:@"00"];
    }
    
    return _preCommand;
}

-(void)setCommand:(NSData *)command {
    if (command) {
        
        if ([command isEqualToData:[NSString dataWithHexString:@"11"]] &&
            [self.preCommand isEqualToData:[NSString dataWithHexString:@"00"]]) {
            // capture
#ifdef DEBUG
            NSLog(@"capture");
#endif
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"captureAction" object:@"captureAction"];
            
        } else if ([command isEqualToData:[NSString dataWithHexString:@"12"]] &&
                   [self.preCommand isEqualToData:[NSString dataWithHexString:@"00"]]) {
            // record
#ifdef DEBUG
            NSLog(@"record");
#endif
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"recordAction" object:@"recordAction"];
            
        } else if ([command isEqualToData:[NSString dataWithHexString:@"13"]] &&
                   [self.preCommand isEqualToData:[NSString dataWithHexString:@"00"]]) {
#ifdef DEBUG
            NSLog(@"switch camera");
#endif
            [[NSNotificationCenter defaultCenter] postNotificationName:@"switchAction" object:@"switchAction"];
            
        } else if ([command isEqualToData:[NSString dataWithHexString:@"00"]]) {
            // clear
            [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"00"];
        }
        
        self.preCommand = command;
    }
}

-(void)setGimbalMode:(NSData *)gimbalMode {
    if (gimbalMode) {
        if ([gimbalMode isEqualToData:[NSString dataWithHexString:@"03"]]) {
        
            if ([self.preGimbalMode isEqualToData:[NSString dataWithHexString:@"00"]]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"canTracking" object:self userInfo:@{@"canTracking" : @"1"}];
            
                self.preGimbalMode = gimbalMode;
            }
            
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"canTracking" object:self userInfo:@{@"canTracking" : @"0"}];
            
            self.preGimbalMode = [NSString dataWithHexString:@"00"];
        }
        
    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"canTracking" object:self userInfo:@{@"canTracking" : @"0"}];
            
        self.preGimbalMode = [NSString dataWithHexString:@"00"];
    }

}

-(void)setGimbalStatus:(NSData *)gimbalStatus {
    if (gimbalStatus) {
        if ([gimbalStatus isEqualToData:[NSString dataWithHexString:@"02"]]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"gimbalRunning" object:self userInfo:@{@"gimbalRunning" : @"1"}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"gimbalRunning" object:self userInfo:@{@"gimbalRunning" : @"0"}];
        }
    }
}

- (void)setGimbalBattery:(NSData *)gimbalBattery {
    if (gimbalBattery) {
        uint8_t battery = 0;
        [gimbalBattery getBytes:&battery length:gimbalBattery.length];
//        NSLog(@"battery: %d", battery);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gimbalBattery" object:self userInfo:@{@"gimbalBattery" : @((int)battery)}];
    }
}

- (void)setZoom:(NSData *)zoom {
    if (zoom) {
        int8_t z = 0;
        [zoom getBytes:&z length:zoom.length];
        
//        NSLog(@"zoom: %d", z);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gimbalZoom" object:self userInfo:@{@"gimbalZoom" : @((int)z)}];
    }
}

+ (instancetype)sharedInstance {
    static ReceiveFramePacket *framePacket = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        framePacket = [[ReceiveFramePacket alloc] init];
    });
    
    return framePacket;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self addObserver:self forKeyPath:@"receiveData" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    id newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"receiveData"]) {
        NSData *receiveData = (NSData *)newValue;
        
        // check header
        self.header = [receiveData subdataWithRange:NSMakeRange(0, 2)];
        if (![self.header isEqualToData:[NSString dataWithHexString:@"9abc"]]) {
            return;
        }
        
        // check crc
        NSData *payload = [receiveData subdataWithRange:NSMakeRange(0, 18)];
        NSData *crc = [receiveData subdataWithRange:NSMakeRange(18, 2)];
        char *crc_c = [self calculateCRC16WithBuffer:(Byte *)payload.bytes andLength:payload.length];
        if (![crc isEqualToData:[NSData dataWithBytes:crc_c length:2]]) {
            return;
        }
        
        // valid data
        self.command = [receiveData subdataWithRange:NSMakeRange(2, 1)];
        
        self.gimbalStatus = [receiveData subdataWithRange:NSMakeRange(3, 1)];
        
        self.gimbalMode = [receiveData subdataWithRange:NSMakeRange(4, 1)];
        
        self.deviceMode = [receiveData subdataWithRange:NSMakeRange(5, 1)];
        
        self.firmwareVersion = [receiveData subdataWithRange:NSMakeRange(6, 1)];
        
        self.gimbalRollAngle = [receiveData subdataWithRange:NSMakeRange(7, 2)];
        
        self.gimbalPitchAngle = [receiveData subdataWithRange:NSMakeRange(9, 2)];
        
        self.gimbalHeaderAngle = [receiveData subdataWithRange:NSMakeRange(11, 2)];
        
        self.gimbalBattery = [receiveData subdataWithRange:NSMakeRange(13, 1)];
        
        self.zoom = [receiveData subdataWithRange:NSMakeRange(14, 1)];
        
        self.offset = [receiveData subdataWithRange:NSMakeRange(15, 3)];
    }
}

- (char *)calculateCRC16WithBuffer:(Byte *)tx andLength:(NSInteger) len {
    short crc = 0x0;
    short crc_ta[] = {0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef};
    
    char da;
    int i = 0;
    while (len-- != 0 ) {
        da = (char)(((crc & 0xffff) >> 12));
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (((tx[i] & 0xff) >> 4) & 0x0f)]));
        da = (char)((crc & 0xffff) >> 12);
        crc <<= 4;
        crc = (short)(crc ^ (crc_ta[da ^ (tx[i] & 0x0F)]));
        i++;
    }
    
    char *bytes = malloc(sizeof(char *) * 3);
    bytes[0] = (char)((crc >> 8) & 0xff);
    bytes[1] = (char)(crc & 0xff);
    bytes[2] = '\0';
    
    return bytes;
}

@end
