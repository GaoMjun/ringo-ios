//
//  SendFramePacket.h
//  Tracking
//
//  Created by qq on 29/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendFramePacket : NSObject

// header  2byte  0xabcd
@property (nonatomic, strong) NSData *header;

// commandback  1byte  capture:0x11 record:0x12
@property (nonatomic, strong) NSData *commandback;

// tracking flag  1byte on:0x01 off:0x00
@property (nonatomic, strong) NSData *trackingFlag;

// tracking quality  1byte  good:0x01 weak:0x00
@property (nonatomic, strong) NSData *trackingQuailty;

// 4byte int xoffset 4byte int yoffset
@property (nonatomic, strong) NSData *xoffset;
@property (nonatomic, strong) NSData *yoffset;

// 1byte
@property (nonatomic, strong) NSData *sceneMode;

// offset 4byte
@property (nonatomic, strong) NSData *offset;

// crc 2byte
@property (nonatomic, strong) NSData *crc;

@property (nonatomic, strong, readonly) NSData *framePacket;

@property (nonatomic, strong) NSData *payload;

+ (instancetype)sharedInstance;

@end
