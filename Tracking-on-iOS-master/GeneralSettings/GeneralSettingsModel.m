//
//  GeneralSettingsModel.m
//  Ringo-iOS
//
//  Created by qq on 26/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GeneralSettingsModel.h"

@implementation GeneralSettingsModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    _titles = @[@"Living"];
}


@end
