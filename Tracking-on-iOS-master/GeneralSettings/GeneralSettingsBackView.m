//
//  GeneralSettingsBackView.m
//  Ringo-iOS
//
//  Created by qq on 26/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GeneralSettingsBackView.h"
#import "GeneralSettingsModel.h"
#import "GeneralSettingsTableViewCell.h"

@interface GeneralSettingsBackView ()

@property (weak, nonatomic) IBOutlet UITableView *generalSettingsTableView;

@property (strong, nonatomic) GeneralSettingsModel *model;

@property (copy, nonatomic) void (^changeSwitcherFromExternal)(BOOL on);

@end

@implementation GeneralSettingsBackView

- (GeneralSettingsModel *)model {
    if ((!_model)) {
        _model = [[GeneralSettingsModel alloc] init];
    }
    
    return _model;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.generalSettingsTableView.rowHeight = UITableViewAutomaticDimension;
    self.generalSettingsTableView.estimatedRowHeight = 44.f;
}

- (void)closeLiving:(BOOL)on {
    if (self.changeSwitcherFromExternal) {
        self.changeSwitcherFromExternal(!on);
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            GeneralSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
            cell.title.text = self.model.titles[indexPath.row];
            cell.switchChanged = ^(UISwitch *switcher) {
                if (self.openLiving) {
                    self.openLiving(switcher);
                }
            };
            self.changeSwitcherFromExternal = cell.changeSwitcherFromExternal;
            
            return cell;
        }
            
            break;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

@end
