//
//  GeneralSettingsTableViewCell.h
//  Ringo-iOS
//
//  Created by qq on 26/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;

@property (copy, nonatomic) void (^switchChanged)(UISwitch *switcher);

@property (copy, nonatomic) void (^changeSwitcherFromExternal)(BOOL on);

@end
