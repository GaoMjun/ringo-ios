//
//  GeneralSettingsTableViewCell.m
//  Ringo-iOS
//
//  Created by qq on 26/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "GeneralSettingsTableViewCell.h"

@implementation GeneralSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.switcher.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    __weak typeof(self) weakSelf = self;
    self.changeSwitcherFromExternal = ^(BOOL on) {
        weakSelf.switcher.on = on;
        [weakSelf.switcher sendActionsForControlEvents:UIControlEventValueChanged];
    };
}

- (IBAction)switchAction:(UISwitch *)sender {
    if (self.switchChanged) {
        self.switchChanged(sender);
    }
}

@end
