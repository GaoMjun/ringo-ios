//
//  GeneralSettingsBackView.h
//  Ringo-iOS
//
//  Created by qq on 26/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralSettingsBackView : UIView

@property (copy, nonatomic) void (^openLiving)(UISwitch *switcher);

- (void)closeLiving:(BOOL)on;

@end
