//
//  ViewController.h
//  Tracking
//
//  Created by FloodSurge on 8/3/15.
//  Copyright (c) 2015 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/highgui/cap_ios.h>

typedef NS_ENUM(NSUInteger, RecordStatus) {
    RECORDSATRT,
    RECORDING,
    RECORDSTOP
};

typedef NS_ENUM(NSUInteger, CameraPreviewGrid) {
    CameraPreviewGridNone,
    CameraPreviewGridCenter,
    CameraPreviewGridDiagonal,
    CameraPreviewGridSudoku
};

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

@property (assign, nonatomic) RecordStatus recordStatus;

- (void)captureAction;

@end

