//
//  AlbumController.m
//  Tracking
//
//  Created by qq on 20/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "AlbumController.h"

@interface AlbumController ()

@end

@implementation AlbumController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

@end
