//
//  ViewController.m
//  VisionSeg
//
//  Created by FloodSurge on 15/7/14.
//  Copyright (c) 2015年 FloodSurge. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AlbumController.h"
#import "BleDevicesListTableViewCell.h"
#import "BLEDriven.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "NSString+HexString.h"
#import "NSData+RandomData.h"
#import "SendFramePacket.h"
#import "MWPhotoBrowser.h"
#import "BLEDriven.h"
#import "MZTimerLabel.h"
#import <BlocksKit/BlocksKit.h>
#import <BlocksKit/UIControl+BlocksKit.h>
#import "RTMPClient.h"
#import "MBProgressHUD+Toast.h"
#import "MotionOrientation.h"
#import "FilterTableViewCell.h"
#import "FiltersTableViewModel.h"
#import "InstaFilters.h"
#import "Masonry.h"
#import "CameraSettingsTableViewCell.h"
#import "CameraSettingsTableViewModel.h"
#import "CameraSettingsDetailTableViewModel.h"
#import "CameraSettingsDetailTableViewCell.h"
#import "UIImage+Category.h"
#import "CameraManuleSettingTableViewCell.h"
#import "TimeLapseCaptureView.h"
#import "GimbalSettingsBackView.h"
#import "GeneralSettingsBackView.h"
#import "FrontCameraCaptureModesView.h"

#ifdef __cplusplus
#import "CMT.h"
#import "kcftracker.hpp"
#endif

#import "UIView+GCLibrary.h"
#import "SevenSwitch.h"

#import "CustomAlbum.h"
#import <Bugly/Bugly.h>

#import "CvCamera.h"

#define RATIO  2.0
#define SCALE  10.0

#define SCREEN_WIDTH    [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT   [UIScreen mainScreen].bounds.size.height

using namespace cv;
using namespace std;

typedef NS_ENUM(NSUInteger, CameraMode) {
    CAPTURE,
    RECORD
};

typedef NS_ENUM(NSUInteger, CameraPosition) {
    FRONT,
    BACK
};

typedef NS_ENUM(NSUInteger, BleStatus) {
    BLEDISCONNECTED,
    BLECONNECTING,
    BLECONNECTED
};

@interface ViewController ()<CAAnimationDelegate,
UINavigationControllerDelegate, UIImagePickerControllerDelegate,
UITableViewDelegate, UITableViewDataSource,
MWPhotoBrowserDelegate,
CvCameraDelegate,
UIPickerViewDelegate,
UIPickerViewDataSource>
{
    CGPoint rectLeftTopPoint;
    CGPoint rectRightDownPoint;
    
    cv::Rect selectBox;
    cv::Rect initCTBox;
    bool beginInit;
    bool startTracking;
    
    cmt::CMT *cmtTracker;
    KCFTracker *kcfTracker;
    
    size_t initialPoints;
    
    UIImage *willSavePhoto;
    cv::Mat tmpMat;
    
    
//    RecordStatus recordStatus;
    BleStatus bleStatus;
    
    NSString *maxRecordTime;
    
    BOOL canSwitchTrackingStatus;
    
    cv::Mat mat_t;
}

@property (weak, nonatomic) IBOutlet UILabel *userId_live;
@property (weak, nonatomic) IBOutlet UILabel *livingTime;
@property (copy, nonatomic) void (^livingTimeChanged)(NSString *timeString);
@property (nonatomic, strong) MZTimerLabel *mzTimerLabel_live;


@property (weak, nonatomic) IBOutlet UIView *rightToolBar;
@property (weak, nonatomic) IBOutlet UIButton *showFiltersButton;
@property (weak, nonatomic) IBOutlet UIButton *showCameraSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *gimbalSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *generalSettingButton;
@property (weak, nonatomic) IBOutlet UIButton *liveSettingButton;


@property (weak, nonatomic) IBOutlet UIView *leftToolBar;
@property (weak, nonatomic) IBOutlet UIButton *cameraModeSwitcher;
@property (weak, nonatomic) IBOutlet UIButton *captureModeButton;
@property (weak, nonatomic) IBOutlet UIImageView *captureModeArrow;
//@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraSwitchButton;
@property (weak, nonatomic) IBOutlet UIButton *albumButton;

@property (weak, nonatomic) IBOutlet UIButton *bleStatusButton;
@property (weak, nonatomic) IBOutlet UIButton *trackingStatusButton;

@property (weak, nonatomic) IBOutlet UITableView *bleDevicesListTableView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *bleDevicesListView;
@property (weak, nonatomic) IBOutlet UIButton *closeBLEDevicesListViewButton;
@property (weak, nonatomic) IBOutlet UIView *trackingBox;
@property (weak, nonatomic) IBOutlet UIImageView *testImage;
@property (weak, nonatomic) IBOutlet UILabel *limitRecordTimeLabel;


@property (strong, nonatomic) NSArray *bleDevices;
@property (strong, nonatomic) NSString *connectedDevice;
@property (strong, nonatomic) NSIndexPath *connectedDeviceIndexPath;
@property (assign, nonatomic) BOOL enableTracking;



@property (strong, nonatomic) NSData *pureData;
@property (assign, nonatomic) BOOL detected;

@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;

@property (nonatomic, strong) MZTimerLabel *mzTimerLabel;

@property (nonatomic, assign) BOOL takePhotoButtonStatus;
@property (nonatomic, assign) CameraMode cameraMode;
@property (nonatomic, assign) CameraPosition cameraPosition;

@property (nonatomic, assign) BOOL remoteAction;
@property (nonatomic, strong) NSString *canTracking;
@property (nonatomic, assign) BOOL faceFollow;

@property (nonatomic, strong) CvCamera *cvCamera;

@property (nonatomic, strong) NSArray *faceRects;

@property (nonatomic, strong) RTMPClient *rtmpClient;

@property (nonatomic, strong) CAShapeLayer *gridLayer;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *settingBackView;
@property (strong, nonatomic) IBOutlet UITableView *filterTableView;
@property (strong, nonatomic) IBOutlet UITableView *cameraSettingsTableView;
@property (strong, nonatomic) IBOutlet UITableView *cameraSettingsDetailTableView;

@property (nonatomic, strong) FiltersTableViewModel *filtersTableViewModel;
@property (nonatomic, strong) CameraSettingsTableViewModel *cameraSettingsTableViewModel;
@property (nonatomic, strong) CameraSettingsDetailTableViewModel *cameraSettingsDetailTableViewModel;

@property (nonatomic, assign) NSIndexPath *selectedFilterIndexPath;

@property (assign, nonatomic) BOOL cameraSettingOpened;
@property (assign, nonatomic) NSIndexPath *lastClickCameraSettingIndexPath;

@property (nonatomic, strong) NSString *currentVideoResolution;
@property (nonatomic, strong) NSString *currentWhiteBalance;
@property (nonatomic, strong) NSString *currentGrid;
@property (nonatomic, strong) NSString *currentFlash;

@property (weak, nonatomic) IBOutlet UIImageView *gimbalBatteryImage;

@property (assign, nonatomic) BOOL cameraManuelSettingsOpened;

@property (nonatomic, strong) UIPickerView *cameraManuleSettingPickerView;

@property (weak, nonatomic) IBOutlet UIView *captureModesBackView;
@property (strong, nonatomic) IBOutlet UIView *backCameraVideoModesView;
@property (weak, nonatomic) IBOutlet UIButton *backSwitchVideoModeToNormalButton;
@property (weak, nonatomic) IBOutlet UIButton *backSwitchVideoModeToSlowMotionButton;
@property (weak, nonatomic) IBOutlet UIButton *backSwitchVideoModeToTimeLapseButton;
@property (strong, nonatomic) IBOutlet UIView *frontCameraVideoModesView;
@property (weak, nonatomic) IBOutlet UIButton *frontSwitchVideoModeToNormalButton;
@property (weak, nonatomic) IBOutlet UIButton *frontSwitchVideoModeToSlowMotionButton;
@property (weak, nonatomic) IBOutlet UIButton *frontSwitchVideoModeToTimeLapseButton;

@property (strong, nonatomic) IBOutlet FrontCameraCaptureModesView *frontCameraCaptureModesView;


@property (strong, nonatomic) IBOutlet TimeLapseCaptureView *timeLapseCaptureView;
@property (weak, nonatomic) IBOutlet UIView *timeLapseCaptureBackView;
@property (strong, nonatomic) IBOutlet GimbalSettingsBackView *gimbalSettingsBackView;
@property (strong, nonatomic) IBOutlet GeneralSettingsBackView *generalSettingsBackView;

@property (weak, nonatomic) IBOutlet UIButton *startLivingButton;

@property (assign, nonatomic) BOOL startTimeLapse;

@end

@implementation ViewController

- (CameraSettingsDetailTableViewModel *)cameraSettingsDetailTableViewModel {
    if (!_cameraSettingsDetailTableViewModel) {
        _cameraSettingsDetailTableViewModel = [[CameraSettingsDetailTableViewModel alloc] init];
    }
    
    return _cameraSettingsDetailTableViewModel;
}

- (CameraSettingsTableViewModel *)cameraSettingsTableViewModel {
    if (!_cameraSettingsTableViewModel) {
        _cameraSettingsTableViewModel = [[CameraSettingsTableViewModel alloc] init];
    }
    
    return _cameraSettingsTableViewModel;
}

- (FiltersTableViewModel *)filtersTableViewModel {
    if (!_filtersTableViewModel) {
        _filtersTableViewModel = [[FiltersTableViewModel alloc] init];
    }
    
    return _filtersTableViewModel;
}

- (MZTimerLabel *)mzTimerLabel_live {
    if (!_mzTimerLabel_live) {
        _mzTimerLabel_live = [[MZTimerLabel alloc] init];
        
        [_mzTimerLabel_live bk_addObserverForKeyPath:@"text" task:^(id target) {
            self.livingTime.text = _mzTimerLabel_live.text;
            if (self.livingTimeChanged) {
                self.livingTimeChanged(_mzTimerLabel_live.text);
            }
        }];
        
    }
    
    return _mzTimerLabel_live;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.cvCamera = [[CvCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraPosition:AVCaptureDevicePositionFront orientation:UIInterfaceOrientationPortrait];
        
        self.cvCamera.delegate = self;
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)layoutView {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [_captureModeButton bk_addEventHandler:^(UIButton *button) {
        self.captureModeArrow.highlighted = !self.captureModeArrow.isHighlighted;

        if (self.captureModeButton.isSelected) { // Video
            if (self.captureModeArrow.isHighlighted) { // extend
                self.captureModesBackView.hidden = NO;
                
                self.bleStatusButton.hidden = YES;
                self.trackingStatusButton.hidden = YES;

                if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                    [self.captureModesBackView addSubview:self.backCameraVideoModesView];
                    [self.backCameraVideoModesView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.equalTo(self.captureModesBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
                    }];

                } else if (self.cvCamera.cameraPosition == AVCaptureDevicePositionFront) {
                    [self.captureModesBackView addSubview:self.frontCameraVideoModesView];
                    [self.frontCameraVideoModesView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.equalTo(self.captureModesBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
                    }];
                }
            } else { // hiden
                self.captureModesBackView.hidden = YES;
                self.bleStatusButton.hidden = NO;
                self.trackingStatusButton.hidden = NO;

                [self.backCameraVideoModesView removeFromSuperview];
                [self.frontCameraVideoModesView removeFromSuperview];
            }

        } else { // photo
            if (self.captureModeArrow.isHighlighted) {
                self.captureModesBackView.hidden = NO;
                
                self.bleStatusButton.hidden = YES;
                self.trackingStatusButton.hidden = YES;
                
                if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                    
                } else {
                    [self.captureModesBackView addSubview:self.frontCameraCaptureModesView];
                    [self.frontCameraCaptureModesView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.equalTo(self.captureModesBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
                    }];
                }
                
            } else {
                self.captureModesBackView.hidden = YES;
                
                self.bleStatusButton.hidden = NO;
                self.trackingStatusButton.hidden = NO;
                
                [self.frontCameraCaptureModesView removeFromSuperview];
            }
            
        }


    } forControlEvents:UIControlEventTouchUpInside];

    [_takePhotoButton bk_addEventHandler:^(id sender) {
        [self captureAction];
//        self.takePhotoButtonStatus = YES;
//        self.takePhotoButtonStatus = NO;
    } forControlEvents:UIControlEventTouchUpInside];

    [_cameraSwitchButton bk_addEventHandler:^(id sender) {
        if (self.cameraPosition == FRONT) {
            self.cameraPosition = BACK;
        } else {
            self.cameraPosition = FRONT;
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_cameraModeSwitcher bk_addEventHandler:^(id sender) {
        _cameraModeSwitcher.selected = !_cameraModeSwitcher.isSelected;
        if (self.captureModeArrow.isHighlighted) {
            [self.captureModeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        if (_cameraModeSwitcher.isSelected) {
            self.cameraMode = RECORD;
            self.captureModeButton.selected = YES;
        } else {
            self.cameraMode = CAPTURE;
            self.captureModeButton.selected = NO;
        }
        

        if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
            [self.backSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else if (self.cvCamera.cameraPosition == AVCaptureDevicePositionFront) {
            [self.frontSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }

        if (!self.timeLapseCaptureBackView.isHidden) {
            self.timeLapseCaptureBackView.hidden = YES;
        }

    } forControlEvents:UIControlEventTouchUpInside];
    
    [_albumButton bk_addEventHandler:^(id sender) {
        [self openAlbum];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_bleStatusButton bk_addEventHandler:^(id sender) {
        [self checkBle];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_closeBLEDevicesListViewButton bk_addEventHandler:^(id sender) {
        self.bleDevicesListView.hidden = !self.bleDevicesListView.hidden;
        
        if (self.bleDevicesListView.hidden) {
            [[BLEDriven sharedInstance] stopScan];
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    [_trackingStatusButton bk_addEventHandler:^(UIButton *sender) {
        if (bleStatus == BLECONNECTED) {
            sender.selected = !sender.selected;
            
            [self switchTracking];
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    _trackingBox.layer.masksToBounds = YES;
    _trackingBox.layer.borderWidth = 2.;
    _trackingBox.layer.borderColor = [UIColor colorWithRed:196/255. green:233/255. blue:109/255. alpha:1.].CGColor;
    _trackingBox.layer.cornerRadius = 5;

    _mzTimerLabel = [[MZTimerLabel alloc] init];
    self.limitRecordTimeLabel.hidden = YES;

}

#pragma mark - view lift circle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self checkAlbum];
    
    [self layoutView];
    
    self.cvCamera.cameraPreview = self.view;
    
    rectLeftTopPoint = CGPointZero;
    rectRightDownPoint = CGPointZero;
    
    beginInit = false;
    startTracking = false;
    _enableTracking = NO;
    
    canSwitchTrackingStatus = true;
    
    self.cameraMode = CAPTURE;
    self.cameraPosition = BACK;
    self.recordStatus = RECORDSATRT;
    bleStatus = BLEDISCONNECTED;
    
//    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self addObserver:self forKeyPath:@"pureData" options:NSKeyValueObservingOptionNew context:NULL];
    [self.mzTimerLabel addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"takePhotoButtonStatus" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"cameraMode" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"cameraPosition" options:NSKeyValueObservingOptionNew context:NULL];
    
    [self addObserver:self forKeyPath:@"enableTracking" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"faceFollow" options:NSKeyValueObservingOptionNew context:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BLEDisconnectedAction) name:@"BLEDisconnected" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remoteCaptureAction) name:@"captureAction" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remoteRecordAction) name:@"recordAction" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remoteSwitchAction) name:@"switchAction" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remoteFollowMode:) name:@"canTracking" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gimbalStatus:) name:@"gimbalRunning" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gimbalBattery:) name:@"gimbalBattery" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gimbalZoom:) name:@"gimbalZoom" object:nil];
    
//    [MotionOrientation initialize];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:MotionOrientationChangedNotification object:nil];
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    UIDeviceOrientation deviceOrientation = UIDevice.currentDevice.orientation;
    [self restartPreview:deviceOrientation];
}

- (void)orientationChanged:(NSNotification *)notification {
    UIDeviceOrientation deviceOrientation = [MotionOrientation sharedInstance].deviceOrientation;
    [self restartPreview:deviceOrientation];

    switch (deviceOrientation) {
        case UIDeviceOrientationUnknown:

            return;

        case UIDeviceOrientationPortrait:

            break;

        case UIDeviceOrientationPortraitUpsideDown:

            break;

        case UIDeviceOrientationLandscapeLeft:

            break;

        case UIDeviceOrientationLandscapeRight:

            break;

        case UIDeviceOrientationFaceUp:

            return;

        case UIDeviceOrientationFaceDown:

            return;

        default:
            return;
    }
}

- (void)restartPreview:(UIDeviceOrientation)deviceOrientation {
    UIInterfaceOrientation orientantion = UIInterfaceOrientationLandscapeRight;
    NSString *captureSessionPreset = self.cvCamera.captureSessionPreset;
    AVCaptureDevicePosition cameraPosition = self.cvCamera.cameraPosition;
    
    switch (deviceOrientation) {
        case UIDeviceOrientationUnknown:
            NSLog(@"%s, UIDeviceOrientationUnknown", __func__);
            return;
            
        case UIDeviceOrientationPortrait:
            NSLog(@"%s, UIDeviceOrientationPortrait", __func__);
            orientantion = UIInterfaceOrientationPortrait;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"%s, UIDeviceOrientationPortraitUpsideDown", __func__);
            orientantion = UIInterfaceOrientationPortraitUpsideDown;
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"%s, UIDeviceOrientationLandscapeLeft", __func__);
            orientantion = UIInterfaceOrientationLandscapeRight;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"%s, UIDeviceOrientationLandscapeRight", __func__);
            orientantion = UIInterfaceOrientationLandscapeLeft;
            break;
            
        case UIDeviceOrientationFaceUp:
            NSLog(@"%s, UIDeviceOrientationFaceUp", __func__);
            return;
            
        case UIDeviceOrientationFaceDown:
            NSLog(@"%s, UIDeviceOrientationFaceDown", __func__);
            return;
            
        default:
            return;
    }
    
    if (orientantion == self.cvCamera.previewOrientation) {
        return;
    }
    
    if (self.recordStatus == RECORDING) {
        [self.takePhotoButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    self.cvCamera.delegate = nil;
    [self.cvCamera stop];
    
    if (self.cvCamera.currentCvCameraFormat == CvCameraFormatSlowMotion) {
        [self.cvCamera changeFormat:CvCameraFormatSlowMotion];
        return;
    }
    
    self.cvCamera = [[CvCamera alloc] initWithSessionPreset:captureSessionPreset cameraPosition:cameraPosition orientation:orientantion];
    self.cvCamera.cameraPreview = self.view;
    self.cvCamera.delegate = self;
    
    [self.cvCamera start];
}

- (void)restartCamera {
    AVCaptureDevicePosition position = self.cvCamera.cameraPosition;
    
    if (self.recordStatus == RECORDING) {
        [self.takePhotoButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    self.cvCamera.delegate = nil;
    [self.cvCamera stop];

    UIInterfaceOrientation interfaceOrientation;
    switch (UIDevice.currentDevice.orientation) {
        case UIDeviceOrientationPortrait:
            interfaceOrientation = UIInterfaceOrientationPortrait;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            interfaceOrientation = UIInterfaceOrientationPortraitUpsideDown;
            break;
        case UIDeviceOrientationLandscapeLeft:
            interfaceOrientation = UIInterfaceOrientationLandscapeRight;
            break;
        case UIDeviceOrientationLandscapeRight:
            interfaceOrientation = UIInterfaceOrientationLandscapeLeft;
            break;
        default:
            interfaceOrientation = UIInterfaceOrientationPortrait;
            break;
    }
    self.cvCamera = [[CvCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720
                                             cameraPosition:position
                                                orientation:interfaceOrientation];
    self.cvCamera.cameraPreview = self.view;
    self.cvCamera.delegate = self;

    [self.cvCamera start];
}

- (void)gimbalStatus:(NSNotification *)notification {
    NSDictionary *d = [notification userInfo];
    
    if ([d[@"gimbalRunning"] boolValue]) {
        
        if (!self.trackingStatusButton.enabled) {
            self.trackingStatusButton.enabled = !self.trackingStatusButton.enabled;
        }
        
    } else {
        if (self.trackingStatusButton.selected) {
            self.trackingStatusButton.selected = !self.trackingStatusButton.selected;
        }
        
        if (self.trackingStatusButton.enabled) {
            self.trackingStatusButton.enabled = !self.trackingStatusButton.enabled;
        }
    }
}

- (void)gimbalBattery:(NSNotification *)notification {
    NSDictionary *d = [notification userInfo];
    
    if (self.gimbalBatteryImage.hidden) {
        self.gimbalBatteryImage.hidden = !self.gimbalBatteryImage.hidden;
    }
    
    int battery = [d[@"gimbalBattery"] intValue];
    if (battery > 0 && battery <= 10) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv1"];
    } else if (battery > 10 && battery <= 20) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv2"];
    } else if (battery > 20 && battery <= 30) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv3"];
    } else if (battery > 30 && battery <= 40) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv4"];
    } else if (battery > 40 && battery <= 50) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv5"];
    } else if (battery > 50 && battery <= 60) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv6"];
    } else if (battery > 60 && battery <= 70) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv7"];
    } else if (battery > 70 && battery <= 80) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv8"];
    } else if (battery > 80 && battery <= 100) {
        self.gimbalBatteryImage.image = [UIImage imageNamed:@"lp_ic_battery_lv9"];
    }
}

- (void)gimbalZoom:(NSNotification *)notification {
    NSDictionary *d = [notification userInfo];
    int zoomValue = [d[@"gimbalZoom"] intValue];
    
//    NSLog(@"%d", zoomValue);
    [self.cvCamera zoomCamera:zoomValue];
}

- (void)remoteFollowMode:(NSNotification *)notification {
    NSDictionary *d = [notification userInfo];
    
    if ([d[@"canTracking"] boolValue]) {
        self.faceFollow = YES;
    } else {
        self.faceFollow = NO;
    }
}

- (void)remoteCaptureAction {
    self.remoteAction = YES;
    
    if (self.cameraMode == RECORD) {
        self.cameraMode = CAPTURE;
        if (self.cameraModeSwitcher.isSelected) {
            self.cameraModeSwitcher.selected = !self.cameraModeSwitcher.isSelected;
        }
    }

    [self.takePhotoButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"11"];
}

- (void)remoteRecordAction {
    self.remoteAction = YES;
    
    if (self.cameraMode == CAPTURE) {
        self.cameraMode = RECORD;
        if (!self.cameraModeSwitcher.isSelected) {
            self.cameraModeSwitcher.selected = !self.cameraModeSwitcher.isSelected;
        }
    }

    [self.takePhotoButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"12"];
}

- (void)remoteSwitchAction {
    self.remoteAction = YES;
    
    [self.cameraSwitchButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"13"];
}

- (void)BLEDisconnectedAction {
    self.trackingStatusButton.selected = NO;
    self.enableTracking = NO;
    self.connectedDevice = nil;
    
    if (self.trackingStatusButton.selected) {
        [self stopTracking];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.trackingBox.hidden = YES;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (!self.trackingBox.hidden) {
                    self.trackingBox.hidden = YES;
                }
            });
        });
    }
    
    bleStatus = BLEDISCONNECTED;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.cvCamera start];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.cvCamera stop];
    
    if (self.trackingStatusButton.selected) {
        [self switchTracking];
    }
    
    if (self.recordStatus == RECORDING) {
        NSOperationQueue* operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue addOperationWithBlock:^{
            // Perform long-running tasks without blocking main thread
            [self.takePhotoButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.enableTracking) {
    
        startTracking = false;
        beginInit = false;
        
        UITouch *aTouch  = [touches anyObject];
        rectLeftTopPoint = [aTouch locationInView:self.view];
#ifdef DEBUG
        NSLog(@"touch in :%f,%f",rectLeftTopPoint.x,rectLeftTopPoint.y);
#endif
        rectRightDownPoint = CGPointZero;
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.enableTracking) {
    
        UITouch *aTouch  = [touches anyObject];
        rectRightDownPoint = [aTouch locationInView:self.view];
#ifdef DEBUG
        NSLog(@"touch move :%f,%f",rectRightDownPoint.x,rectRightDownPoint.y);
#endif
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.enableTracking) {
    
        UITouch *aTouch  = [touches anyObject];
        rectRightDownPoint = [aTouch locationInView:self.view];
#ifdef DEBUG
        NSLog(@"touch end :%f,%f",rectRightDownPoint.x,rectRightDownPoint.y);
#endif
        
        float x = rectLeftTopPoint.x;
        float y = rectLeftTopPoint.y;
        float w = rectLeftTopPoint.x - rectRightDownPoint.x;
        float h = rectLeftTopPoint.y - rectRightDownPoint.y;
        
        if (w < 0) {
            w = -w;
        } else {
            x = rectRightDownPoint.x;
        }
        
        if (h < 0) {
            h = -h;
        } else {
            y = rectRightDownPoint.y;
        }
        
        selectBox = cv::Rect(x * RATIO, y * RATIO, w * RATIO, h * RATIO);

        
        if (selectBox.width > 100 &&
            selectBox.height > 100) {
            
            beginInit = YES;
            
            initCTBox.x = selectBox.x/SCALE;
            initCTBox.y = selectBox.y/SCALE;
            initCTBox.width = selectBox.width/SCALE;
            initCTBox.height = selectBox.height/SCALE;
            
            [BLEDriven sharedInstance].running = YES;
            
        } else {
#ifdef DEBUG
            NSLog(@"selectBox is too small");
#endif
            
            [self stopTracking];
            self.trackingBox.hidden = YES;
            [BLEDriven sharedInstance].running = NO;
            
            [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"00"];
            [SendFramePacket sharedInstance].xoffset = [NSString dataWithHexString:@"00000000"];
            [SendFramePacket sharedInstance].yoffset = [NSString dataWithHexString:@"00000000"];
        }
    }
}

#pragma mark CvCameraDelegate
#ifdef __cplusplus
- (void)processImage:(cv::Mat &)matImage{
//    NSLog(@"processImage");
    
    if (self.enableTracking) {

        if (rectLeftTopPoint.x != 0 &&
            rectLeftTopPoint.y != 0 &&
            rectRightDownPoint.x != 0 &&
            rectRightDownPoint.y != 0 &&
            !beginInit &&
            !startTracking) {
            
            if (self.trackingBox.hidden) {
                self.trackingBox.hidden = NO;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                float x = rectLeftTopPoint.x;
                float y = rectLeftTopPoint.y;
                float w = rectLeftTopPoint.x - rectRightDownPoint.x;
                float h = rectLeftTopPoint.y - rectRightDownPoint.y;
                
                if (w < 0) {
                    w = -w;
                } else {
                    x = rectRightDownPoint.x;
                }
                
                if (h < 0) {
                    h = -h;
                } else {
                    y = rectRightDownPoint.y;
                }
                
                self.trackingBox.frame = CGRectMake(x, y, w, h);

            });
        }
        
        [self cmtTracking:matImage];
//        [self kcfTracking:matImage];

    }
#ifdef DEBUG1
    if (!matImage.empty()) {
        cv::Mat image = matImage.clone();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.testImage.image = MatToUIImage(image);
        });
    }
#endif
}
#endif

- (BOOL)pointInRect:(CGRect)rect point:(CGPoint)point {
    if ((point.x > rect.origin.x && point.x < (rect.origin.x+rect.size.width)) &&
        (point.y > rect.origin.y && point.y < (rect.origin.y+rect.size.height))) {
        return YES;
    }
    
    return NO;
}

- (void)kcfTracking:(cv::Mat &)matImage {
    if (beginInit) {
        
        if (kcfTracker != nullptr) {
            delete kcfTracker;
        }
        
        kcfTracker = new KCFTracker(true, false);
        kcfTracker->init(initCTBox, matImage);
        
        startTracking = true;
        beginInit = false;
    }
    
    if (startTracking) {
        cv::Rect rect = kcfTracker->update(matImage);
        self.detected = YES;
        
        Point2f center;
        center.x = rect.x + rect.width/2;
        center.y = rect.y + rect.height/2;
        
        {
            NSMutableData *m_offset_data = [NSMutableData data];
            
            int x_offset = matImage.cols/2 - center.x;
            int y_offset = matImage.rows/2 - center.y;
            
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                x_offset = -matImage.cols/2 + center.x;
                y_offset = -matImage.rows/2 + center.y;
            }
            
            x_offset *= SCALE;
            y_offset *= SCALE;
#ifdef DEBUG1
            NSLog(@"[%d, %d]", x_offset, y_offset);
#endif
            
            NSData *x_offset_data = [NSData dataWithBytes:&x_offset length:sizeof(x_offset)];
            NSData *y_offset_data = [NSData dataWithBytes:&y_offset length:sizeof(y_offset)];
            
            [m_offset_data appendData:x_offset_data];
            [m_offset_data appendData:y_offset_data];
            
            self.pureData = [NSData dataWithData:m_offset_data];
        }
        
        if (self.trackingBox.hidden) {
            self.trackingBox.hidden = NO;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.trackingBox.center = CGPointMake(center.x*SCALE/RATIO, center.y*SCALE/RATIO);
            if (rect.width*SCALE/RATIO < selectBox.width/2 && rect.height*SCALE/RATIO < selectBox.height/2) {
                self.trackingBox.width = rect.width*SCALE/RATIO;
                self.trackingBox.height = rect.height*SCALE/RATIO;
            } else {
                self.trackingBox.width = selectBox.width/2;
                self.trackingBox.height = selectBox.height/2;
            }
            
        });
        
    }
}

- (void)cmtTracking:(cv::Mat &)matImage
{
    if (beginInit) {
        
        if (cmtTracker != NULL) {
            delete cmtTracker;
        }
        cmtTracker = new cmt::CMT();
        cmtTracker->initialize(matImage, initCTBox);
        initialPoints = cmtTracker->points_active.size();
#ifdef DEBUG
        NSLog(@"cmt track init!");
#endif
        startTracking = true;
        beginInit = false;
    }
    
    if (startTracking) {
//        NSLog(@"cmt process...");
        cmtTracker->processFrame(matImage);
        
//        NSLog(@"%lu", cmtTracker->points_active.size());
        
        if (cmtTracker->points_active.size() > initialPoints/4) {
        
            self.detected = YES;

            RotatedRect rect = cmtTracker->bb_rot;
            Point2f center;
            center.x = rect.center.x;//*SCALE;
            center.y = rect.center.y;//*SCALE;
            
            {
                NSMutableData *m_offset_data = [NSMutableData data];
                
                int x_offset = matImage.cols/2 - center.x;
                int y_offset = matImage.rows/2 - center.y;
                
                if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                    x_offset = -matImage.cols/2 + center.x;
                    y_offset = -matImage.rows/2 + center.y;
                }

                x_offset *= SCALE;
                y_offset *= SCALE;
#ifdef DEBUG1
                NSLog(@"[%d, %d]", x_offset, y_offset);
#endif
                
                NSData *x_offset_data = [NSData dataWithBytes:&x_offset length:sizeof(x_offset)];
                NSData *y_offset_data = [NSData dataWithBytes:&y_offset length:sizeof(y_offset)];
                
                [m_offset_data appendData:x_offset_data];
                [m_offset_data appendData:y_offset_data];
                
                self.pureData = [NSData dataWithData:m_offset_data];
            }
            
            if (self.trackingBox.hidden) {
                self.trackingBox.hidden = NO;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.trackingBox.center = CGPointMake(center.x*SCALE/RATIO, center.y*SCALE/RATIO);
                if (rect.size.width*SCALE/RATIO < selectBox.width/2 && rect.size.height*SCALE/RATIO < selectBox.height/2) {
                    self.trackingBox.width = rect.size.width*SCALE/RATIO;
                    self.trackingBox.height = rect.size.height*SCALE/RATIO;
                } else {
                    self.trackingBox.width = selectBox.width/2;
                    self.trackingBox.height = selectBox.height/2;
                }

            });
            
        } else {
            self.detected = NO;
            self.pureData = [NSData randomDataWithLength:8];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.trackingBox.hidden = YES;
                
            });
        }
    }
}

- (void)stopTracking {
    rectLeftTopPoint = CGPointZero;
    rectRightDownPoint = CGPointZero;
    beginInit = NO;
    startTracking = NO;
    [BLEDriven sharedInstance].running = NO;
}

#pragma mark button action

- (void)captureAction {
    // MiB
    uint64_t freespace = [self getFreeDiskspace];
    
//    NSLog(@"%llu", freespace);
//    NSInteger maxVideoScends = freespace / 2.0;
//    maxRecordTime = [self secondsToHHMMSS:maxVideoScends];
    
    if (freespace < 100) {
        BLYLogWarn(@"not enough freespace");
        
        return;
    }
    
    if (self.cameraMode == CAPTURE) {
        
        [self takePicture];
        
    } else if (self.cameraMode == RECORD) {
        switch (self.recordStatus) {
            case RECORDSATRT:
                self.recordStatus = RECORDING;
                _takePhotoButton.selected = YES;
                
                _cameraModeSwitcher.enabled = NO;
                _cameraSwitchButton.enabled = NO;
                _albumButton.enabled = NO;
                _captureModeButton.enabled = NO;
                
                if (!self.startTimeLapse) {
                    [self startRecordVideo];
                }
                
                self.limitRecordTimeLabel.hidden = NO;
                [self.mzTimerLabel reset];
                [self.mzTimerLabel start];
                
                break;
                
            case RECORDING: {
                self.recordStatus = RECORDSATRT;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    _takePhotoButton.selected = NO;
                    _cameraModeSwitcher.enabled = YES;
                    _cameraSwitchButton.enabled = YES;
                    _albumButton.enabled = YES;
                    _captureModeButton.enabled = YES;
                    self.limitRecordTimeLabel.hidden = YES;
                });
                
                if (!self.startTimeLapse) {
                    [self stopRecordVideo];
                } else {
                    __weak ViewController *weakSelf = self;
                    [self.cvCamera terminateTimeLapseWithFinish:^(NSURL *url) {
                        NSLog(@"%@", url);
                        weakSelf.startTimeLapse = NO;
                        [weakSelf saveVideoToAlbum:url];
                    }];
                }
                
                [self.mzTimerLabel pause];
            }
                break;
                
            case RECORDSTOP:
                
                break;
                
            default:
                break;
        }
    }
}

- (void)switchCamera {
    CATransition *animation = [CATransition animation];
    animation.duration = .5f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = @"oglFlip";
    animation.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:animation forKey:nil];


    
    if (self.trackingStatusButton.selected) {
        [self switchTracking];
    }

    if (self.captureModeButton.isSelected) {
        if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
            [self.backSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else if (self.cvCamera.cameraPosition == AVCaptureDevicePositionFront) {
            [self.frontSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        if (self.captureModeArrow.isHighlighted) {
            [self.captureModeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else {

        }
    } else {
        if (self.captureModeArrow.isHighlighted) {
            [self.captureModeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else {

        }
    }

    [self.cvCamera switchCamera];
}

- (void)openAlbum {
    
    MWPhotoBrowser *photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    NSArray *allAssetsInAlbum = [CustomAlbum getAllAssetsWithCollectionName:@"RINGO"];
    UIScreen *screen = [UIScreen mainScreen];
    CGFloat scale = screen.scale;
    CGFloat imageSize = MAX(screen.bounds.size.width, screen.bounds.size.height) * 1.5;
    CGSize imageTargetSize = CGSizeMake(imageSize * scale, imageSize * scale);
    CGSize thumbTargetSize = CGSizeMake(imageSize / 3.0 * scale, imageSize / 3.0 * scale);
    
    NSMutableArray *photos = [NSMutableArray array];
    NSMutableArray *thumbs = [NSMutableArray array];
    for (PHAsset *asset in allAssetsInAlbum) {
        [photos addObject:[MWPhoto photoWithAsset:asset targetSize:imageTargetSize]];
        [thumbs addObject:[MWPhoto photoWithAsset:asset targetSize:thumbTargetSize]];
    }
    
    self.photos = photos;
    self.thumbs = thumbs;
    
    photoBrowser.displayActionButton = YES;
    photoBrowser.displayNavArrows = NO;
    photoBrowser.displaySelectionButtons = NO;
    photoBrowser.alwaysShowControls = NO;
    photoBrowser.zoomPhotosToFill = YES;
    photoBrowser.enableGrid = YES;
    photoBrowser.startOnGrid = YES;
    photoBrowser.enableSwipeToDismiss = NO;
    photoBrowser.autoPlayOnAppear = NO;
    [photoBrowser setCurrentPhotoIndex:0];
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:photoBrowser];
    
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

- (void)checkBle {
    
    self.bleDevicesListView.hidden = NO;
    
    if (bleStatus == BLECONNECTED) {
        // show connected device
        
    } else if (bleStatus == BLECONNECTING) {
        // disable this button
        
    } else if (bleStatus == BLEDISCONNECTED) {
        // scan devices
        [[BLEDriven sharedInstance] scanWithFindDevices:^(NSArray *devices) {
#ifdef DEBUG
            NSLog(@"%lu", (unsigned long)devices.count);
#endif
            
            if (_bleDevices.count != devices.count) {
            
                _bleDevices = [NSArray arrayWithArray:devices];
                
                [self.bleDevicesListTableView reloadData];
            
            }
            
        } failedHandler:^(NSString *errorDescription) {
#ifdef DEBUG
            NSLog(@"%@", errorDescription);
#endif
        }];
        
    }
    
    [self.bleDevicesListTableView reloadData];
}

- (void)switchTracking {
    
    if (bleStatus == BLECONNECTED) {

        if ([[SendFramePacket sharedInstance].trackingFlag isEqualToData:[NSString dataWithHexString:@"01"]]) {
            [SendFramePacket sharedInstance].trackingFlag = [NSString dataWithHexString:@"00"];

        } else {
            [SendFramePacket sharedInstance].trackingFlag = [NSString dataWithHexString:@"01"];

        }
        
        
        
//        self.trackingStatusButton.selected = !self.trackingStatusButton.selected;
        
//        self.enableTracking = !self.enableTracking;
//
//        if (!self.trackingStatusButton.selected) {
//            [self stopTracking];
//            
//            [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"00"];
//            [SendFramePacket sharedInstance].xoffset = [NSString dataWithHexString:@"00000000"];
//            [SendFramePacket sharedInstance].yoffset = [NSString dataWithHexString:@"00000000"];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.trackingBox.hidden = YES;
//                
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    if (!self.trackingBox.hidden) {
//                        self.trackingBox.hidden = YES;
//                    }
//                });
//            });
//        }
    }
}

#pragma mark - camera utils

- (void)takePicture {
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.frame];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:flashView];
    [UIView animateWithDuration:.5f
                     animations:^{
                         [flashView setAlpha:0.f];
                         
                     } completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                         
                     }
     ];
    
    [self.cvCamera takePhoto:^(UIImage *image) {
        if (image) {
            [self savePhotoToAlbum:image];
        }
    }];
}

- (void)savePhotoToAlbum:(UIImage *)img {
    
    PHAssetCollection *ringoAblbum = [CustomAlbum getMyAlbumWithName:@"RINGO"];
    if (ringoAblbum) {
        [CustomAlbum addNewAssetWithImage:img toAlbum:ringoAblbum onSuccess:^(NSString *ImageId) {
            NSLog(@"%@", ImageId);
            
            NSLog(@"saved photo");
//            [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"11"];
            
        } onError:^(NSError *error) {
            NSLog(@"save photo failure");
//            [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"91"];
            
        }];
        
    } else {
        
        [CustomAlbum makeAlbumWithTitle:@"RINGO" onSuccess:^(NSString *AlbumId) {
            NSLog(@"create RINGO ablbum success");
            
            [CustomAlbum addNewAssetWithImage:img toAlbum:ringoAblbum onSuccess:^(NSString *ImageId) {
                NSLog(@"%@", ImageId);
                
                NSLog(@"saved photo");
                [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"11"];
                
            } onError:^(NSError *error) {
                NSLog(@"save photo failure");
                [SendFramePacket sharedInstance].commandback = [NSString dataWithHexString:@"91"];
            }];
            
        } onError:^(NSError *error) {
            NSLog(@"create RINGO ablbum failure");
        }];
        
    }
}

- (void)startRecordVideo {
    if (!self.cvCamera.recording) {
        [self.cvCamera startRecord];
        NSLog(@"startRecord");
    }
}

- (void)stopRecordVideo {
    NSLog(@"stopRecord");
    [self.cvCamera stopRecord:^(NSURL *videoTmpUrl) {
        NSLog(@"%@", videoTmpUrl);
        
        [self saveVideoToAlbum:videoTmpUrl];
    }];
}

- (void)saveVideoToAlbum:(NSURL *)videoUrl {
    PHAssetCollection *ringoAblbum = [CustomAlbum getMyAlbumWithName:@"RINGO"];
    if (ringoAblbum) {
        [CustomAlbum addNewAssetWithVideo:videoUrl toAlbum:ringoAblbum onSuccess:^(NSString *ImageId) {
            NSLog(@"%@", ImageId);
            
            // delete tmp video file
            [[NSFileManager defaultManager] removeItemAtPath:videoUrl.path error:nil];
            
        } onError:^(NSError *error) {
            NSLog(@"save video failure");
        }];
        
    } else {
        
        [CustomAlbum makeAlbumWithTitle:@"RINGO" onSuccess:^(NSString *AlbumId) {
            NSLog(@"create RINGO ablbum success");
            
            [CustomAlbum addNewAssetWithVideo:videoUrl toAlbum:ringoAblbum onSuccess:^(NSString *ImageId) {
                // delete tmp video file
                [[NSFileManager defaultManager] removeItemAtPath:videoUrl.path error:nil];
                
            } onError:^(NSError *error) {
                NSLog(@"save video failure");
            }];
            
        } onError:^(NSError *error) {
            NSLog(@"create RINGO ablbum failure");
        }];
        
    }
}

//- (BOOL)shouldAutorotate {
//    return YES;
//}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationLandscapeRight;
//}

//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    NSLog(@"%s", __func__);
//}

//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    NSLog(@"%s", __func__);
//}


//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
//    NSLog(@"%s", __func__);
//    // Map UIDeviceOrientation to UIInterfaceOrientation.
//    UIInterfaceOrientation orient = UIInterfaceOrientationPortrait;
//    switch ([[UIDevice currentDevice] orientation])
//    {
//        case UIDeviceOrientationLandscapeLeft:
//            orient = UIInterfaceOrientationLandscapeLeft;
//            break;
//            
//        case UIDeviceOrientationLandscapeRight:
//            orient = UIInterfaceOrientationLandscapeRight;
//            break;
//            
//        case UIDeviceOrientationPortrait:
//            orient = UIInterfaceOrientationPortrait;
//            break;
//            
//        case UIDeviceOrientationPortraitUpsideDown:
//            orient = UIInterfaceOrientationPortraitUpsideDown;
//            break;
//            
//        case UIDeviceOrientationFaceUp:
//        case UIDeviceOrientationFaceDown:
//        case UIDeviceOrientationUnknown:
//            // When in doubt, stay the same.
//            orient = fromInterfaceOrientation;
//            break;
//    }
//}

#pragma mark - UITableViewDelegate 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.bleDevicesListTableView) {
    
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        NSString *deviceName = ((CBPeripheral *)(self.bleDevices[indexPath.row])).name;
        
        if (![deviceName isEqualToString:self.connectedDevice]) {
            
            bleStatus = BLEDISCONNECTED;
            
            if (self.connectedDeviceIndexPath) {
                // no check connected device
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:self.connectedDeviceIndexPath];
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                // disconnect this device
                [[BLEDriven sharedInstance] disconnectToDevice:self.bleDevices[self.connectedDeviceIndexPath.row]];
                
                
                // clear something
                self.connectedDevice = nil;
                self.connectedDeviceIndexPath = nil;
            }
            
            // select a device to connect
            
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            spinner.frame = CGRectMake(0, 0, 24, 24);
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryView = spinner;
            [spinner startAnimating];
            
            [[BLEDriven sharedInstance] connectToDevice:self.bleDevices[indexPath.row] success:^(CBPeripheral *peripheral) {
                NSLog(@"%@", peripheral.name);
                
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                [spinner stopAnimating];
                cell.accessoryView = nil;
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                self.connectedDevice = ((CBPeripheral *)(self.bleDevices[indexPath.row])).name;
                self.connectedDeviceIndexPath = indexPath;
                bleStatus = BLECONNECTED;
                
                // auto hiden llist
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [_closeBLEDevicesListViewButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                });
                
            } failedHandler:^(NSString *errorDescription) {
                NSLog(@"%@", errorDescription);
            }];
        }
        
    } else if (tableView == self.filterTableView) {
        if (self.selectedFilterIndexPath) {
            if (self.selectedFilterIndexPath == indexPath) {
                return;
            }
            FilterTableViewCell *cell = [tableView cellForRowAtIndexPath:self.selectedFilterIndexPath];
            cell.filtedImage.layer.borderWidth = .0f;
        }
        self.selectedFilterIndexPath = indexPath;
        
        FilterTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.filtedImage.layer.borderColor = [UIColor orangeColor].CGColor;
        cell.filtedImage.layer.borderWidth = 2.f;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            GPUImageOutput<GPUImageInput> *filter = [[NSClassFromString(self.filtersTableViewModel.filters[indexPath.row]) alloc] init];
            [self.cvCamera switchFilter:filter];
        });
        
    } else if (tableView == self.cameraSettingsTableView) {
        CameraSettingsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (indexPath.row == 4) {
            return;
        }
        
        if (self.lastClickCameraSettingIndexPath) {
            if (self.lastClickCameraSettingIndexPath != indexPath && self.cameraSettingOpened) {
                self.cameraSettingOpened = !self.cameraSettingOpened;
                CameraSettingsTableViewCell *cell = [tableView cellForRowAtIndexPath:self.lastClickCameraSettingIndexPath];
                [UIView animateWithDuration:.5f animations:^{
                    cell.more.transform = CGAffineTransformRotate(cell.more.transform, -M_PI_2);
                    
                    [self.cameraSettingsDetailTableView removeFromSuperview];
                    [cell.secondTableViewContainer mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.mas_equalTo(@(0));
                    }];
                } completion:^(BOOL finished) {
                    
                }];
            }
        }
        
        self.lastClickCameraSettingIndexPath = indexPath;
        
        self.cameraSettingOpened = !self.cameraSettingOpened;
        if (self.cameraSettingOpened) {
            __weak ViewController *weakSelf = self;
            [UIView animateWithDuration:.5f animations:^{
                cell.more.transform = CGAffineTransformRotate(cell.more.transform, M_PI_2);
                
                self.cameraSettingsDetailTableViewModel.settingsKind = self.cameraSettingsDetailTableViewModel.cameraSettingsKinds[cell.title.text];
                self.cameraSettingsDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                [cell.secondTableViewContainer addSubview:self.cameraSettingsDetailTableView];
                [self.cameraSettingsDetailTableView reloadData];
                [self.cameraSettingsDetailTableView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(cell.secondTableViewContainer).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
                    [cell.secondTableViewContainer mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.mas_equalTo(@(44*weakSelf.cameraSettingsDetailTableViewModel.settingsKind[@"titles"].count));
                        [tableView reloadData];
                    }];
                }];
            } completion:^(BOOL finished) {
                
            }];
        } else {
            
            [UIView animateWithDuration:.5f animations:^{
                cell.more.transform = CGAffineTransformRotate(cell.more.transform, -M_PI_2);
                
                [self.cameraSettingsDetailTableView removeFromSuperview];
                [cell.secondTableViewContainer mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(@(0));
                    [tableView reloadData];
                }];
            } completion:^(BOOL finished) {
                
            }];
        }
    
    } else if (tableView == self.cameraSettingsDetailTableView) {
        CameraSettingsDetailTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        UIImage *image = cell.image.image;
        if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_videoformat_1280x720_30p"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentVideoResolution = @"advanced_more_videoformat_1280x720_30p";
                [self.cvCamera changeFormat:CvCameraFormat720p30];
            } else {
                NSLog(@"front camera not support change resolution");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support change resolution"];
            }
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_videoformat_1920x1080_30p"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentVideoResolution = @"advanced_more_videoformat_1920x1080_30p";
                [self.cvCamera changeFormat:CvCameraFormat1080p30];
            } else {
                NSLog(@"front camera not support change resolution");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support change resolution"];
            }
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_videoformat_1920x1080_60p"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentVideoResolution = @"advanced_more_videoformat_1920x1080_60p";
                [self.cvCamera changeFormat:CvCameraFormat1080p60];
            } else {
                NSLog(@"front camera not support change resolution");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support change resolution"];
            }
        }
        
        else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_whitebalance_auto"]]) {
            self.currentWhiteBalance = @"advanced_more_whitebalance_auto";
            [self.cvCamera changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeAuto];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_whitebalance_outdoor"]]) {
            self.currentWhiteBalance = @"advanced_more_whitebalance_outdoor";
            [self.cvCamera changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeSunny];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_whitebalance_inhouse"]]) {
            self.currentWhiteBalance = @"advanced_more_whitebalance_inhouse";
            [self.cvCamera changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeCloudy];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_whitebalance_tungsten"]]) {
            self.currentWhiteBalance = @"advanced_more_whitebalance_tungsten";
            [self.cvCamera changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeIncandescent];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_whitebalance_neon"]]) {
            self.currentWhiteBalance = @"advanced_more_whitebalance_neon";
            [self.cvCamera changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeFluorescent];
        }
        
        else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_none"]]) {
            self.currentGrid = @"advanced_more_none";
            [self changeShowGrid:CameraPreviewGridNone];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_grid_sudoku"]]) {
            self.currentGrid = @"advanced_more_grid_sudoku";
            [self changeShowGrid:CameraPreviewGridSudoku];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_grid_diagonal"]]) {
            self.currentGrid = @"advanced_more_grid_diagonal";
            [self changeShowGrid:CameraPreviewGridDiagonal];
        } else if ([image isEqualToImage:[UIImage imageNamed:@"advanced_more_grid_center"]]) {
            self.currentGrid = @"advanced_more_grid_center";
            [self changeShowGrid:CameraPreviewGridCenter];
        }
        
        else if ([image isEqualToImage:[UIImage imageNamed:@"lp_camera_flash_off"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentFlash = @"lp_camera_flash_off";
                [self.cvCamera cameraFlashMode:CvCameraFlashModeOff];
            } else {
                NSLog(@"front camera not support flash");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support flash"];
            }
        } else if ([image isEqualToImage:[UIImage imageNamed:@"lp_camera_flash_on"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentFlash = @"lp_camera_flash_on";
                [self.cvCamera cameraFlashMode:CvCameraFlashModeOn];
            } else {
                NSLog(@"front camera not support flash");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support flash"];
            }
        } else if ([image isEqualToImage:[UIImage imageNamed:@"lp_camera_flash_auto"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentFlash = @"lp_camera_flash_auto";
                [self.cvCamera cameraFlashMode:CvCameraFlashModeAuto];
            } else {
                NSLog(@"front camera not support flash");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support flash"];
            }
        } else if ([image isEqualToImage:[UIImage imageNamed:@"lp_camera_flash_torch"]]) {
            if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
                self.currentFlash = @"lp_camera_flash_torch";
                [self.cvCamera cameraFlashMode:CvCameraFlashModeAlwayOn];
            } else {
                NSLog(@"front camera not support flash");
                [MBProgressHUD toast:self.view toastStr:@"front camera not support flash"];
            }
        }
        
        [self.cameraSettingsTableView reloadData];
        [self.cameraSettingsDetailTableView reloadData];
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.bleDevicesListTableView) {
        return self.bleDevices.count;
        
    } else if (tableView == self.filterTableView) {
        return self.filtersTableViewModel.filtersName.count;
        
    } else if (tableView == self.cameraSettingsTableView) {
        if (self.cameraManuelSettingsOpened) {
            return self.cameraSettingsTableViewModel.titles.count+1;
        } else {
            return self.cameraSettingsTableViewModel.titles.count;
        }
        
    } else if (tableView == self.cameraSettingsDetailTableView) {
        return self.cameraSettingsDetailTableViewModel.settingsKind[@"titles"].count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.bleDevicesListTableView) {
        
        BleDevicesListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        if (!cell) {
            cell = [[BleDevicesListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        }
        
        cell.bleDevicesName.text = ((CBPeripheral *)(self.bleDevices[indexPath.row])).name;
        if (self.connectedDevice && [self.connectedDevice isEqualToString:cell.bleDevicesName.text]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        return cell;
        
    } else if (tableView == self.filterTableView) {
        FilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        if (!cell) {
            cell = [[FilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        }
        cell.filterName.text = self.filtersTableViewModel.filtersName[indexPath.row];
        cell.filtedImage.image = self.filtersTableViewModel.filtersImage[indexPath.row];
        
        if (self.selectedFilterIndexPath != indexPath) {
            cell.filtedImage.layer.borderWidth = .0f;
        } else {
            cell.filtedImage.layer.borderColor = [UIColor orangeColor].CGColor;
            cell.filtedImage.layer.borderWidth = 2.0f;
        }
        if (!self.selectedFilterIndexPath && indexPath.row == 0) {
            cell.filtedImage.layer.borderColor = [UIColor orangeColor].CGColor;
            cell.filtedImage.layer.borderWidth = 2.0f;
        }
        return cell;
        
    } else if (tableView == self.cameraSettingsTableView) {
        CameraSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        if (!cell) {
            cell = [[CameraSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        }
        for (UIView *v in cell.contentView.subviews) {
            if (v.tag == 1000 || v.tag == 10001) {
                [v removeFromSuperview];
                break;
            }
        }
        
        switch (indexPath.row) {
            case 0:
                cell.title.text = self.cameraSettingsTableViewModel.titles[indexPath.row];
                cell.more.image = [UIImage imageNamed:@"advanced_more_sharpness"];
                if (self.currentVideoResolution) {
                    cell.image.image = [UIImage imageNamed:self.currentVideoResolution];
                } else {
                    cell.image.image = [UIImage imageNamed:@"advanced_more_videoformat_1280x720_30p"];
                }
                break;
                
            case 1:
                cell.title.text = self.cameraSettingsTableViewModel.titles[indexPath.row];
                cell.more.image = [UIImage imageNamed:@"advanced_more_sharpness"];
                if (self.currentWhiteBalance) {
                    cell.image.image = [UIImage imageNamed:self.currentWhiteBalance];
                } else {
                    cell.image.image = [UIImage imageNamed:@"advanced_more_whitebalance_auto"];
                }
                break;
                
            case 2:
                cell.title.text = self.cameraSettingsTableViewModel.titles[indexPath.row];
                cell.more.image = [UIImage imageNamed:@"advanced_more_sharpness"];
                if (self.currentGrid) {
                    cell.image.image = [UIImage imageNamed:self.currentGrid];
                } else {
                    cell.image.image = [UIImage imageNamed:@"advanced_more_none"];
                }
                break;
                
            case 3:
                cell.title.text = self.cameraSettingsTableViewModel.titles[indexPath.row];
                cell.more.image = [UIImage imageNamed:@"advanced_more_sharpness"];
                if (self.currentFlash) {
                    cell.image.image = [UIImage imageNamed:self.currentFlash];
                } else {
                    cell.image.image = [UIImage imageNamed:@"lp_camera_flash_off"];
                }
                break;
                
            case 4: {
                cell.title.text = self.cameraSettingsTableViewModel.titles[indexPath.row];
                cell.image.image = nil;
                cell.more.image = nil;
                
                UISwitch *uiSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
                if (self.cameraManuelSettingsOpened) {
                    uiSwitch.on = YES;
                }
                [uiSwitch addTarget:self action:@selector(switchCameraManuelSettings:) forControlEvents:UIControlEventValueChanged];
                uiSwitch.tag = 1000;
                uiSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
                [cell.contentView addSubview:uiSwitch];
                [uiSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(cell.title);
                    make.right.equalTo(cell.contentView).with.offset(-10);
                }];
                
                }
                break;
                
            case 5: {
                CameraManuleSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID2" forIndexPath:indexPath];
                if (!cell) {
                    cell = [[CameraManuleSettingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID2"];
                }
                for (int i = 0; i < cell.cameraManuleSettingPickerView.numberOfComponents; i++) {
                    [cell.cameraManuleSettingPickerView selectRow:0 inComponent:i animated:NO];
                }
                
                return cell;
                }
                break;
                
            default:
                
                break;
        }
        return cell;
        
    } else if (tableView == self.cameraSettingsDetailTableView) {
        CameraSettingsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
        if (!cell) {
            cell = [[CameraSettingsDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
        }
        cell.title.text = self.cameraSettingsDetailTableViewModel.settingsKind[@"titles"][indexPath.row];
        cell.image.image = self.cameraSettingsDetailTableViewModel.settingsKind[@"images"][indexPath.row];
        cell.checkmark.hidden = YES;
        NSArray<UIView *> *vs = cell.superview.superview.superview.superview.subviews;
        for (UIView *v in vs) {
            if ([v isKindOfClass:UIImageView.class]) {
                UIImageView *imageView = (UIImageView *)v;
                if ([imageView.image isEqualToImage:cell.image.image]) {
                    cell.checkmark.hidden = NO;
                }
            }
        }
        
        return cell;
    }
    
    return nil;
}

#pragma mark - observeValueForKeyPath
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    id newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"pureData"]) {
        NSData *pureData = (NSData *)newValue;
        NSData *flag = nil;
        
        if (self.detected) {
            [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"01"];
            [SendFramePacket sharedInstance].xoffset = [pureData subdataWithRange:NSMakeRange(0, 4)];
            [SendFramePacket sharedInstance].yoffset = [pureData subdataWithRange:NSMakeRange(4, 4)];
        } else {
            [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"00"];
            [SendFramePacket sharedInstance].xoffset = [NSString dataWithHexString:@"00000000"];
            [SendFramePacket sharedInstance].yoffset = [NSString dataWithHexString:@"00000000"];
        }
        
        NSMutableData *payload = [NSMutableData data];
        [payload appendData:flag];
        [payload appendData:pureData];
        
//        NSLog(@"%@", payload);
        [SendFramePacket sharedInstance].payload = payload;
        
    } else if ([keyPath isEqualToString:@"text"] &&
               [object isKindOfClass:[MZTimerLabel class]]) {
//        if (maxRecordTime && self.mzTimerLabel) {
//            self.limitRecordTimeLabel.text = [NSString stringWithFormat:@"%@ | %@", self.mzTimerLabel.text, maxRecordTime];
//            
//            if ([self.mzTimerLabel.text isEqualToString:maxRecordTime]) {
//                [self.mzTimerLabel pause];
//                [self captureAction];
//            }
//        }
        
        if (self.mzTimerLabel) {
            uint64_t free = [self getFreeDiskspace];
            
            if (free < 100) {
                [self.mzTimerLabel pause];
                [self captureAction];
            }
            
            if (free / 1024 > 0) {
                self.limitRecordTimeLabel.text = [NSString stringWithFormat:@"%@ | %.2fGB", self.mzTimerLabel.text, free / 1024.0];
            } else {
                self.limitRecordTimeLabel.text = [NSString stringWithFormat:@"%@ | %.2fMB", self.mzTimerLabel.text, free];
            }
        }
        
    } else if ([keyPath isEqualToString:@"takePhotoButtonStatus"]) {
        if ([newValue boolValue]) {
//            [self captureAction];
        }
        
    } else if ([keyPath isEqualToString:@"cameraMode"]) {
        if (self.cameraMode == CAPTURE) {
            NSLog(@"photoMode");
            [_takePhotoButton setImage:[UIImage imageNamed:@"ic_leftbar_photo_normal"] forState:UIControlStateNormal];
            [_takePhotoButton setImage:[UIImage imageNamed:@"ic_leftbar_photo_tab"] forState:UIControlStateHighlighted];
            
        } else if (self.cameraMode == RECORD) {
            NSLog(@"videoMode");
            [_takePhotoButton setImage:[UIImage imageNamed:@"ic_leftbar_video_normal"] forState:UIControlStateNormal];
            [_takePhotoButton setImage:[UIImage imageNamed:@"ic_leftbar_video_tab"] forState:UIControlStateHighlighted];
            [_takePhotoButton setImage:[UIImage imageNamed:@"ic_leftbar_video_release"] forState:UIControlStateSelected];
        }
        
    } else if ([keyPath isEqualToString:@"cameraPosition"]) {
        if (self.cameraPosition == FRONT) {
            NSLog(@"front carmera");
            [self switchCamera];
        } else if (self.cameraPosition == BACK) {
            NSLog(@"back carmera");
            [self switchCamera];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.currentFlash = @"lp_camera_flash_off";
                [self.cvCamera cameraFlashMode:CvCameraFlashModeOff];
                self.currentVideoResolution = @"advanced_more_videoformat_1280x720_30p";
                [self.cvCamera changeFormat:CvCameraFormat720p30];
                [self.cameraSettingsTableView reloadData];
                [self.cameraSettingsDetailTableView reloadData];
            });
        }
        
    } else if ([keyPath isEqualToString:@"enableTracking"]) {
        if (canSwitchTrackingStatus) {
            canSwitchTrackingStatus = false;
            
            if ([newValue boolValue]) {
                [SendFramePacket sharedInstance].trackingFlag = [NSString dataWithHexString:@"01"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    canSwitchTrackingStatus = true;
                });
            } else {
                [SendFramePacket sharedInstance].trackingFlag = [NSString dataWithHexString:@"00"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    canSwitchTrackingStatus = true;
                });
            }
        }
        
        
    } else if ([keyPath isEqualToString:@"faceFollow"]) {
        if ([newValue boolValue]) {
            self.trackingStatusButton.selected = YES;
            
            self.enableTracking = YES;
            
            if (!self.trackingStatusButton.selected) {
                [self stopTracking];
                
                [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"00"];
                [SendFramePacket sharedInstance].xoffset = [NSString dataWithHexString:@"00000000"];
                [SendFramePacket sharedInstance].yoffset = [NSString dataWithHexString:@"00000000"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.trackingBox.hidden = YES;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (!self.trackingBox.hidden) {
                            self.trackingBox.hidden = YES;
                        }
                    });
                });
            }
            
        } else {
            self.trackingStatusButton.selected = NO;
            
            self.enableTracking = NO;
            
            if (!self.trackingStatusButton.selected) {
                [self stopTracking];
                
                [SendFramePacket sharedInstance].trackingQuailty = [NSString dataWithHexString:@"00"];
                [SendFramePacket sharedInstance].xoffset = [NSString dataWithHexString:@"00000000"];
                [SendFramePacket sharedInstance].yoffset = [NSString dataWithHexString:@"00000000"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.trackingBox.hidden = YES;
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (!self.trackingBox.hidden) {
                            self.trackingBox.hidden = YES;
                        }
                    });
                });
            }
        }
        
    }
}

- (IBAction)livingAction:(id)sender {
    UIButton *button = sender;
    
    if ([button.titleLabel.text isEqualToString:@"start living"]) {
        
        [self startLiveConfirmAlert:^(NSString *url) {
//            if (confirm) {
//                
//                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                hud.mode = MBProgressHUDModeIndeterminate;
//                [button setTitle:@"连接中" forState:UIControlStateNormal];
//                
//                // connect
//                if (!self.rtmpClient) {
//                    self.rtmpClient = [RTMPClient sharedInstance];
//                }
//                
//                NSString *publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
//                if (!publishUrl) {
//                    [[NSUserDefaults standardUserDefaults] setObject:@"rtmp://117.78.39.184/live-ceewa" forKey:@"publishUrl"];
//                    publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
//                }
//                publishUrl = @"rtmp://192.168.20.56:1935/gzhm/room";
//                [self.rtmpClient connect:publishUrl success:^(BOOL success) {
//                    if (success) {
//                        
//                        self.cvCamera.startEncode = YES;
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            {// show user id
//                                NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
//                                if (userId) {
//                                    self.userId_live.text = [NSString stringWithFormat:@"用户ID:%@", userId];
//                                } else {
//                                    self.userId_live.text = [NSString stringWithFormat:@"用户ID:无"];
//                                }
//                                self.userId_live.hidden = NO;
//                            }
//                            
//                            {
//                                [self.mzTimerLabel_live reset];
//                                [self.mzTimerLabel_live start];
//                                self.livingTime.hidden = NO;
//                            }
//                            
//                            hud.hidden = YES;
//                            
//                            [button setTitle:@"直播中" forState:UIControlStateNormal];
//                            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                            [button setBackgroundColor:[UIColor redColor]];
//                            self.liveSettingButton.enabled = NO;
//                            self.albumButton.enabled = NO;
//                        });
//                    } else {
//                        hud.mode = MBProgressHUDModeText;
//                        hud.labelText = @"连接失败";
//                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                            hud.hidden = YES;
//                        });
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [button setTitle:@"开始直播" forState:UIControlStateNormal];
//                        });
//                        
//                        NSLog(@"连接失败");
//                        //                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                        //                [button setBackgroundColor:[UIColor whiteColor]];
//                        self.liveSettingButton.enabled = YES;
//                        self.albumButton.enabled = YES;
////                        [MBProgressHUD toast:self.view toastStr:@"连接失败"];
//                    }
//                }];
//            }
            
            if (url) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeIndeterminate;
                [button setTitle:@"connecting..." forState:UIControlStateNormal];
                
                if (!self.rtmpClient) {
                    self.rtmpClient = [RTMPClient sharedInstance];
                }
                
                [self.rtmpClient connect:@"rtmp://192.168.1.77:1935/gzhm/room" success:^(BOOL success) {
                    if (success) {

                        self.cvCamera.startEncode = YES;

                        dispatch_async(dispatch_get_main_queue(), ^{
//                            {// show user id
//                                NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
//                                if (userId) {
//                                    self.userId_live.text = [NSString stringWithFormat:@"用户ID:%@", userId];
//                                } else {
//                                    self.userId_live.text = [NSString stringWithFormat:@"用户ID:无"];
//                                }
//                                self.userId_live.hidden = NO;
//                            }

                            {
                                [self.mzTimerLabel_live reset];
                                [self.mzTimerLabel_live start];
//                                self.livingTime.hidden = NO;
                            }

                            hud.hidden = YES;

                            
                            self.livingTimeChanged = ^(NSString *timeString) {
                                [UIView performWithoutAnimation:^{
                                    [button setTitle:timeString forState:UIControlStateNormal];
                                    [button layoutIfNeeded];
                                }];
                            };
                            
                            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                            [button setBackgroundColor:[UIColor redColor]];
                            self.liveSettingButton.enabled = NO;
                            self.albumButton.enabled = NO;
                        });
                    } else {
                        hud.mode = MBProgressHUDModeText;
                        hud.labelText = @"connect faild";
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            hud.hidden = YES;
                        });

                        dispatch_async(dispatch_get_main_queue(), ^{
                            [button setTitle:@"start living" forState:UIControlStateNormal];
                        });
                        

                        //                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        //                [button setBackgroundColor:[UIColor whiteColor]];
                        self.liveSettingButton.enabled = YES;
                        self.albumButton.enabled = YES;
//                        [MBProgressHUD toast:self.view toastStr:@"连接失败"];
                    }
                }];
            }
        }];

    } else {
        
        [self stopLiveConfirmAlert:^(BOOL confirm) {
            if (confirm) {
                self.userId_live.hidden = YES;
                
                {
                    [self.mzTimerLabel_live pause];
                    self.livingTime.hidden = YES;
                }
                
                // close
                
                self.cvCamera.startEncode = NO;
                
                if (self.rtmpClient.state == LIVING) {
                    [self.rtmpClient close];
                }
                [button setTitle:@"start living" forState:UIControlStateNormal];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor whiteColor]];
                self.liveSettingButton.enabled = YES;
                self.albumButton.enabled = YES;
            }
        }];
        
    }
}

- (IBAction)FlashModeSwitchAction:(id)sender {
    
    UISwitch *_switch = (UISwitch *)sender;
    
//    if (self.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
//        
//        if (_switch.on) {
//            [self.cvCamera cameraFlashMode:CvCameraFlashModeOn];
//        } else {
//            [self.cvCamera cameraFlashMode:CvCameraFlashModeOff];
//        }
//    } else {
//        _switch.on = NO;
//    }
    
    if (_switch.on) {
        [self changeShowGrid:CameraPreviewGridCenter];
    } else {
        [self changeShowGrid:CameraPreviewGridNone];
    }
}

- (void)changeShowGrid:(CameraPreviewGrid)gridMode {
    __weak ViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.gridLayer) {
            [weakSelf.gridLayer removeFromSuperlayer];
        }
        
        switch (gridMode) {
            case CameraPreviewGridNone:
                break;
                
            case CameraPreviewGridCenter:
                weakSelf.gridLayer = [weakSelf drawCenter];
                [weakSelf.view.layer insertSublayer:weakSelf.gridLayer atIndex:0];
                break;
                
            case CameraPreviewGridDiagonal:
                weakSelf.gridLayer = [weakSelf drawDiagonal];
                [weakSelf.view.layer insertSublayer:weakSelf.gridLayer atIndex:0];
                break;
                
            case CameraPreviewGridSudoku:
                weakSelf.gridLayer = [weakSelf drawSudoku];
                [weakSelf.view.layer insertSublayer:weakSelf.gridLayer atIndex:0];
                break;
                
            default:
                break;
        }
    });
}

- (CAShapeLayer *)drawDiagonal {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float space = .0f;
    space = self.view.frame.size.width / 3;
    for (int i = 1; i < 3; i++) {
        CGPoint u = CGPointMake(space * i, 0);
        CGPoint d = CGPointMake(space * i, self.view.frame.size.height);
        
        [path moveToPoint:u];
        [path addLineToPoint:d];
    }
    
    space = self.view.frame.size.height / 3;
    for (int i = 1; i < 3; i++) {
        CGPoint l = CGPointMake(0, space * i);
        CGPoint r = CGPointMake(self.view.frame.size.width, space * i);
        
        [path moveToPoint:l];
        [path addLineToPoint:r];
    }
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shapeLayer.lineWidth = .5f;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    return shapeLayer;
}

- (CAShapeLayer *)drawSudoku {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float space = .0f;
    space = self.view.frame.size.width / 3;
    for (int i = 1; i < 3; i++) {
        CGPoint u = CGPointMake(space * i, 0);
        CGPoint d = CGPointMake(space * i, self.view.frame.size.height);
        
        [path moveToPoint:u];
        [path addLineToPoint:d];
    }
    
    space = self.view.frame.size.height / 3;
    for (int i = 1; i < 3; i++) {
        CGPoint l = CGPointMake(0, space * i);
        CGPoint r = CGPointMake(self.view.frame.size.width, space * i);
        
        [path moveToPoint:l];
        [path addLineToPoint:r];
    }
    
    CGPoint lu = CGPointMake(0, 0);
    CGPoint rd = CGPointMake(self.view.frame.size.width, self.view.frame.size.height);
    [path moveToPoint:lu];
    [path addLineToPoint:rd];
    
    CGPoint ld = CGPointMake(0, self.view.frame.size.height);
    CGPoint ru = CGPointMake(self.view.frame.size.width, 0);
    [path moveToPoint:ld];
    [path addLineToPoint:ru];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shapeLayer.lineWidth = .5f;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    return shapeLayer;
}

- (CAShapeLayer *)drawCenter {
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    
    CGPoint center = CGPointMake(width/2, height/2);
    float radius = 15.f;
    [path addArcWithCenter:center radius:radius startAngle:0.f endAngle:2*M_PI clockwise:YES];
    
    CGPoint A = CGPointMake(center.x, center.y-radius);
    CGPoint A1 = CGPointMake(A.x, A.y-8);
    [path moveToPoint:A];
    [path addLineToPoint:A1];
    
    CGPoint B = CGPointMake(center.x+radius, center.y);
    CGPoint B1 = CGPointMake(B.x+8, B.y);
    [path moveToPoint:B];
    [path addLineToPoint:B1];
    
    CGPoint C = CGPointMake(center.x, center.y+radius);
    CGPoint C1 = CGPointMake(C.x, C.y+8);
    [path moveToPoint:C];
    [path addLineToPoint:C1];
    
    CGPoint D = CGPointMake(center.x-radius, center.y);
    CGPoint D1 = CGPointMake(D.x-8, D.y);
    [path moveToPoint:D];
    [path addLineToPoint:D1];
    
    float radius2 = 5.f;
    [path moveToPoint:CGPointMake(center.x+radius2, center.y)];
    [path addArcWithCenter:center radius:radius2 startAngle:0.f endAngle:2*M_PI clockwise:YES];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shapeLayer.lineWidth = 1.f;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    return shapeLayer;
}

- (void)stopLiveConfirmAlert:(void (^)(BOOL confirm))confirm {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"stop living" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        confirm(NO);
    }];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        confirm(YES);
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)startLiveConfirmAlert:(void (^)(NSString *url))confirm {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"start living" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"please input rtmp publish url";
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alertController.textFields.firstObject;
        
        confirm(textField.text);
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)inputLivePublishUrl:(void (^)(NSString *url))finish {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"请输入推流地址" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        NSString *publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
        if (!publishUrl) {
            [[NSUserDefaults standardUserDefaults] setObject:@"rtmp://hwbztl.8686c.com/digitalincloud/" forKey:@"publishUrl"];
            publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
        }
        textField.text = publishUrl;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *urlTextField = alertController.textFields.firstObject;
        [[NSUserDefaults standardUserDefaults] setObject:urlTextField.text forKey:@"publishUrl"];
        finish(urlTextField.text);
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark utils

- (void)checkAlbum {
    PHAssetCollection *ringoAblbum = [CustomAlbum getMyAlbumWithName:@"RINGO"];
    if (!ringoAblbum) {
        
        [CustomAlbum makeAlbumWithTitle:@"RINGO" onSuccess:^(NSString *AlbumId) {
            NSLog(@"create RINGO ablbum success");
            
        } onError:^(NSError *error) {
            NSLog(@"create RINGO ablbum failure");
        }];
        
    }
}

-(uint64_t)getFreeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
//        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
//        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return ((totalFreeSpace/1024ll)/1024ll);
}

- (NSString *)secondsToHHMMSS:(NSInteger)ti {
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (IBAction)showFiltersAction:(UIButton *)sender {
    if (self.cvCamera.currentCvCameraFormat == CvCameraFormatSlowMotion) return;
    
    if (self.cvCamera.recording) return;
    
    if (self.settingBackView.isHidden) {
        self.settingBackView.hidden = NO;
        
        [self prepareFilterTableView];
        
        UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [sender setImage:image forState:UIControlStateNormal];
        
    } else {
        if (self.showFiltersButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.filterTableView removeFromSuperview];
            
            self.settingBackView.hidden = YES;
            
            UIImage *image = [self.showFiltersButton.currentImage imageWithRenderingMode:UIImageRenderingModeAutomatic];
            [self.showFiltersButton setImage:image forState:UIControlStateNormal];
            
        } else if (self.showCameraSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showCameraSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareFilterTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.gimbalSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.gimbalSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareFilterTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.generalSettingButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.generalSettingButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareFilterTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
        }
    }
}

- (void)prepareFilterTableView {
    self.filterTableView.rowHeight = UITableViewAutomaticDimension;
    self.filterTableView.estimatedRowHeight = 70.0f;
    self.filterTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.settingBackView addSubview:self.filterTableView];
    [self.filterTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.settingBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (IBAction)showCameraSettingsAction:(UIButton *)sender {
    if (self.cvCamera.currentCvCameraFormat == CvCameraFormatSlowMotion) return;
    
    if (self.cvCamera.recording) return;
    
    if (self.settingBackView.isHidden) {
        self.settingBackView.hidden = NO;
        
        [self prepareCameraSettingsTableView];
        
        UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [sender setImage:image forState:UIControlStateNormal];
        
    } else {
        if (self.showFiltersButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showFiltersButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareCameraSettingsTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.showCameraSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.cameraSettingsTableView removeFromSuperview];
            
            self.settingBackView.hidden = YES;
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAutomatic];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.gimbalSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.gimbalSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareCameraSettingsTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.generalSettingButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.generalSettingButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareCameraSettingsTableView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
        }
    }
}

- (void)prepareCameraSettingsTableView {
    self.cameraSettingsTableView.rowHeight = UITableViewAutomaticDimension;
    self.cameraSettingsTableView.estimatedRowHeight = 44.0f;
    self.cameraSettingsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.settingBackView addSubview:self.cameraSettingsTableView];
    [self.cameraSettingsTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.settingBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (IBAction)showGimbalSettingsAction:(UIButton *)sender {
    
    if (self.settingBackView.isHidden) {
        
        self.settingBackView.hidden = NO;
        
        [self prepareGimbalSettingsBackView];

        UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [sender setImage:image forState:UIControlStateNormal];
        
    } else {
        if (self.showFiltersButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showFiltersButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGimbalSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.showCameraSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showCameraSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGimbalSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.gimbalSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.gimbalSettingsBackView removeFromSuperview];
            
            self.settingBackView.hidden = YES;
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAutomatic];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.generalSettingButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.generalSettingButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGimbalSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
        }
    }
}

- (void)prepareGimbalSettingsBackView{
    self.gimbalSettingsBackView.gimbalSceneChangedBlock = ^(NSString *scene) {
        NSLog(@"gimbalScene %@", scene);
        if ([scene isEqualToString:@"Walk"]) {
            [SendFramePacket sharedInstance].sceneMode = [NSString dataWithHexString:@"00"];
        } else if ([scene isEqualToString:@"Sport"]) {
            [SendFramePacket sharedInstance].sceneMode = [NSString dataWithHexString:@"01"];
        }
    };
    [self.settingBackView addSubview:self.gimbalSettingsBackView];
    [self.gimbalSettingsBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.settingBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (IBAction)showGeneralSettingsAction:(UIButton *)sender {
    if (self.settingBackView.isHidden) {
        self.settingBackView.hidden = NO;
        
        [self prepareGeneralSettingsBackView];
        
        UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [sender setImage:image forState:UIControlStateNormal];
        
    } else {
        if (self.showFiltersButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showFiltersButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGeneralSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.showCameraSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.showCameraSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGeneralSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.gimbalSettingsButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.gimbalSettingsButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            
            self.settingBackView.hidden = NO;
            
            [self prepareGeneralSettingsBackView];
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [sender setImage:image forState:UIControlStateNormal];
            
        } else if (self.generalSettingButton.currentImage.renderingMode == UIImageRenderingModeAlwaysTemplate) {
            [self.generalSettingsBackView removeFromSuperview];
            
            self.settingBackView.hidden = YES;
            
            UIImage *image = [sender.currentImage imageWithRenderingMode:UIImageRenderingModeAutomatic];
            [sender setImage:image forState:UIControlStateNormal];
        }
    }
}

- (void)prepareGeneralSettingsBackView {
    __weak ViewController *weakSelf = self;
    self.generalSettingsBackView.openLiving = ^(UISwitch *switcher) {
        if (switcher.on) {
            if (self.cvCamera.currentCvCameraFormat == CvCameraFormatSlowMotion) {
                switcher.on = NO;
                
                return;
            }
            weakSelf.startLivingButton.hidden = NO;
            
        } else {
            
            weakSelf.startLivingButton.hidden = YES;
        }
    };
    [self.settingBackView addSubview:self.generalSettingsBackView];
    [self.generalSettingsBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.settingBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)switchCameraManuelSettings:(UISwitch *)uiSwitch {
    if (uiSwitch.on) {
//        NSLog(@"ON");
        self.cameraManuelSettingsOpened = YES;
    } else {
//        NSLog(@"OFF");
        self.cameraManuelSettingsOpened = NO;
        [self.cvCamera changeToDefault];
    }
    
    [self.cameraSettingsTableView reloadData];
}

#pragma mark - UIPickerView delegate

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    UILabel *title = (UILabel *)view;
    
    if (!title) {
        title = [[UILabel alloc] init];
        
        title.font = [UIFont systemFontOfSize:16];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
    }
    
    switch (component) {
        case 0:
            title.text = [NSString stringWithFormat:@"%d", [self.cvCamera.isoRange[row] intValue]];
            break;
            
        case 1:
            title.text = [NSString stringWithFormat:@"1/%d", [self.cvCamera.shutterRange[row] intValue]];
            break;
            
        case 2:
            title.text = [NSString stringWithFormat:@"%d", [self.cvCamera.whiteBalanceRange[row] intValue]];
            break;
            
        default:
            break;
    }
    
    if ([title.text isEqualToString:@"0"] || [title.text isEqualToString:@"1/0"]) {
        title.text = @"Auto";
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch (component) {
        case 0:
            NSLog(@"%@", self.cvCamera.isoRange[row]);
            
            [self.cvCamera changeISO:[self.cvCamera.isoRange[row] intValue]];
            break;
            
        case 1:
            NSLog(@"%@", self.cvCamera.shutterRange[row]);
            
            [self.cvCamera changeShutter:[self.cvCamera.shutterRange[row] intValue]];
            break;
            
        case 2:
            NSLog(@"%@", self.cvCamera.whiteBalanceRange[row]);
            
            [self.cvCamera changeWhiteBalance:[self.cvCamera.whiteBalanceRange[row] intValue]];
            break;
            
        default:
            break;
    }
}

#pragma mark UIPickerView datasorce

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger numberOfRowsInComponent = 0;
    switch (component) {
        case 0:
            numberOfRowsInComponent = self.cvCamera.isoRange.count;
            break;
            
        case 1:
            numberOfRowsInComponent = self.cvCamera.shutterRange.count;
            break;
            
        case 2:
            numberOfRowsInComponent = self.cvCamera.whiteBalanceRange.count;
            break;
            
        default:
            break;
    }
    
    return numberOfRowsInComponent;
}

- (IBAction)switchVideoModeToNormal:(UIButton *)sender {
    [self.captureModeButton setImage:sender.imageView.image forState:UIControlStateSelected];

    [self restartCamera];
}

- (IBAction)switchVideoModeToSlowMotion:(UIButton *)sender {
    if (!self.liveSettingButton.isHidden) {
        if (self.cvCamera.startEncode) {
            [MBProgressHUD toast:self.view toastStr:@"please stop living first"];
            return;
        } else {
            [self.generalSettingsBackView closeLiving:YES];
        }
    }
    
    [self.captureModeButton setImage:sender.imageView.image forState:UIControlStateSelected];

    [self.cvCamera changeFormat:CvCameraFormatSlowMotion];
}

- (IBAction)switchVideoModeToTimeLapse:(UIButton *)sender {
    [self.captureModeButton setImage:sender.imageView.image forState:UIControlStateSelected];
    
    [self restartCamera];
    
    [self.captureModeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    if (self.timeLapseCaptureBackView.isHidden) {
        self.self.timeLapseCaptureBackView.hidden = NO;
    }
    [self.timeLapseCaptureView removeFromSuperview];
    [self.timeLapseCaptureBackView addSubview:self.timeLapseCaptureView];
    [self.timeLapseCaptureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.timeLapseCaptureBackView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    __weak ViewController *weakSelf = self;
    self.timeLapseCaptureView.startBlock = ^(Float32 interval, NSUInteger duration) {
        weakSelf.cvCamera.timeLapseShutter = interval;
        weakSelf.cvCamera.timeLapseDuration = duration;
        
        
        weakSelf.startTimeLapse = YES;
        [weakSelf captureAction];
        
        [weakSelf.cvCamera startTimeLapseWithFinish:^(NSURL *url) {
            NSLog(@"%@", url);
            [weakSelf captureAction];
            weakSelf.startTimeLapse = NO;
            [weakSelf saveVideoToAlbum:url];
        }];
    };
    self.timeLapseCaptureView.closeViewBlock = ^{
        weakSelf.timeLapseCaptureBackView.hidden = YES;
        if (weakSelf.cvCamera.cameraPosition == AVCaptureDevicePositionBack) {
            [weakSelf.backSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else if (weakSelf.cvCamera.cameraPosition == AVCaptureDevicePositionFront) {
            [weakSelf.frontSwitchVideoModeToNormalButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
    };
}

@end
