//
//  TimeLapseCaptureTableViewCell3.m
//  Ringo-iOS
//
//  Created by qq on 17/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "TimeLapseCaptureTableViewCell3.h"

@implementation TimeLapseCaptureTableViewCell3

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tripodModeSwitch.transform = CGAffineTransformMakeScale(0.9, 0.9);
}

- (IBAction)triopModeSwitchAction:(UISwitch *)sender {
}

@end
