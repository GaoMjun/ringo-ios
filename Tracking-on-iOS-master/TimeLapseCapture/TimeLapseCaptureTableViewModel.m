//
//  TimeLapseCaptureTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 13/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "TimeLapseCaptureTableViewModel.h"

@implementation TimeLapseCaptureTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    _sectionTitles = @[@"interval",
                           @"duration",
                           @"video duration",
                           @"tripod mode"];
    
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:@(0.5)];
    for (int i = 1; i <= 60; i ++) {
        [array addObject:@(i)];
    }
    _intervals = array;
    
    _durations = @[@(0.5),
                   @(1),
                   @(5),
                   @(10),
                   @(30),
                   @(60),
                   @(120),
                   @(180)];
    
}

@end
