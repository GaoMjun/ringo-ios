//
//  TimeLapseCaptureTableViewCell.m
//  Ringo-iOS
//
//  Created by qq on 14/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "TimeLapseCaptureTableViewCell.h"

@interface TimeLapseCaptureTableViewCell () <UIPickerViewDelegate, UIPickerViewDataSource>



@end

@implementation TimeLapseCaptureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.pickerView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    self.pickerView.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.width);
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40.f;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    UILabel *title = (UILabel *)view;
    
    if (!title) {
        title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
        
        title.font = [UIFont systemFontOfSize:14];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
    }

    if (pickerView.tag == INTERVAL) {
        title.text = [NSString stringWithFormat:@"%@s", self.model[row]];
    } else if (pickerView.tag == DURATION) {
        title.text = [NSString stringWithFormat:@"%@m", self.model[row]];
    }
    
    
    title.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == INTERVAL) {
        if (self.intervalChanged) {
            self.intervalChanged([self.model[row] floatValue]);
        }
        
    } else if (pickerView.tag == DURATION) {
        if (self.durationChanged) {
            self.durationChanged([self.model[row] floatValue]);
        }
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.model.count;
}

@end
