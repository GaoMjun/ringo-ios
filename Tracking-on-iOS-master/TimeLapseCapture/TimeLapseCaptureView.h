//
//  TimeLapseCaptureView.h
//  Ringo-iOS
//
//  Created by qq on 13/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLapseCaptureView : UIView

@property (copy, nonatomic) void (^startBlock)(Float32 interval, NSUInteger duration);
@property (copy, nonatomic) void (^closeViewBlock)();

@end
