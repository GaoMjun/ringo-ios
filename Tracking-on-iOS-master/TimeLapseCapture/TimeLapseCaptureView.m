//
//  TimeLapseCaptureView.m
//  Ringo-iOS
//
//  Created by qq on 13/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "TimeLapseCaptureView.h"
#import "TimeLapseCaptureTableViewModel.h"
#import "TimeLapseCaptureTableViewCell.h"
#import "TimeLapseCaptureTableViewCell2.h"
#import "TimeLapseCaptureTableViewCell3.h"

@interface TimeLapseCaptureView ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UITableView *TimeLapseCaptureTableView;

@property (strong, nonatomic) TimeLapseCaptureTableViewModel *model;

@property (copy, nonatomic) void (^intervalChanged)(float value);
@property (copy, nonatomic) void (^durationChanged)(float value);

@property (assign, nonatomic) float interval;
@property (assign, nonatomic) float duration;

@end

@implementation TimeLapseCaptureView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.cornerRadius = 5.f;
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.model = [[TimeLapseCaptureTableViewModel alloc] init];
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
            return 30.f;
            break;
            
        default:
            break;
    }
    
    return FLT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return FLT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *tableViewHeader = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    
    tableViewHeader.contentView.backgroundColor = [UIColor clearColor];
    [tableViewHeader.textLabel setTextColor:[UIColor whiteColor]];
    tableViewHeader.textLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightThin];
    
    switch (section) {
        case 0:
        case 1:
            return tableViewHeader;
            break;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.model.sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            TimeLapseCaptureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
            
            cell.model = self.model.intervals;
            cell.pickerView.tag = INTERVAL;
            cell.intervalChanged = ^(float value) {
                self.intervalChanged(value);
                self.interval = value;
            };

            return cell;
        }
            break;
            
        case 1: {
            TimeLapseCaptureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
            
            cell.model = self.model.durations;
            cell.pickerView.tag = DURATION;
            cell.durationChanged = ^(float value) {
                self.durationChanged(value);
                self.duration = value;
            };
            
            return cell;
        }
            break;
            
        case 2: {
            TimeLapseCaptureTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID2" forIndexPath:indexPath];
            self.intervalChanged = cell.intervalChanged;
            self.durationChanged = cell.durationChanged;
            
            return cell;
        }
            break;
            
        case 3: {
            TimeLapseCaptureTableViewCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID3" forIndexPath:indexPath];
            
            
            return cell;
        }
            break;
            
        default: {
            TimeLapseCaptureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
            
            return cell;
        }
            break;
    }

    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
        case 1:
            return self.model.sectionTitles[section];
            break;
            
        default:
            break;
    }
    
    return nil;
}

- (IBAction)closeView:(UIButton *)sender {
    [self removeFromSuperview];
    
    if (self.closeViewBlock) {
        self.closeViewBlock();
    }
}

- (IBAction)startAction:(UIButton *)sender {
    if (self.startBlock) {
        if (!self.interval) {
            self.interval = 0.5f;
        }
        
        if (!self.duration) {
            self.duration = 0.5f;
        }
        
        [self.closeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        self.startBlock(self.interval, self.duration*60);
    }
}


@end
