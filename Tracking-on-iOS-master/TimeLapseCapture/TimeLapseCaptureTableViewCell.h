//
//  TimeLapseCaptureTableViewCell.h
//  Ringo-iOS
//
//  Created by qq on 14/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

#define INTERVAL 1000
#define DURATION 1001

@interface TimeLapseCaptureTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) NSArray *model;

@property (copy, nonatomic) void (^intervalChanged)(float value);
@property (copy, nonatomic) void (^durationChanged)(float value);

@end
