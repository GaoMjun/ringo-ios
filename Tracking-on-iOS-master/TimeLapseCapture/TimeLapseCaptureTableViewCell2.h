//
//  TimeLapseCaptureTableViewCell2.h
//  Ringo-iOS
//
//  Created by qq on 17/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLapseCaptureTableViewCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeString;

@property (copy, nonatomic) void (^intervalChanged)(float value);
@property (copy, nonatomic) void (^durationChanged)(float value);

@end
