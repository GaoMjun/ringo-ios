//
//  TimeLapseCaptureTableViewCell3.h
//  Ringo-iOS
//
//  Created by qq on 17/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLapseCaptureTableViewCell3 : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *tripodModeSwitch;

@end
