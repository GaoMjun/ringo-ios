//
//  TimeLapseCaptureTableViewModel.h
//  Ringo-iOS
//
//  Created by qq on 13/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeLapseCaptureTableViewModel : NSObject

@property (nonatomic, strong, readonly) NSArray *sectionTitles;

@property (nonatomic, strong, readonly) NSArray *intervals;

@property (nonatomic, strong, readonly) NSArray *durations;

@end
