//
//  TimeLapseCaptureTableViewCell2.m
//  Ringo-iOS
//
//  Created by qq on 17/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "TimeLapseCaptureTableViewCell2.h"

@interface TimeLapseCaptureTableViewCell2 ()

@property (assign, nonatomic) float interval;
@property (assign, nonatomic) float duration;

@end

@implementation TimeLapseCaptureTableViewCell2

- (void)awakeFromNib {
    [super awakeFromNib];

    
    __weak typeof(self) weakSelf = self;
    self.intervalChanged = ^(float value) {
        weakSelf.interval = value;
        
        if (!weakSelf.duration) {
            weakSelf.duration = 0.5f;
        }
        
        int seconds = weakSelf.duration * 60 / weakSelf.interval / 30;
        if (seconds == 0) {
            seconds = 1;
        }
        
        weakSelf.timeString.text = [weakSelf secondsToHHMMSS:seconds];
    };

    self.durationChanged = ^(float value) {
        weakSelf.duration = value;
        
        if (!weakSelf.interval) {
            weakSelf.interval = 0.5f;
        }
        
        int seconds = weakSelf.duration * 60 / weakSelf.interval / 30;
        if (seconds == 0) {
            seconds = 1;
        }
        
        weakSelf.timeString.text = [weakSelf secondsToHHMMSS:seconds];
    };

}

- (NSString *)secondsToHHMMSS:(NSInteger)ti {
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}


@end
