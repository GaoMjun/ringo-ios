//
//  FiltersTableViewModel.h
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FiltersTableViewModel : NSObject

@property (nonatomic, strong) NSArray<NSString *> *filters;
@property (nonatomic, strong) NSArray<NSString *> *filtersName;
@property (nonatomic, strong) NSArray<UIImage *> *filtersImage;

@end
