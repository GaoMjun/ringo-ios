//
//  FiltersTableViewModel.m
//  Ringo-iOS
//
//  Created by qq on 15/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "FiltersTableViewModel.h"

@implementation FiltersTableViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initModel];
    }
    return self;
}

- (void)initModel {
    [self initFilters];
    [self initFiltersName];
    [self initFiltersImage];
}

- (void)initFilters {
    NSMutableArray *filters = [NSMutableArray array];
    
    [filters addObject:@"IFNormalFilter"];
    [filters addObject:@"IF1977Filter"];
    [filters addObject:@"IFAmaroFilter"];
    [filters addObject:@"IFBrannanFilter"];
    [filters addObject:@"IFEarlybirdFilter"];
    [filters addObject:@"IFHefeFilter"];
    [filters addObject:@"IFHudsonFilter"];
    [filters addObject:@"IFInkwellFilter"];
    [filters addObject:@"IFLordKelvinFilter"];
    [filters addObject:@"IFNashvilleFilter"];
    [filters addObject:@"IFRiseFilter"];
    [filters addObject:@"IFSierraFilter"];
    [filters addObject:@"IFSutroFilter"];
    [filters addObject:@"IFToasterFilter"];
    [filters addObject:@"IFValenciaFilter"];
    [filters addObject:@"IFWaldenFilter"];
    [filters addObject:@"IFXproIIFilter"];
    [filters addObject:@"GPUImageBeautifyFilter"];
    
    self.filters = filters;
}

- (void)initFiltersName {
    NSMutableArray *names = [NSMutableArray array];
    
    [names addObject:@"Normal"];
    [names addObject:@"1977"];
    [names addObject:@"Amaro"];
    [names addObject:@"Brannan"];
    [names addObject:@"Earlybird"];
    [names addObject:@"Hefe"];
    [names addObject:@"Hudson"];
    [names addObject:@"Inkwell"];
    [names addObject:@"Kelvin"];
    [names addObject:@"Nashville"];
    [names addObject:@"Rise"];
    [names addObject:@"Sierra"];
    [names addObject:@"Sutro"];
    [names addObject:@"Toaster"];
    [names addObject:@"Valencia"];
    [names addObject:@"Walden"];
    [names addObject:@"XproII"];
    [names addObject:@"Beautify"];
    
    self.filtersName = names;
}

- (void)initFiltersImage {
    NSMutableArray *images = [NSMutableArray array];
    
    [images addObject:[UIImage imageNamed:@"normal"]];
    [images addObject:[UIImage imageNamed:@"1977"]];
    [images addObject:[UIImage imageNamed:@"amaro"]];
    [images addObject:[UIImage imageNamed:@"brannan"]];
    [images addObject:[UIImage imageNamed:@"earlybird"]];
    [images addObject:[UIImage imageNamed:@"hefe"]];
    [images addObject:[UIImage imageNamed:@"hudson"]];
    [images addObject:[UIImage imageNamed:@"inkwell"]];
    [images addObject:[UIImage imageNamed:@"kelvin"]];
    [images addObject:[UIImage imageNamed:@"nashville"]];
    [images addObject:[UIImage imageNamed:@"rise"]];
    [images addObject:[UIImage imageNamed:@"sierra"]];
    [images addObject:[UIImage imageNamed:@"sutro"]];
    [images addObject:[UIImage imageNamed:@"toaster"]];
    [images addObject:[UIImage imageNamed:@"valencia"]];
    [images addObject:[UIImage imageNamed:@"walden"]];
    [images addObject:[UIImage imageNamed:@"xproII"]];
    [images addObject:[UIImage imageNamed:@"normal"]];
    
    self.filtersImage = images;
}

@end
