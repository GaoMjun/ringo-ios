//
//  AudioConfiguration.m
//  Ringo-iOS
//
//  Created by qq on 10/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "AudioConfiguration.h"

@implementation AudioConfiguration

+ (instancetype)sharedInstance {
    static AudioConfiguration *configuration = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        configuration = [[AudioConfiguration alloc] initWithDefault];
    });
    
    return configuration;
}

- (instancetype)initWithDefault
{
    self = [super init];
    if (self) {
        _channels = 1;
        _smaplerate = 44100;
        _bitrate = 96000;
    }
    return self;
}

@end
