//
//  VideoConfiguration.m
//  Ringo-iOS
//
//  Created by qq on 10/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "VideoConfiguration.h"

@implementation VideoConfiguration

+ (instancetype)sharedInstance {
    static VideoConfiguration *configuration = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        configuration = [[VideoConfiguration alloc] initWithDefault];
    });
    
    return configuration;
}

- (instancetype)initWithDefault
{
    self = [super init];
    if (self) {
//        _width = 640;
//        _height = 360;
        _fps = 10;
//        _bitrate = 200 * 1024;
        _keyframeInterval = _fps * 2;
        
        [self setDimention];
        
        [self setBitrate];
        
    }
    return self;
}

- (int)width {
    [self setDimention];
    
    return _width;
}

- (int)height {
    [self setDimention];
    
    return _height;
}

- (int)bitrate {
    [self setBitrate];
    
    return _bitrate;
}

- (void)setDimention {
    NSString *dimention = [[NSUserDefaults standardUserDefaults] stringForKey:@"dimention"];
    if (!dimention) {
        [[NSUserDefaults standardUserDefaults] setObject:@"640 x 360" forKey:@"dimention"];
        dimention = [[NSUserDefaults standardUserDefaults] stringForKey:@"dimention"];
    }
    
    if ([dimention isEqualToString:@"256 x 144"]) {
        _width = 256;
        _height = 144;
    } else if ([dimention isEqualToString:@"426 x 240"]) {
        _width = 426;
        _height = 240;
    } else if ([dimention isEqualToString:@"640 x 360"]) {
        _width = 640;
        _height = 360;
    } else if ([dimention isEqualToString:@"852 x 480"]) {
        _width = 852;
        _height = 480;
    } else if ([dimention isEqualToString:@"1280 x 720"]) {
        _width = 1280;
        _height = 720;
    }
}

- (void)setBitrate {
    NSString *bitrate = [[NSUserDefaults standardUserDefaults] stringForKey:@"bitrate"];
    if (!bitrate) {
        [[NSUserDefaults standardUserDefaults] setObject:@"200kbps" forKey:@"bitrate"];
        bitrate = [[NSUserDefaults standardUserDefaults] stringForKey:@"bitrate"];
    }
    
    if ([bitrate isEqualToString:@"100kbps"]) {
        _bitrate = 100;
    } else if ([bitrate isEqualToString:@"200kbps"]) {
        _bitrate = 200;
    } else if ([bitrate isEqualToString:@"400kbps"]) {
        _bitrate = 400;
    } else if ([bitrate isEqualToString:@"600kbps"]) {
        _bitrate = 600;
    } else if ([bitrate isEqualToString:@"800kbps"]) {
        _bitrate = 800;
    } else if ([bitrate isEqualToString:@"1000kbps"]) {
        _bitrate = 1000;
    } else if ([bitrate isEqualToString:@"1500kbps"]) {
        _bitrate = 1500;
    } else if ([bitrate isEqualToString:@"2000kbps"]) {
        _bitrate = 2000;
    }
    
    _bitrate *= 1024;
}

@end
