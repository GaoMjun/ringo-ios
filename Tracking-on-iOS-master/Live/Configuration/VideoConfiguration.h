//
//  VideoConfiguration.h
//  Ringo-iOS
//
//  Created by qq on 10/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoConfiguration : NSObject

@property (assign, nonatomic) int width;

@property (assign, nonatomic) int height;

@property (assign, nonatomic) int bitrate;

@property (assign, nonatomic) int fps;

@property (assign, nonatomic) int keyframeInterval;

+ (instancetype)sharedInstance;

@end
