//
//  AudioConfiguration.h
//  Ringo-iOS
//
//  Created by qq on 10/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioConfiguration : NSObject

@property (assign, nonatomic) int channels;

@property (assign, nonatomic) int smaplerate;

@property (assign, nonatomic) int bitrate;

+ (instancetype)sharedInstance;

@end
