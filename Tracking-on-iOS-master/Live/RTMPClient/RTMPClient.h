//
//  RTMPClient.h
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTMPFrame.h"

typedef NS_ENUM(NSInteger, LIVE_STATE) {
    LIVING,
    STOP
};

@interface RTMPClient : NSObject

@property (assign, nonatomic, readonly) LIVE_STATE state;

+ (instancetype)sharedInstance;
- (void)connect:(NSString *)publishUrl success:(void (^)(BOOL success))success;
- (void)close;
- (void)sendFrame:(RTMPFrame *)frame;

@end
