//
//  RTMPClient.m
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPClient.h"
#import "rtmp.h"
#import "RTMPVideoFrame.h"
#import "RTMPAudioFrame.h"
#import <QuartzCore/CABase.h>
#import "FrameBuffer.h"
#import "VideoConfiguration.h"
#import "AudioConfiguration.h"

@interface RTMPClient() {
    PILI_RTMP *rtmp;
    
    uint64_t startTime;
}

@property (nonatomic, assign) RTMPError error;

@property (nonatomic, strong) dispatch_queue_t rtmpQueue;
@property (nonatomic, strong) dispatch_queue_t rtmpSendConcrrentQueue;

@property (nonatomic, assign) BOOL sendVideoHeader;
@property (nonatomic, assign) BOOL sendAudioHeader;

@property (nonatomic, assign) BOOL sending;
@property (nonatomic, assign) BOOL hasKeyframe;

@property (nonatomic, strong) FrameBuffer *frameBuffer;

@property (strong, nonatomic) VideoConfiguration *videoConfiguration;
@property (strong, nonatomic) AudioConfiguration *audioConfiguration;

@end

@implementation RTMPClient

- (VideoConfiguration *)videoConfiguration {
    if (!_videoConfiguration) {
        _videoConfiguration = [VideoConfiguration sharedInstance];
    }
    
    return _videoConfiguration;
}

- (AudioConfiguration *)audioConfiguration {
    if (!_audioConfiguration) {
        _audioConfiguration = [AudioConfiguration sharedInstance];
    }
    
    return _audioConfiguration;
}

- (FrameBuffer *)frameBuffer {
    if (!_frameBuffer) {
        _frameBuffer = [FrameBuffer sharedInstance];
    }
    
    return _frameBuffer;
}

+ (instancetype)sharedInstance {
    static RTMPClient *client = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[RTMPClient alloc] init];
    });
    
    return client;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _state = STOP;
        _rtmpQueue = dispatch_queue_create("rtmpQueue", DISPATCH_QUEUE_SERIAL);
        _rtmpSendConcrrentQueue = dispatch_queue_create("rtmpSendConcrrentQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

//void RTMPErrorCallback(RTMPError *error, void *userData) {
////    LFStreamRTMPSocket *socket = (__bridge LFStreamRTMPSocket *)userData;
////    if (error->code < 0) {
////        [socket reconnect];
////    }
//}
//
//void ConnectionTimeCallback(PILI_CONNECTION_TIME *conn_time, void *userData) {
//    
//}

- (void)connect:(NSString *)publishUrl success:(void (^)(BOOL success))success {

    dispatch_async(self.rtmpQueue, ^{
        rtmp = PILI_RTMP_Alloc();
        
        PILI_RTMP_Init(rtmp);
        
        rtmp->Link.timeout = 5;
        rtmp->Link.send_timeout = 5;
//        RTMP_LOG_ENABLE();
        
        if (PILI_RTMP_SetupURL(rtmp, publishUrl.UTF8String, &_error) == FALSE) {
            //log(LOG_ERR, "RTMP_SetupURL() failed!");
            goto Failed;
        }
        
        //    rtmp->m_errorCallback = RTMPErrorCallback;
        //    rtmp->m_connCallback = ConnectionTimeCallback;
        //    rtmp->m_userData = (__bridge void *)self;
        //    rtmp->m_msgCounter = 1;
        //    rtmp->Link.timeout = 2;
        
        PILI_RTMP_EnableWrite(rtmp);
        
        if (PILI_RTMP_Connect(rtmp, NULL, &_error) == FALSE) {
            goto Failed;
        }
        
        if (PILI_RTMP_ConnectStream(rtmp, 0, &_error) == FALSE) {
            goto Failed;
        }
        
        //    if (self.delegate && [self.delegate respondsToSelector:@selector(socketStatus:status:)]) {
        //        [self.delegate socketStatus:self status:LFLiveStart];
        //    }
        //
        [self sendMetaData];
        
        //    _isConnected = YES;
        //    _isConnecting = NO;
        //    _isReconnecting = NO;
        //    _isSending = NO;
        _state = LIVING;
        success(YES);
        //    return 0;
        return;
        
    Failed:
        PILI_RTMP_Close(rtmp, &_error);
        PILI_RTMP_Free(rtmp);
        rtmp = NULL;
        //    [self reconnect];
        _state = STOP;
        success(NO);
        //    return -1;
    });

}

- (void)close {
    _state = STOP;
    self.sendVideoHeader = NO;
    self.sendAudioHeader = NO;
    self.hasKeyframe = NO;
    
    [self.frameBuffer clear];
    startTime = 0;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (rtmp != NULL) {
            dispatch_async(self.rtmpQueue, ^{
                PILI_RTMP_Close(rtmp, &_error);
                PILI_RTMP_Free(rtmp);
                rtmp = NULL;
            });
        }
    });
}

static const AVal av_setDataFrame = AVC("@setDataFrame");
static const AVal av_SDKVersion = AVC("ringo");

#define SAVC(x)    static const AVal av_ ## x = AVC(#x)

SAVC(onMetaData);
SAVC(duration);
SAVC(fileSize);
SAVC(width);
SAVC(height);
SAVC(videocodecid);
SAVC(avc1);
SAVC(videodatarate);
SAVC(framerate);

SAVC(audiocodecid);
SAVC(mp4a);
SAVC(audiodatarate);
SAVC(audiosamplerate);
SAVC(audiosamplesize);
SAVC(stereo);
SAVC(encoder);

- (void)sendMetaData {
    PILI_RTMPPacket packet;
    
    char pbuf[2048], *pend = pbuf + sizeof(pbuf);
    
    packet.m_nChannel = 0x03;                   // control channel (invoke)
    packet.m_headerType = RTMP_PACKET_SIZE_LARGE;
    packet.m_packetType = RTMP_PACKET_TYPE_INFO;
    packet.m_nTimeStamp = 0;
    packet.m_nInfoField2 = rtmp->m_stream_id;
    packet.m_hasAbsTimestamp = TRUE;
    packet.m_body = pbuf + RTMP_MAX_HEADER_SIZE;
    
    char *enc = packet.m_body;
    enc = AMF_EncodeString(enc, pend, &av_setDataFrame);
    enc = AMF_EncodeString(enc, pend, &av_onMetaData);
    
    *enc++ = AMF_OBJECT;
    
    enc = AMF_EncodeNamedNumber(enc, pend, &av_duration, 0.0);
    enc = AMF_EncodeNamedNumber(enc, pend, &av_fileSize, 0.0);
    
    // videosize
    enc = AMF_EncodeNamedNumber(enc, pend, &av_width, self.videoConfiguration.width);
    enc = AMF_EncodeNamedNumber(enc, pend, &av_height, self.videoConfiguration.height);
    
    // video
    enc = AMF_EncodeNamedString(enc, pend, &av_videocodecid, &av_avc1);
    
    enc = AMF_EncodeNamedNumber(enc, pend, &av_videodatarate, self.videoConfiguration.bitrate / 1000.f);
    enc = AMF_EncodeNamedNumber(enc, pend, &av_framerate, self.videoConfiguration.fps);
    
    // audio
    enc = AMF_EncodeNamedString(enc, pend, &av_audiocodecid, &av_mp4a);
    enc = AMF_EncodeNamedNumber(enc, pend, &av_audiodatarate, self.audioConfiguration.bitrate);
    
    enc = AMF_EncodeNamedNumber(enc, pend, &av_audiosamplerate, self.audioConfiguration.smaplerate);
    enc = AMF_EncodeNamedNumber(enc, pend, &av_audiosamplesize, 16.0);
    enc = AMF_EncodeNamedBoolean(enc, pend, &av_stereo, 1);
    
    // sdk version
    enc = AMF_EncodeNamedString(enc, pend, &av_encoder, &av_SDKVersion);
    
    *enc++ = 0;
    *enc++ = 0;
    *enc++ = AMF_OBJECT_END;
    
    packet.m_nBodySize = (uint32_t)(enc - packet.m_body);
    if (!PILI_RTMP_SendPacket(rtmp, &packet, FALSE, &_error)) {
        return;
    }
}

- (RTMPFrame *)calculateTimestamp:(RTMPFrame *)frame {

    if (startTime == 0) {
        startTime = frame.timestamp;
    }
    
    frame.timestamp -= startTime;

    
    return frame;
}

- (void)sendFrame:(RTMPFrame *)frame {

    if (!self.hasKeyframe) {
        if ([frame isKindOfClass:[RTMPVideoFrame class]]) {
            RTMPVideoFrame *videoFrame = (RTMPVideoFrame *)frame;
            if (videoFrame.isKeyFrame) {
                self.hasKeyframe = YES;
                
            } else {
                return;
            }
            
        } else {
            return;
        }
    }
    
    frame = [self calculateTimestamp:frame];
    
//    [self.frameBuffer appendFrame:frame];
    
    if (self.state == LIVING && (!self.sending)) {
//            self.sending = YES;
        
//        RTMPFrame *frame = [self.frameBuffer popFrame];
        
        if ([frame isKindOfClass:[RTMPVideoFrame class]]) {
            
            [self sendVideoFrame:(RTMPVideoFrame *)frame];
            
        } else if ([frame isKindOfClass:[RTMPAudioFrame class]]) {
            
            [self sendAudioFrame:(RTMPAudioFrame *)frame];
            
        }
    }

}

- (void)sendVideoFrame:(RTMPVideoFrame *)videoFrame {
    
    dispatch_async(self.rtmpQueue, ^{
        if (!self.sendVideoHeader) {
            if (!videoFrame.sps) {
                return;
            }
            self.sendVideoHeader = YES;
            
            [self sendPacket:RTMP_PACKET_TYPE_VIDEO data:(unsigned char *)(videoFrame.header.bytes) size:videoFrame.header.length nTimestamp:0];
            
        } else {

            [self sendPacket:RTMP_PACKET_TYPE_VIDEO data:(unsigned char *)(videoFrame.body.bytes) size:videoFrame.body.length nTimestamp:videoFrame.timestamp];
        }
        
    });
}

- (void)sendAudioFrame:(RTMPAudioFrame *)audioFrame {
    dispatch_async(self.rtmpQueue, ^{
        if (!self.sendAudioHeader) {
            self.sendAudioHeader = YES;
            
            [self sendPacket:RTMP_PACKET_TYPE_AUDIO data:(unsigned char *)(audioFrame.header.bytes) size:audioFrame.header.length nTimestamp:0];
            
        } else {
            [self sendPacket:RTMP_PACKET_TYPE_AUDIO data:(unsigned char *)(audioFrame.body.bytes) size:audioFrame.body.length nTimestamp:audioFrame.timestamp];
        }
    });
}

- (void)sendPacket:(unsigned int)nPacketType data:(unsigned char *)data size:(NSInteger)size nTimestamp:(uint64_t)nTimestamp {
    NSInteger rtmpLength = size;
    PILI_RTMPPacket rtmp_pack;
    PILI_RTMPPacket_Reset(&rtmp_pack);
    PILI_RTMPPacket_Alloc(&rtmp_pack, (uint32_t)rtmpLength);
    
    rtmp_pack.m_nBodySize = (uint32_t)size;
    memcpy(rtmp_pack.m_body, data, size);
    rtmp_pack.m_hasAbsTimestamp = 0;
    rtmp_pack.m_packetType = nPacketType;
    if (rtmp)
        rtmp_pack.m_nInfoField2 = rtmp->m_stream_id;
    rtmp_pack.m_nChannel = 0x04;
    rtmp_pack.m_headerType = RTMP_PACKET_SIZE_LARGE;
    if (RTMP_PACKET_TYPE_AUDIO == nPacketType && size != 4) {
        rtmp_pack.m_headerType = RTMP_PACKET_SIZE_MEDIUM;
    }
    rtmp_pack.m_nTimeStamp = (uint32_t)nTimestamp;
    
    PILI_RTMP_SendPacket(rtmp, &rtmp_pack, 0, &_error);
    
    PILI_RTMPPacket_Free(&rtmp_pack);
    
    self.sending = NO;
}

@end
