//
//  FrameBuffer.m
//  Ringo-iOS
//
//  Created by qq on 9/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "FrameBuffer.h"
#import "RTMPVideoFrame.h"

#define MAX_BUFFER_SIZE 500
#define REMOVE_OLD_FRAME_ONE_TIME 5

@interface FrameBuffer() {
    dispatch_queue_t frameBufferQueue;
}

@property (strong, nonatomic) NSMutableArray *buffer;

@end

@implementation FrameBuffer

+ (instancetype)sharedInstance {
    static FrameBuffer *buffer = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        buffer = [[FrameBuffer alloc] init];
    });
    
    return buffer;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.buffer = [NSMutableArray arrayWithCapacity:MAX_BUFFER_SIZE];
        frameBufferQueue = dispatch_queue_create("frameBufferQueue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void)appendFrame:(RTMPFrame *)frame {
    
    __weak typeof(self) _self = self;
    
    dispatch_async(frameBufferQueue, ^{
        __strong typeof(_self) __self = _self;
        
        if (__self.buffer.count >= MAX_BUFFER_SIZE) {
            [__self removeOldFrame:REMOVE_OLD_FRAME_ONE_TIME];
        }
        [__self.buffer addObject:frame];
//        NSLog(@"frame buffer size %lu", (unsigned long)__self.buffer.count);
    });
}

- (RTMPFrame *)popFrame {
    __block RTMPFrame *frame;
    
    __weak typeof(self) _self = self;
    dispatch_sync(frameBufferQueue, ^{
        frame = _self.buffer.firstObject;
        
        [_self.buffer removeObjectAtIndex:0];
    });
    
    return frame;
}

- (void)clear {
    __weak typeof(self) _self = self;
    dispatch_async(frameBufferQueue, ^{
        [_self.buffer removeAllObjects];
    });
}

- (void)removeOldFrame:(NSUInteger)count {

    NSMutableArray *willRomoveFrameIndexs = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < self.buffer.count; i++) {
        RTMPFrame *frame = self.buffer[i];
        if ([frame isKindOfClass:[RTMPVideoFrame class]]) {
            RTMPVideoFrame *videoFrame = (RTMPVideoFrame *)frame;
            
            if (videoFrame.isKeyFrame) {
                if (willRomoveFrameIndexs.count < count) {
                    for (int j = i; j > 0; j--) {
                        if ([self.buffer[j-1] isKindOfClass:[RTMPVideoFrame class]]) {
                            [willRomoveFrameIndexs addObject:[NSNumber numberWithInt:j-1]];
                            break;
                        }
                    }
                }
            }
        }
    }
    
    for (int i = 0; i < willRomoveFrameIndexs.count; i++) {
        [self.buffer removeObjectAtIndex:i];
    }
}

- (NSInteger)count {
    return self.buffer.count;
}

@end
