//
//  RTMPAudioFrame.m
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPAudioFrame.h"
#import "NSString+HexString.h"

@implementation RTMPAudioFrame

- (NSData *)packet {
    NSData *header = [self prepareHeader];
    NSData *body = [self prepareBody];
    
    NSMutableData *frame = [NSMutableData data];
    [frame appendData:header];
    [frame appendData:body];
    
    return frame;
}

- (NSData *)header {
    return [self prepareHeader];
}

- (NSData *)body {
    return [self prepareBody];
}

- (NSData *)prepareHeader {
    // AAC sequence header
//    NSInteger rtmpLength = self.info.length + 2;     /*spec data长度,一般是2*/
//    unsigned char *body = (unsigned char *)malloc(rtmpLength);
//    memset(body, 0, rtmpLength);
//    
//    /*AF 00 + AAC RAW data*/
//    body[0] = 0xAF;
//    body[1] = 0x00;
//    body[2] = 0x12;
//    body[3] = 0x10;
//    memcpy(&body[2], self.info.bytes, self.info.length);
//    
//    NSData *frameHeader = [NSData dataWithBytes:body length:self.info.length];
//    
//    free(body);
    
    NSData *frameHeader = [NSString dataWithHexString:@"AF001210"];
    
    return frameHeader;
}

- (NSData *)prepareBody {
    NSInteger rtmpLength = self.data.length + 2;    /*spec data长度,一般是2*/
    unsigned char *body = (unsigned char *)malloc(rtmpLength);
    memset(body, 0, rtmpLength);
    
    /*AF 01 + AAC RAW data*/
    body[0] = 0xAF;
    body[1] = 0x01;
    memcpy(&body[2], self.data.bytes, self.data.length);
    
    NSData *frameBody = [NSData dataWithBytes:body length:rtmpLength];
    
    free(body);
    
    return frameBody;
}

- (NSData *)info {
    if (!_info) {
        uint16_t asc = 0;
        asc |= ((2<<11) & 0xf800);
        asc |= ((4<<7) & 0x0780);
        asc |= ((2<<3) & 0x78);
        asc |= (0 & 0x07);
        
        _info = [NSData dataWithBytes:&asc length:2];
    }
    
    return _info;
}

@end
