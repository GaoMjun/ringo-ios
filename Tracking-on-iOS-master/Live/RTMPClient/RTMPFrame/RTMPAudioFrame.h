//
//  RTMPAudioFrame.h
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPFrame.h"

@interface RTMPAudioFrame : RTMPFrame

@property (strong, nonatomic) NSData *info;

@end
