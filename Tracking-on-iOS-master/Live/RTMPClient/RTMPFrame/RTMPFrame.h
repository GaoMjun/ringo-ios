//
//  RTMPFrame.h
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTMPFrame : NSObject

@property (assign, nonatomic) uint64_t timestamp;

@property (strong, nonatomic) NSData *data;

@property (strong, nonatomic, readonly) NSData *packet;

@property (strong, nonatomic, readonly) NSData *header;

@property (strong, nonatomic, readonly) NSData *body;

@end
