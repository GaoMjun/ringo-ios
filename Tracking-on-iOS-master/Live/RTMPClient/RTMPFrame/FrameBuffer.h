//
//  FrameBuffer.h
//  Ringo-iOS
//
//  Created by qq on 9/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPFrame.h"
#import "RTMPFrame.h"

@interface FrameBuffer : RTMPFrame

@property (assign, nonatomic) NSInteger count;

+ (instancetype)sharedInstance;

- (void)appendFrame:(RTMPFrame *)frame;

- (RTMPFrame *)popFrame;

- (void)clear;

@end
