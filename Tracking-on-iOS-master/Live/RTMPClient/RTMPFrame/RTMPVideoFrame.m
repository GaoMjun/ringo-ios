//
//  RTMPVideoFrame.m
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPVideoFrame.h"

@implementation RTMPVideoFrame

- (NSData *)packet {
    
    NSData *header = [self prepareHeader];
    NSData *body = [self prepareBody];
    
    NSMutableData *frame = [NSMutableData data];
    [frame appendData:header];
    [frame appendData:body];
    
    return frame;
}

- (NSData *)header {
    return [self prepareHeader];
}

- (NSData *)body {
    return [self prepareBody];
}

- (NSData *)prepareHeader {
    // AVC sequence header
    unsigned char *body = NULL;
    NSInteger iIndex = 0;
    NSInteger rtmpLength = 1024;
    const char *sps = self.sps.bytes;
    const char *pps = self.pps.bytes;
    NSInteger sps_len = self.sps.length;
    NSInteger pps_len = self.pps.length;
    
    body = (unsigned char *)malloc(rtmpLength);
    memset(body, 0, rtmpLength);
    
    body[iIndex++] = 0x17;
    body[iIndex++] = 0x00;
    
    body[iIndex++] = 0x00;
    body[iIndex++] = 0x00;
    body[iIndex++] = 0x00;
    
    body[iIndex++] = 0x01;
    body[iIndex++] = sps[1];
    body[iIndex++] = sps[2];
    body[iIndex++] = sps[3];
    body[iIndex++] = 0xff;
    
    /*sps*/
    body[iIndex++] = 0xe1;
    body[iIndex++] = (sps_len >> 8) & 0xff;
    body[iIndex++] = sps_len & 0xff;
    memcpy(&body[iIndex], sps, sps_len);
    iIndex += sps_len;
    
    /*pps*/
    body[iIndex++] = 0x01;
    body[iIndex++] = (pps_len >> 8) & 0xff;
    body[iIndex++] = (pps_len) & 0xff;
    memcpy(&body[iIndex], pps, pps_len);
    iIndex += pps_len;
    
    NSData *frameHeader = [NSData dataWithBytes:body length:iIndex];
    
    free(body);
    
    return frameHeader;
}

- (NSData *)prepareBody {
    NSInteger i = 0;
    NSInteger rtmpLength = self.data.length + 9;
    unsigned char *body = (unsigned char *)malloc(rtmpLength);
    memset(body, 0, rtmpLength);
    
    if (self.isKeyFrame) {
        body[i++] = 0x17;        // 1:Iframe  7:AVC
    } else {
        body[i++] = 0x27;        // 2:Pframe  7:AVC
    }
    body[i++] = 0x01;    // AVC NALU
    body[i++] = 0x00;
    body[i++] = 0x00;
    body[i++] = 0x00;
    body[i++] = (self.data.length >> 24) & 0xff;
    body[i++] = (self.data.length >> 16) & 0xff;
    body[i++] = (self.data.length >>  8) & 0xff;
    body[i++] = (self.data.length) & 0xff;
    memcpy(&body[i], self.data.bytes, self.data.length);
    
    NSData *frameBody = [NSData dataWithBytes:body length:rtmpLength];
    
    free(body);
    
    return frameBody;
}

@end
