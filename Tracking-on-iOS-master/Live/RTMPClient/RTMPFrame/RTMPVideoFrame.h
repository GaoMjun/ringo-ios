//
//  RTMPVideoFrame.h
//  Ringo-iOS
//
//  Created by qq on 5/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "RTMPFrame.h"

@interface RTMPVideoFrame : RTMPFrame

@property (strong, nonatomic) NSData *sps;
@property (strong, nonatomic) NSData *pps;
@property (assign, nonatomic) BOOL isKeyFrame;

@end
