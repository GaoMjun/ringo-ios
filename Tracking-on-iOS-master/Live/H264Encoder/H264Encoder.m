//
//  H264Encoder.m
//  Ringo-iOS
//
//  Created by qq on 29/12/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "H264Encoder.h"
#import "RTMPVideoFrame.h"
#import "RTMPClient.h"
#import <QuartzCore/CABase.h>
#import "VideoConfiguration.h"

@interface H264Encoder() {
    VTCompressionSessionRef compressionSession;
    FILE *fp;
    NSData *sps;
    NSData *pps;
    
    BOOL enable;
    
    NSInteger frameCount;
}

@property (assign, nonatomic) int videoFrameWidth;
@property (assign, nonatomic) int videoFrameHeight;
@property (assign, nonatomic) int fps;
@property (assign, nonatomic) int videoBitrate;
@property (assign, nonatomic) int keyframeInterval;

@property (assign, nonatomic) BOOL enabledWriteVideoFile;
@property (strong, nonatomic) VideoConfiguration *videoConfiguration;

@end

@implementation H264Encoder

- (VideoConfiguration *)videoConfiguration {
    if (!_videoConfiguration) {
        _videoConfiguration = [VideoConfiguration sharedInstance];
    }
    
    return _videoConfiguration;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        enable = YES;
        self.enabledWriteVideoFile = NO;
        self.videoFrameWidth = self.videoConfiguration.width;
        self.videoFrameHeight = self.videoConfiguration.height;
        self.fps = self.videoConfiguration.fps;
        self.videoBitrate = self.videoConfiguration.bitrate;
        self.keyframeInterval = self.videoConfiguration.keyframeInterval;
        [self initEncode];
//        [self initForFilePath];

    }
    return self;
}

- (void)initEncode {
    OSStatus status =
    VTCompressionSessionCreate(kCFAllocatorDefault,
                               self.videoFrameWidth, self.videoFrameHeight,
                               kCMVideoCodecType_H264,
                               NULL,
                               NULL,
                               NULL,
                               outputCallback,
                               (__bridge void * _Nullable)(self),
                               &compressionSession);
    if ((status != 0) || (compressionSession == NULL)) {
        NSLog(@"VTCompressionSessionCreate faild %d", status);
        enable = NO;
        return;
    }
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_MaxKeyFrameInterval, (__bridge CFTypeRef)@(self.keyframeInterval));
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_MaxKeyFrameIntervalDuration, (__bridge CFTypeRef)@(self.keyframeInterval / self.fps));
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_ExpectedFrameRate, (__bridge CFTypeRef)@(self.fps));
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_AverageBitRate, (__bridge CFTypeRef)@(self.videoBitrate));
    
    NSArray *limit = @[@(self.videoBitrate * 1.2/8), @(1)];
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_DataRateLimits, (__bridge CFArrayRef)limit);
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_RealTime, kCFBooleanTrue);
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_Main_AutoLevel);
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_AllowFrameReordering, kCFBooleanTrue);
    
    status =
    VTSessionSetProperty(compressionSession, kVTCompressionPropertyKey_H264EntropyMode, kVTH264EntropyMode_CABAC);

    status =
    VTCompressionSessionPrepareToEncodeFrames(compressionSession);
}

- (void)encoding:(CVImageBufferRef)imageBuffer timestamp:(uint64_t)timestamp {
    if (enable) {
        
        CFRetain(imageBuffer);
    
        frameCount++;
        CMTime presentationTimeStamp = CMTimeMake(frameCount, self.fps);
        CMTime duration = CMTimeMake(1, self.fps);
        
        NSDictionary *properties = nil;
        if (frameCount % self.keyframeInterval == 0) {
            properties = @{(__bridge NSString *)kVTEncodeFrameOptionKey_ForceKeyFrame: @YES};
        }
        
        NSNumber *timeNumber = @(timestamp);
        
        OSStatus status =
        VTCompressionSessionEncodeFrame(compressionSession,
                                        imageBuffer,
                                        presentationTimeStamp,
                                        duration,
                                        (__bridge CFDictionaryRef)properties,
                                        (__bridge void * _Nullable)(timeNumber),
                                        NULL);
        
        CFRelease(imageBuffer);

    }
}

- (void)stopEncoding {
    enable = NO;
    sps = nil;
    pps = nil;
    frameCount = 0;
    
    if (compressionSession) {
        OSStatus status =
        VTCompressionSessionCompleteFrames(compressionSession, kCMTimeInvalid);
        
        if (status) {
            VTCompressionSessionInvalidate(compressionSession);
            
            CFRelease(compressionSession);
            
            compressionSession = NULL;
        }
    }
    
    if (self->fp) {
        fclose(fp);
        
        fp = NULL;
    }
}

static void outputCallback(void *outputCallbackRefCon,
                void *sourceFrameRefCon,
                OSStatus status,
                VTEncodeInfoFlags infoFlags,
                CMSampleBufferRef sampleBuffer ) {
    
    if (status != 0) return;
    if (!sampleBuffer) return;
    
    CFArrayRef attachmentsArray = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, true);
    if (!attachmentsArray) return;
    
    CFDictionaryRef dic = (CFDictionaryRef)CFArrayGetValueAtIndex(attachmentsArray, 0);
    if (!dic) return;
    
    H264Encoder *h264Encoder = (__bridge H264Encoder *)(outputCallbackRefCon);
    if (h264Encoder == nil) return;
    
    BOOL keyframe = !CFDictionaryContainsKey(dic, kCMSampleAttachmentKey_NotSync);
    
    if (!h264Encoder->pps && keyframe) {
        CMFormatDescriptionRef format = CMSampleBufferGetFormatDescription(sampleBuffer);
        
        const uint8_t *sparameterSet;
        size_t sparameterSetSize;
        size_t sparameterSetCount;
        status =
        CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format,
                                                           0,
                                                           &sparameterSet,
                                                           &sparameterSetSize,
                                                           &sparameterSetCount,
                                                           0);
        if (status == 0) {
            const uint8_t *pparameterSet;
            size_t pparameterSetSize;
            size_t pparameterSetCount;
            status =
            CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format,
                                                               1,
                                                               &pparameterSet,
                                                               &pparameterSetSize,
                                                               &pparameterSetCount,
                                                               0);
            
            if (status == 0) {
                
                h264Encoder->sps = [NSData dataWithBytes:sparameterSet length:sparameterSetSize];
                h264Encoder->pps = [NSData dataWithBytes:pparameterSet length:pparameterSetSize];
                
                if (h264Encoder.enabledWriteVideoFile) {
                    NSMutableData *data = [[NSMutableData alloc] init];
                    
                    uint8_t header[] = {0x00, 0x00, 0x00, 0x01};
                    
                    [data appendBytes:header length:4];
                    [data appendData:h264Encoder->sps];
                    
                    [data appendBytes:header length:4];
                    [data appendData:h264Encoder->pps];
                    
                    fwrite(data.bytes, 1, data.length, h264Encoder->fp);
                }
            }
        }
    }
    
    
    CMBlockBufferRef blockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer);
    
    char *dataPointer;
    size_t length;
    size_t totalLength;
    
    status =
    CMBlockBufferGetDataPointer(blockBuffer,
                                0,
                                &length,
                                &totalLength,
                                &dataPointer);
    
    if (status == 0) {
        size_t bufferOffset = 0;
        static const int AVCCHeaderLength = 4;
        while (bufferOffset < totalLength - AVCCHeaderLength) {
            // Read the NAL unit length
            uint32_t NALUnitLength = 0;
            memcpy(&NALUnitLength, dataPointer + bufferOffset, AVCCHeaderLength);
            
            NALUnitLength = CFSwapInt32BigToHost(NALUnitLength);
            
            NSData *h264Data = [[NSData alloc] initWithBytes:(dataPointer + bufferOffset + AVCCHeaderLength) length:NALUnitLength];

//            NSLog(@"h264Data %lu", h264Data.length);
            if (h264Data.length > 41) {
                
            
                RTMPVideoFrame *videoFrame = [[RTMPVideoFrame alloc] init];
                videoFrame.timestamp = [((__bridge NSNumber *)sourceFrameRefCon) longLongValue];
                videoFrame.data = h264Data;
                videoFrame.sps = h264Encoder->sps;
                videoFrame.pps = h264Encoder->pps;
                videoFrame.isKeyFrame = keyframe;
                
//                NSLog(@"%s, %lu", (keyframe ? "1" : "0"), (unsigned long)h264Data.length);
                
                RTMPClient *rtmpClient = [RTMPClient sharedInstance];
                [rtmpClient sendFrame:videoFrame];
                
                if (h264Encoder.enabledWriteVideoFile) {
                    NSMutableData *data = [[NSMutableData alloc] init];
                    if (keyframe) {
                        uint8_t header[] = {0x00, 0x00, 0x00, 0x01};
                        [data appendBytes:header length:4];
                    } else {
                        uint8_t header[] = {0x00, 0x00, 0x01};
                        [data appendBytes:header length:3];
                    }
                    [data appendData:h264Data];
                    
                    fwrite(data.bytes, 1, data.length, h264Encoder->fp);
                }
            } else {
//                NSLog(@"%@", h264Data);
            }
            
            bufferOffset += AVCCHeaderLength + NALUnitLength;
            
        }
    }
}

- (void)initForFilePath {
    NSString *path = [self GetFilePathByfileName:@"IOSCamDemo.h264"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    NSLog(@"%@", path);
    self->fp = fopen([path cStringUsingEncoding:NSUTF8StringEncoding], "wb");
}

- (NSString *)GetFilePathByfileName:(NSString*)filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writablePath = [documentsDirectory stringByAppendingPathComponent:filename];
    return writablePath;
}

@end
