//
//  H264Encoder.h
//  Ringo-iOS
//
//  Created by qq on 29/12/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VideoToolbox/VideoToolbox.h>

@interface H264Encoder : NSObject

- (void)encoding:(CVImageBufferRef)imageBuffer timestamp:(uint64_t)timestamp;
- (void)stopEncoding;

@end
