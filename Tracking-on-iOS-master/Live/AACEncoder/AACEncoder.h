//
//  AACEncoder.h
//  Ringo-iOS
//
//  Created by qq on 30/12/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreMedia/CoreMedia.h>

@interface AACEncoder : NSObject

- (void)encoding:(NSData *)pcmData timestamp:(uint64_t)timestamp;
- (void)stopEncoding;

@end
