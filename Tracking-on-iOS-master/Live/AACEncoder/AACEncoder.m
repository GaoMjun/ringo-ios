//
//  AACEncoder.m
//  Ringo-iOS
//
//  Created by qq on 30/12/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import "AACEncoder.h"
#import "RTMPAudioFrame.h"
#import "RTMPClient.h"
#import <QuartzCore/CABase.h>
#import "AudioConfiguration.h"

@interface AACEncoder() {
    AudioConverterRef audioConverter;

    char *aacBuf;

    FILE *fp;
    
    UInt32 dataByteSize;
    
    
    
    BOOL enable;
}

@property (assign, nonatomic) BOOL enabledWriteVideoFile;

@property (assign, nonatomic) int numberOfChannels;
@property (assign, nonatomic) int samplerate;
@property (assign, nonatomic) int bitrate;
@property (strong, nonatomic) AudioConfiguration *audioConfiguration;

@end

@implementation AACEncoder

- (AudioConfiguration *)audioConfiguration {
    if (!_audioConfiguration) {
        _audioConfiguration = [AudioConfiguration sharedInstance];
    }
    
    return _audioConfiguration;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        enable = YES;
        self.enabledWriteVideoFile = NO;
        self.numberOfChannels = self.audioConfiguration.channels;
        self.samplerate = self.audioConfiguration.smaplerate;
        self.bitrate = self.audioConfiguration.bitrate;
//        [self initForFilePath];
        [self initEncoder];
    }
    return self;
}

- (void)initEncoder {
    AudioStreamBasicDescription inSourceFormat;
    inSourceFormat.mBitsPerChannel = 16;
    inSourceFormat.mChannelsPerFrame = self.numberOfChannels;
    inSourceFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked;
    inSourceFormat.mFormatID = kAudioFormatLinearPCM;
    inSourceFormat.mFramesPerPacket = 1;
    inSourceFormat.mReserved = 0;
    inSourceFormat.mSampleRate = self.samplerate;
    inSourceFormat.mBytesPerFrame = inSourceFormat.mBitsPerChannel/8 * inSourceFormat.mChannelsPerFrame;
    inSourceFormat.mBytesPerPacket = inSourceFormat.mBytesPerFrame * inSourceFormat.mFramesPerPacket;
    
    AudioStreamBasicDescription inDestinationFormat = {0};
//    inDestinationFormat.mBitsPerChannel = inSourceFormat.mBitsPerChannel;
//    inDestinationFormat.mBytesPerFrame =
//    inDestinationFormat.mBytesPerPacket =
    inDestinationFormat.mChannelsPerFrame = inSourceFormat.mChannelsPerFrame;
//    inDestinationFormat.mFormatFlags =
    inDestinationFormat.mFormatID = kAudioFormatMPEG4AAC;
    inDestinationFormat.mFramesPerPacket = 1024;
//    inDestinationFormat.mReserved = 0;
    inDestinationFormat.mSampleRate = inSourceFormat.mSampleRate;
    
    UInt32 size = sizeof(inDestinationFormat);
    
    OSStatus status =
    AudioFormatGetProperty(kAudioFormatProperty_FormatInfo, 0, NULL, &size, &inDestinationFormat);
    
//    AudioClassDescription description[2] = {
//        {
//            kAudioEncoderComponentType,
//            kAudioFormatMPEG4AAC,
//            kAppleSoftwareAudioCodecManufacturer
//        }
//        ,
//        {
//            kAudioEncoderComponentType,
//            kAudioFormatMPEG4AAC,
//            kAppleHardwareAudioCodecManufacturer
//        }
//    };
    AudioClassDescription *description = [self getAudioClassDescriptionWithType:kAudioFormatMPEG4AAC
                                                               fromManufacturer:kAppleSoftwareAudioCodecManufacturer];
    
    status =
    AudioConverterNewSpecific(&inSourceFormat,
                              &inDestinationFormat,
                              2,
                              description,
                              &audioConverter);

    if(status == 0) {
        UInt32 outputBitrate = self.bitrate;
        UInt32 propSize = sizeof(outputBitrate);
        
        status =
        AudioConverterSetProperty(audioConverter, kAudioConverterEncodeBitRate, propSize, &outputBitrate);
    }
    
    UInt32 value = 0;
    size = sizeof(value);
    status =
    AudioConverterGetProperty(audioConverter, kAudioConverterPropertyMaximumOutputPacketSize, &size, &value);
    if (status == 0) {
        dataByteSize = value;
    }
}

- (AudioClassDescription *)getAudioClassDescriptionWithType:(UInt32)type
                                           fromManufacturer:(UInt32)manufacturer {
    static AudioClassDescription desc;
    
    UInt32 encoderSpecifier = type;
    OSStatus st;
    
    UInt32 size;
    st = AudioFormatGetPropertyInfo(kAudioFormatProperty_Encoders,
                                    sizeof(encoderSpecifier),
                                    &encoderSpecifier,
                                    &size);
    if (st) {
        NSLog(@"error getting audio format propery info: %d", st);
        return nil;
    }
    
    unsigned int count = size / sizeof(AudioClassDescription);
    AudioClassDescription descriptions[count];
    st = AudioFormatGetProperty(kAudioFormatProperty_Encoders,
                                sizeof(encoderSpecifier),
                                &encoderSpecifier,
                                &size,
                                descriptions);
    if (st) {
        NSLog(@"error getting audio format propery: %d", st);
        return nil;
    }
    
    for (unsigned int i = 0; i < count; i++) {
        if ((type == descriptions[i].mSubType) &&
            (manufacturer == descriptions[i].mManufacturer)) {
            memcpy(&desc, &(descriptions[i]), sizeof(desc));
            return &desc;
        }
    }
    
    return nil;
}

- (void)encoding:(NSData *)pcmData timestamp:(uint64_t)timestamp{
    if (enable) {

        if (!aacBuf) {
            aacBuf = malloc(dataByteSize);
        }
        memset(aacBuf, 0, dataByteSize);
        
        UInt32 ioOutputDataPacketSize = 1;
        
        AudioBufferList outOutputData;
        outOutputData.mNumberBuffers = 1;
        outOutputData.mBuffers[0].mData = aacBuf;
        outOutputData.mBuffers[0].mNumberChannels = 1;
        outOutputData.mBuffers[0].mDataByteSize = dataByteSize;
        
        OSStatus status =
        AudioConverterFillComplexBuffer(audioConverter,
                                        inInputDataProc,
                                        (__bridge void * _Nullable)(pcmData),
                                        &ioOutputDataPacketSize,
                                        &outOutputData,
                                        NULL);
        if ((status == 0) && (outOutputData.mBuffers[0].mData) && (outOutputData.mBuffers[0].mDataByteSize)) {
            NSData *aacData = [NSData dataWithBytes:outOutputData.mBuffers[0].mData length:outOutputData.mBuffers[0].mDataByteSize];

//            NSLog(@"aacData %lu", (unsigned long)aacData.length);
            RTMPAudioFrame *audioFrame = [[RTMPAudioFrame alloc] init];
            audioFrame.timestamp = timestamp;
            audioFrame.data = aacData;
            
            RTMPClient *rtmpClient = [RTMPClient sharedInstance];
            [rtmpClient sendFrame:audioFrame];
            
            if (self.enabledWriteVideoFile && aacData) {
                NSData *adts = [self adtsData:2 rawDataLength:aacData.length];
                fwrite(adts.bytes, 1, adts.length, self->fp);
                fwrite(aacData.bytes, 1, aacData.length, self->fp);
            }
        }

    }
}

- (void)stopEncoding {
    enable = NO;
    @synchronized (self) {
        if (audioConverter != NULL) {
            OSStatus status =
            AudioConverterDispose(audioConverter);
            
            if (status == 0) {
                
                audioConverter = NULL;
                free(aacBuf);
            }
        }
    }
    
    if (self->fp) {
        fclose(fp);
        
        fp = NULL;
    }
}

static OSStatus inInputDataProc(AudioConverterRef inConverter,
                       UInt32 *ioNumberDataPackets,
                       AudioBufferList *ioData,
                       AudioStreamPacketDescription * *outDataPacketDescription,
                       void *inUserData) {

    NSData *pcmData = (__bridge NSData *)(inUserData);
    if (pcmData.length <= 0) {
        *ioNumberDataPackets = 0;
        return -1;
    }

    ioData->mNumberBuffers = 1;
    ioData->mBuffers[0].mNumberChannels = 1;
    ioData->mBuffers[0].mData = (void *)(pcmData.bytes);
    ioData->mBuffers[0].mDataByteSize = (UInt32)(pcmData.length);
    
    *ioNumberDataPackets = 1;
    
    return 0;
}

/**
 *  Add ADTS header at the beginning of each and every AAC packet.
 *  This is needed as MediaCodec encoder generates a packet of raw
 *  AAC data.
 *
 *  Note the packetLen must count in the ADTS header itself.
 *  See: http://wiki.multimedia.cx/index.php?title=ADTS
 *  Also: http://wiki.multimedia.cx/index.php?title=MPEG-4_Audio#Channel_Configurations
 **/
- (NSData *)adtsData:(NSInteger)channel rawDataLength:(NSInteger)rawDataLength {
    int adtsLength = 7;
    char *packet = malloc(sizeof(char) * adtsLength);
    // Variables Recycled by addADTStoPacket
    int profile = 2;  //AAC LC
    //39=MediaCodecInfo.CodecProfileLevel.AACObjectELD;
    NSInteger freqIdx = 44100;  //44.1KHz
    int chanCfg = (int)channel;  //MPEG-4 Audio Channel Configuration. 1 Channel front-center
    NSUInteger fullLength = adtsLength + rawDataLength;
    // fill in ADTS data
    packet[0] = (char)0xFF;     // 11111111     = syncword
    packet[1] = (char)0xF9;     // 1111 1 00 1  = syncword MPEG-2 Layer CRC
    packet[2] = (char)(((profile-1)<<6) + (freqIdx<<2) +(chanCfg>>2));
    packet[3] = (char)(((chanCfg&3)<<6) + (fullLength>>11));
    packet[4] = (char)((fullLength&0x7FF) >> 3);
    packet[5] = (char)(((fullLength&7)<<5) + 0x1F);
    packet[6] = (char)0xFC;
    NSData *data = [NSData dataWithBytesNoCopy:packet length:adtsLength freeWhenDone:YES];
    return data;
}

- (void)initForFilePath {
    NSString *path = [self GetFilePathByfileName:@"IOSCamDemo_HW.aac"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    NSLog(@"%@", path);
    self->fp = fopen([path cStringUsingEncoding:NSUTF8StringEncoding], "wb");
}

- (NSString *)GetFilePathByfileName:(NSString*)filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writablePath = [documentsDirectory stringByAppendingPathComponent:filename];
    return writablePath;
}

@end
