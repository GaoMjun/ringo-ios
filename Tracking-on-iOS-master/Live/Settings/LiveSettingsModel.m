//
//  LiveSettingsModel.m
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "LiveSettingsModel.h"

@implementation LiveSettingsModel

- (NSArray *)settingsKinds {
    if (!_settingsKinds) {
        _settingsKinds = @[@"服务器地址",
                           @"用户ID",
                           @"观看地址",
                           @"分辨率",
                           @"码率"];
    }
    
    return _settingsKinds;
}

@end
