//
//  LiveSettingsTableViewController.m
//  Ringo-iOS
//
//  Created by qq on 17/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "LiveSettingsTableViewController.h"
#import "LiveSettingsModel.h"
#import <BlocksKit/BlocksKit.h>
#import <BlocksKit/UITextField+BlocksKit.h>
#import "MBProgressHUD+Toast.h"

@interface LiveSettingsTableViewController ()

@property (strong, nonatomic) LiveSettingsModel *liveSettingsModel;

@property (strong, nonatomic) NSString *publishUri;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *publishUrl;
@property (strong, nonatomic) NSString *dimention;
@property (strong, nonatomic) NSString *bitrate;
@property (strong, nonatomic) NSString *watchUrl;

@property (strong, nonatomic) UITextField *inputUriTextField;
@property (strong, nonatomic) UITextField *inputIdTextField;

@end

@implementation LiveSettingsTableViewController

- (UITextField *)inputUrlTextField {
    if (!_inputUriTextField) {
        _inputUriTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, self.view.bounds.size.width, 44)];
        _inputUriTextField.text = self.publishUri;
        
        __weak typeof(self) _self = self;
        [_inputUriTextField setBk_didEndEditingBlock:^(UITextField *textField) {
            if (![textField.text isEqualToString:@"rtmp://hwbztl.8686c.com/digitalincloud/"]) {
                [_self changePublishUriConfirmAlert:^(BOOL confirm) {
                    if (confirm) {
                        _self.publishUri = textField.text;
                        
                        [[NSUserDefaults standardUserDefaults] setObject:_self.publishUri forKey:@"publishUri"];
                        
                        _self.publishUrl = [NSString stringWithFormat:@"%@%@", _self.publishUri, _self.userId];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:_self.publishUrl forKey:@"publishUrl"];
                    } else {
                        textField.text = @"rtmp://hwbztl.8686c.com/digitalincloud/";
                    }
                }];
                
            } else {
                _self.publishUri = textField.text;
                
                [[NSUserDefaults standardUserDefaults] setObject:_self.publishUri forKey:@"publishUri"];
                
                _self.publishUrl = [NSString stringWithFormat:@"%@%@", _self.publishUri, _self.userId];
                
                [[NSUserDefaults standardUserDefaults] setObject:_self.publishUrl forKey:@"publishUrl"];
            }
        }];
        
        [_inputUriTextField setBk_shouldReturnBlock:^BOOL(UITextField *textField) {
            [_self.inputIdTextField becomeFirstResponder];
            return NO;
        }];
    }
    
    return _inputUriTextField;
}

- (void)changePublishUriConfirmAlert:(void (^)(BOOL confirm))confirm {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"确定修改默认服务器地址" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        confirm(NO);
    }];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        confirm(YES);
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (UITextField *)inputIdTextField {
    if (!_inputIdTextField) {
        _inputIdTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, self.view.bounds.size.width, 44)];
        _inputIdTextField.text = self.userId;
        
        __weak typeof(self) _self = self;
        [_inputIdTextField setBk_didEndEditingBlock:^(UITextField *textField) {
            _self.userId = textField.text;
            
            [[NSUserDefaults standardUserDefaults] setObject:_self.userId forKey:@"userId"];
            
            _self.publishUrl = [NSString stringWithFormat:@"%@%@", _self.publishUri, _self.userId];
            
            [[NSUserDefaults standardUserDefaults] setObject:_self.publishUrl forKey:@"publishUrl"];
            
            [_self generateWatchUrl];
            [_self.tableView reloadData];
        }];
    }
    
    return _inputIdTextField;
}

- (LiveSettingsModel *)liveSettingsModel {
    if (!_liveSettingsModel) {
        _liveSettingsModel = [[LiveSettingsModel alloc] init];
    }
    
    return _liveSettingsModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [self loadConfiguration];
    
    [self.tableView reloadData];
}

- (void)loadConfiguration {
    
    NSString *publishUri = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUri"];
    if (!publishUri) {
        [[NSUserDefaults standardUserDefaults] setObject:@"rtmp://hwbztl.8686c.com/digitalincloud/" forKey:@"publishUri"];
        publishUri = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUri"];
    }
    self.publishUri = publishUri;
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (!userId) {
        [[NSUserDefaults standardUserDefaults] setObject:@"00274705" forKey:@"userId"];
        userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    }
    self.userId = userId;
    
    NSString *publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
    if (!publishUrl) {
        [[NSUserDefaults standardUserDefaults] setObject:@"rtmp://hwbztl.8686c.com/digitalincloud/00274705" forKey:@"publishUrl"];
        publishUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"publishUrl"];
    }
    self.publishUrl = publishUrl;
    
    NSString *dimention = [[NSUserDefaults standardUserDefaults] stringForKey:@"dimention"];
    if (!dimention) {
        [[NSUserDefaults standardUserDefaults] setObject:@"640 x 360" forKey:@"dimention"];
        dimention = [[NSUserDefaults standardUserDefaults] stringForKey:@"dimention"];
    }
    self.dimention = dimention;
    
    NSString *bitrate = [[NSUserDefaults standardUserDefaults] stringForKey:@"bitrate"];
    if (!bitrate) {
        [[NSUserDefaults standardUserDefaults] setObject:@"200kbps" forKey:@"bitrate"];
        bitrate = [[NSUserDefaults standardUserDefaults] stringForKey:@"bitrate"];
    }
    self.bitrate = bitrate;
    
    [self generateWatchUrl];
}

- (void)generateWatchUrl {
    NSString *baseUrl = @"http://hwbzll.8686c.com/digitalincloud/";
    NSString *userId = self.userId;
    NSString *tail = @"/playlist.m3u8";
    
    self.watchUrl = [NSString stringWithFormat:@"%@%@%@", baseUrl, userId, tail];
}

- (void)setupViews {
    self.title = @"推流设置";
    
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.liveSettingsModel.settingsKinds.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];

    switch (indexPath.section) {
        case 0:
//            cell.textLabel.text = self.pulishUrl;
            cell.accessoryType = UITableViewCellAccessoryNone;
            [cell.contentView addSubview:self.inputUrlTextField];
            break;
            
        case 1:
            cell.accessoryType = UITableViewCellAccessoryNone;
            [cell.contentView addSubview:self.inputIdTextField];
            break;
            
        case 2:
            cell.textLabel.text = self.watchUrl;
            
            break;
            
        case 3:
            cell.textLabel.text = self.dimention;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
            
        case 4:
            cell.textLabel.text = self.bitrate;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.liveSettingsModel.settingsKinds[section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return FLT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
            
            break;
            
        case 1:
            
            break;
            
        case 2:
            [[UIPasteboard generalPasteboard] setString:self.watchUrl];
            [MBProgressHUD toast:self.view toastStr:@"观看地址已复制"];
            break;
            
        case 3:
            [self performSegueWithIdentifier:@"pushToDimentionSelector" sender:nil];
            break;
            
        case 4:
            [self performSegueWithIdentifier:@"pushToBitrateSelector" sender:nil];
            break;
            
        default:
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeader = [[UITableViewHeaderFooterView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    
    tableViewHeader.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [tableViewHeader.textLabel setTextColor:[UIColor lightGrayColor]];
    tableViewHeader.textLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightThin];
    
    return tableViewHeader;
}

- (IBAction)dismissSelf:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
