//
//  BitrateSelectorModel.m
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "BitrateSelectorModel.h"

@implementation BitrateSelectorModel

- (NSArray *)bitrates {
    if (!_bitrates) {
        _bitrates = @[@"100kbps",
                      @"200kbps",
                      @"400kbps",
                      @"600kbps",
                      @"800kbps",
                      @"1000kbps",
                      @"1500kbps",
                      @"2000kbps"];
    }
    
    return _bitrates;
}
@end
