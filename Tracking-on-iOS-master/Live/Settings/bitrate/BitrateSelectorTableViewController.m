//
//  BitrateSelectorTableViewController.m
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "BitrateSelectorTableViewController.h"
#import "BitrateSelectorModel.h"

@interface BitrateSelectorTableViewController ()

@property (strong, nonatomic) BitrateSelectorModel *bitrateSelectorModel;

@end

@implementation BitrateSelectorTableViewController

- (BitrateSelectorModel *)bitrateSelectorModel {
    if (!_bitrateSelectorModel) {
        _bitrateSelectorModel = [[BitrateSelectorModel alloc] init];
    }
    
    return _bitrateSelectorModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
}

- (void)setupViews {
    self.title = @"选择码率";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bitrateSelectorModel.bitrates.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = self.bitrateSelectorModel.bitrates[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *bitrate = cell.textLabel.text;
    [[NSUserDefaults standardUserDefaults] setObject:bitrate forKey:@"bitrate"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
