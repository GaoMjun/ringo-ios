//
//  DimentionSelectorModel.m
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "DimentionSelectorModel.h"

@implementation DimentionSelectorModel

- (NSArray *)dimentions {
    if (!_dimentions) {
        _dimentions = @[@"1280 x 720",
                        @"852 x 480",
                        @"640 x 360",
                        @"426 x 240",
                        @"256 x 144"];
    }
    
    return _dimentions;
}

@end
