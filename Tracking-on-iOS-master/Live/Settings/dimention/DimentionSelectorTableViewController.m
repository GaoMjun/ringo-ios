//
//  DimentionSelectorTableViewController.m
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "DimentionSelectorTableViewController.h"
#import "DimentionSelectorModel.h"

@interface DimentionSelectorTableViewController ()

@property (strong, nonatomic) DimentionSelectorModel *dimentionSelectorModel;

@end

@implementation DimentionSelectorTableViewController

- (DimentionSelectorModel *)dimentionSelectorModel {
    if (!_dimentionSelectorModel) {
        _dimentionSelectorModel = [[DimentionSelectorModel alloc] init];
    }
    
    return _dimentionSelectorModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
}

- (void)setupViews {
    self.title = @"选择分辨率";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dimentionSelectorModel.dimentions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = self.dimentionSelectorModel.dimentions[indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *dimention = cell.textLabel.text;
    [[NSUserDefaults standardUserDefaults] setObject:dimention forKey:@"dimention"];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
