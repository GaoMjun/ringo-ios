//
//  DimentionSelectorModel.h
//  Ringo-iOS
//
//  Created by qq on 18/1/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DimentionSelectorModel : NSObject

@property (strong, nonatomic) NSArray *dimentions;

@end
