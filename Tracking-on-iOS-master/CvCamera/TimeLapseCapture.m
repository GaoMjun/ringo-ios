//
//  TimeLapseCapture.m
//  Ringo-iOS
//
//  Created by qq on 6/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "TimeLapseCapture.h"

@interface TimeLapseCapture()

@property (nonatomic, strong) AVAssetWriter *assetWriter;
@property (nonatomic, strong) AVAssetWriterInput *videoAssetWriterInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *assetWriterInputPixelBufferAdaptor;

@property (nonatomic, assign) NSUInteger frameCount;

@property (nonatomic, strong) NSTimer *writeTimer;
@property (nonatomic, strong) NSTimer *durationTimer;

@property(nonatomic) BOOL canWrite;
@property(nonatomic, strong) NSURL *url_t;

@property (nonatomic, copy) void(^finish)(NSURL *url);

@property (assign, nonatomic) BOOL recording;

@end

@implementation TimeLapseCapture

- (instancetype)initWithShutter:(Float32)shutter duration:(NSUInteger)duration {
    self = [super init];
    if (self) {
        _shutter = shutter;
        _duration = duration;
        
        [self initTimeLapseCapture];
    }
    return self;
}

- (void)initTimeLapseCapture {


}

- (void)startWithFinish:(void(^)(NSURL *url))finish {
    self.finish = finish;

    [self start];
}

- (void)terminate:(void(^)(NSURL *url))finish {
    if (self.recording) {
        self.finish = finish;
        
        [self stop];
    }
}

- (void)start {
//    NSLog(@"%s", __func__);

    self.recording = YES;
    
    self.frameCount = 0;

    self.writeTimer = [NSTimer timerWithTimeInterval:self.shutter target:self selector:@selector(startWrite) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.writeTimer forMode:NSRunLoopCommonModes];

    self.durationTimer = [NSTimer timerWithTimeInterval:self.duration target:self selector:@selector(stop) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:self.durationTimer forMode:NSRunLoopCommonModes];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy.MM.dd.HH:mm:SS"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *tmpPath = [NSString stringWithFormat:@"%@video%@.mov", NSTemporaryDirectory(), dateString];
    self.url_t = [NSURL fileURLWithPath:tmpPath];
    self.assetWriter = [[AVAssetWriter alloc] initWithURL:self.url_t fileType:AVFileTypeQuickTimeMovie error:nil];

    NSDictionary *videoOutputSettings = @{
            AVVideoCodecKey : AVVideoCodecH264,
            AVVideoWidthKey : @(self.height),
            AVVideoHeightKey : @(self.width),
            AVVideoCompressionPropertiesKey : @{
                    AVVideoExpectedSourceFrameRateKey : @(30),
                    AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
                    AVVideoAverageBitRateKey : @(self.width*self.height * 11.4),
                    AVVideoMaxKeyFrameIntervalKey : @(30)
            }
    };
    self.videoAssetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoOutputSettings];
    self.videoAssetWriterInput.expectsMediaDataInRealTime = YES;

    self.assetWriterInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.videoAssetWriterInput
                                                                                                               sourcePixelBufferAttributes:nil];

    NSParameterAssert([self.assetWriter canAddInput:self.videoAssetWriterInput]);

    [self.assetWriter addInput:self.videoAssetWriterInput];

    [self.assetWriter startWriting];

    [self.assetWriter startSessionAtSourceTime:kCMTimeZero];

}

- (void)stop {
//    NSLog(@"%s", __func__);
    self.recording = NO;
    
    self.canWrite = NO;

    [self.writeTimer invalidate];
    self.writeTimer = nil;

    [self.durationTimer invalidate];
    self.durationTimer = nil;

    [self.videoAssetWriterInput markAsFinished];
    [self.assetWriter finishWritingWithCompletionHandler:^{
        if (self.assetWriter.status == AVAssetWriterStatusCompleted) {
            self.finish(self.url_t);
        } else {
            self.finish(nil);
        }

        CVPixelBufferPoolRelease(self.assetWriterInputPixelBufferAdaptor.pixelBufferPool);
    }];
}

- (void)feedImage:(CVPixelBufferRef)imageBuffer {
    if (self.videoAssetWriterInput.readyForMoreMediaData && self.canWrite) {
//        NSLog(@"%s", __func__);
        [self.assetWriterInputPixelBufferAdaptor appendPixelBuffer:imageBuffer withPresentationTime:CMTimeMake(self.frameCount++, 30)];
        self.canWrite = NO;
    }
}

- (void)startWrite {
    self.canWrite = YES;
}

@end
