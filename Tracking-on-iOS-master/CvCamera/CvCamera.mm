//
//  CvCamera.m
//  AVCaptureSession
//
//  Created by ; on 21/10/2016.
//  Copyright © 2016 GitHub. All rights reserved.
//

#import "CvCamera.h"
#import "H264Encoder.h"
#import "AACEncoder.h"
#import <QuartzCore/CABase.h>
#import "InstaFilters.h"
#import "Masonry.h"
#import "TimeLapseCapture.h"

#ifdef __cplusplus

using namespace std;
using namespace cv;

#endif

@interface CvCamera () <AVCaptureVideoDataOutputSampleBufferDelegate,
AVCaptureAudioDataOutputSampleBufferDelegate,
AVCaptureMetadataOutputObjectsDelegate,
GPUImageVideoCameraDelegate,
GPUImageSampleBufferDelegate,
AVCaptureFileOutputRecordingDelegate> {
    
    GPUImageStillCamera *gpuCamera;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageOutput<GPUImageInput> *emptyFilter;
    GPUImageMovieWriter *movieWriter;
    GPUImageRawDataOutput *rawDataOutput;
    GPUImageOutput<GPUImageInput> *output;
}

@property (nonatomic, strong) dispatch_queue_t cvProcessQueue;
@property (nonatomic, strong) dispatch_queue_t audioOutputProcessQueue;
@property (nonatomic, strong) dispatch_queue_t videoOutputProcessQueue;
@property (nonatomic, strong) dispatch_queue_t movieWritingQueue;

@property (nonatomic, copy) void(^frameProcessingCompletionBlock)(GPUImageOutput*, CMTime);

@property (nonatomic, copy) void(^finalOutputBlock)(GPUImageOutput*, CMTime);

@property (nonatomic, copy) void(^finishRecordBlock)(NSURL *videoTmpUrl);

// session
@property (nonatomic, strong) AVCaptureSession *captureSession;

// metadata
@property (nonatomic, strong) AVCaptureMetadataOutput *metadataFaceOutput;

// video
@property (nonatomic, strong) AVCaptureDevice *videoCaptureDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *videoCaptureDeviceInput;
@property (nonatomic, strong) AVCaptureVideoDataOutput *captureVideoDataOutput;
@property (nonatomic, strong) AVCaptureConnection *captureVideoDataOutputConnection;

// audio
@property (nonatomic, strong) AVCaptureDevice *audioCaptureDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *audioCaptureDeviceInput;
@property (nonatomic, strong) AVCaptureAudioDataOutput *captureAudioDataOutput;
@property (nonatomic, strong) AVCaptureConnection *captureAudioDataOutputConnection;

// photo
@property (nonatomic, strong) AVCaptureStillImageOutput *captureStillImageOutput;
@property (nonatomic, strong) AVCaptureConnection *captureStillImageOutputConnection;

// preview
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, strong) UIView *preview;

// asset writer
@property (nonatomic, strong) AVAssetWriter *assetWriter;
@property (nonatomic, strong) AVAssetWriterInput *videoAssetWriterInput;
@property (nonatomic, strong) AVAssetWriterInput *audioAssetWriterInput;
//@property (nonatomic, strong) NSURL *assetUrl;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *pixelBufferAdaptor;

@property (nonatomic, strong) AVCaptureMovieFileOutput *movieFileOutput;


//@property (nonatomic, assign) BOOL recording;

@property (nonatomic, strong) H264Encoder *h264Encoder;
@property (nonatomic, strong) AACEncoder *aacEncoder;

@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;

@property (nonatomic, assign) CGFloat currentZoom;

@property (nonatomic, strong) NSTimer *fpsTimer;

@property (nonatomic, assign) NSUInteger fps;


@property (nonatomic, strong) AVCaptureDeviceFormat *currentFormat;

//@property (nonatomic, assign) CvCameraFormat currentCvCameraFormat;
@property (nonatomic, strong) AVFrameRateRange *currentFrameRateRange;
@property (nonatomic, assign) CMFormatDescriptionRef currentVideoFormatDescription;
@property (nonatomic, assign) CMFormatDescriptionRef currentAudioFormatDescription;

@property (nonatomic, strong) UIView *capturePreview;

@property (nonatomic, strong) TimeLapseCapture *timeLapseCapture;

@end

@implementation CvCamera

- (AVCaptureDevicePosition)cameraPosition {
//    return self.videoCaptureDevice.position;
    if (gpuCamera) {
        return gpuCamera.inputCamera.position;
    }

    if (self.captureSession) {
        return self.videoCaptureDevice.position;
    }
    
    return AVCaptureDevicePositionUnspecified;
}

- (NSString *)captureSessionPreset {
    return gpuCamera.captureSessionPreset;
}

- (UIInterfaceOrientation)previewOrientation {
    return gpuCamera.outputImageOrientation;
}

- (void)configureSession {
    self.captureSession = [[AVCaptureSession alloc] init];
    if ([self.captureSession canSetSessionPreset:AVCaptureSessionPresetInputPriority]) {
        self.captureSession.sessionPreset = AVCaptureSessionPresetInputPriority;
    }
}

- (void)configureMetadataOutput {
    self.metadataFaceOutput = [[AVCaptureMetadataOutput alloc] init];
    
    if ([self.captureSession canAddOutput:self.metadataFaceOutput]) {
        [self.captureSession addOutput:self.metadataFaceOutput];
        
        [self.metadataFaceOutput setMetadataObjectsDelegate:self queue:dispatch_queue_create("metadataFaceOutputQueue", DISPATCH_QUEUE_SERIAL)];
        
        if ([self.metadataFaceOutput.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeFace]) {
            self.metadataFaceOutput.metadataObjectTypes = @[AVMetadataObjectTypeFace];
        } else {
            NSLog(@"not support AVMetadataObjectTypeFace");
        }
    }
}

- (void)configureVideoWithCameraPosition:(AVCaptureDevicePosition)position {
    self.videoCaptureDevice = [self cameraWithPosition:position];
    self.videoCaptureDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:self.videoCaptureDevice error:nil];
    
    if ([self.captureSession canAddInput:self.videoCaptureDeviceInput]) {
        [self.captureSession addInput:self.videoCaptureDeviceInput];
    }
    
    self.captureVideoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
    self.captureVideoDataOutput.alwaysDiscardsLateVideoFrames = YES;
    self.captureVideoDataOutput.videoSettings = [NSDictionary dictionaryWithObject: [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                                            forKey: (id)kCVPixelBufferPixelFormatTypeKey];
    if ([self.captureSession canAddOutput:self.captureVideoDataOutput]) {
        [self.captureSession addOutput:self.captureVideoDataOutput];
        [self.captureVideoDataOutput setSampleBufferDelegate:self queue:dispatch_queue_create("captureVideoDataOutputQueue", DISPATCH_QUEUE_SERIAL)];
    }
    
    self.captureVideoDataOutputConnection = [self.captureVideoDataOutput connectionWithMediaType:AVMediaTypeVideo];
    switch (UIDevice.currentDevice.orientation) {
        case UIDeviceOrientationPortrait:
            self.captureVideoDataOutputConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            self.captureVideoDataOutputConnection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
            break;
        case UIDeviceOrientationLandscapeLeft:
            self.captureVideoDataOutputConnection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
            break;
        case UIDeviceOrientationLandscapeRight:
            self.captureVideoDataOutputConnection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
            break;
            
        default:
            self.captureVideoDataOutputConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
            break;
    }
    
    if (position == AVCaptureDevicePositionFront) {
        self.captureVideoDataOutputConnection.videoMirrored = YES;
    }
}

- (void)configureAudio {
    self.audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    self.audioCaptureDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:self.audioCaptureDevice error:nil];
    
    if ([self.captureSession canAddInput:self.audioCaptureDeviceInput]) {
        [self.captureSession addInput:self.audioCaptureDeviceInput];
    }
    
    self.captureAudioDataOutput = [[AVCaptureAudioDataOutput alloc] init];
    if ([self.captureSession canAddOutput:self.captureAudioDataOutput]) {
        [self.captureSession addOutput:self.captureAudioDataOutput];
        [self.captureAudioDataOutput setSampleBufferDelegate:self queue:dispatch_queue_create("captureAudioDataOutputQueue", DISPATCH_QUEUE_SERIAL)];
    }
    
    self.captureAudioDataOutputConnection = [self.captureAudioDataOutput connectionWithMediaType:AVMediaTypeAudio];
}

- (void)configurePreview {

    
    
    [self.capturePreview removeFromSuperview];
    self.capturePreview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.capturePreview.backgroundColor = [UIColor clearColor];
    [self.cameraPreview insertSubview:self.capturePreview atIndex:0];
    [self.capturePreview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.cameraPreview).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        
        
    }];
    
    self.captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.captureVideoPreviewLayer.frame = self.cameraPreview.bounds;
//        NSLog(@"%@", NSStringFromCGRect(self.captureVideoPreviewLayer.frame));
        self.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self.captureVideoPreviewLayer.connection.videoOrientation = self.captureVideoDataOutputConnection.videoOrientation;
        
        [self.capturePreview.layer addSublayer:self.captureVideoPreviewLayer];
    });
    
    
}

- (void)configurePhoto {
    self.captureStillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    self.captureStillImageOutput.highResolutionStillImageOutputEnabled = YES;
    if ([self.captureSession canAddOutput:self.captureStillImageOutput]) {
        [self.captureSession addOutput:self.captureStillImageOutput];
    }
    self.captureStillImageOutputConnection = [self.captureStillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    self.captureStillImageOutputConnection.videoOrientation = self.captureVideoDataOutputConnection.videoOrientation;
    if (self.videoCaptureDevice.position == AVCaptureDevicePositionFront) {
        self.captureStillImageOutputConnection.videoMirrored = YES;
    }
}

- (void)configureAssetWriter {
    // config assetsUrl
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmSS"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    
    NSString *tmpPath = [NSString stringWithFormat:@"%@video%@.mov", NSTemporaryDirectory(), dateString];
    self.assetUrl = [NSURL fileURLWithPath:tmpPath];
    self.assetWriter = [AVAssetWriter assetWriterWithURL:self.assetUrl fileType:AVFileTypeQuickTimeMovie error:nil];
    
//    NSDictionary *videoOutputSettings = [self.captureVideoDataOutput recommendedVideoSettingsForAssetWriterWithOutputFileType:AVFileTypeMPEG4];
    NSDictionary *videoOutputSettings = nil;
    NSDictionary *adaptorDict = nil;
    if ([UIScreen mainScreen].bounds.size.width > [UIScreen mainScreen].bounds.size.height) {
        videoOutputSettings = @{AVVideoCodecKey : AVVideoCodecH264,
                                AVVideoWidthKey : @(self.width),
                                AVVideoHeightKey : @(self.height),
                                AVVideoCompressionPropertiesKey : @{
                                        AVVideoExpectedSourceFrameRateKey : @(self.currentFrameRateRange.maxFrameRate),
                                        AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
                                        AVVideoAverageBitRateKey : @(self.width*self.height * 11.4),
                                        AVVideoMaxKeyFrameIntervalKey : @(30)
                                        }
                                };
        adaptorDict = @{
                        (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA),
                        (id)kCVPixelBufferWidthKey : @(self.width),
                        (id)kCVPixelBufferHeightKey : @(self.height)
                        };
    } else {
        videoOutputSettings = @{AVVideoCodecKey : AVVideoCodecH264,
                                AVVideoWidthKey : @(self.height),
                                AVVideoHeightKey : @(self.width),
                                AVVideoCompressionPropertiesKey : @{
                                        AVVideoExpectedSourceFrameRateKey : @(self.currentFrameRateRange.maxFrameRate),
                                        AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
                                        AVVideoAverageBitRateKey : @(self.width*self.height * 11.4),
                                        AVVideoMaxKeyFrameIntervalKey : @(30)
                                        }
                                };
        adaptorDict = @{
                        (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA),
                        (id)kCVPixelBufferWidthKey : @(self.height),
                        (id)kCVPixelBufferHeightKey : @(self.width)
                        };
    }
   
//    self.videoAssetWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoOutputSettings];
    self.videoAssetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoOutputSettings];
    self.videoAssetWriterInput.expectsMediaDataInRealTime = YES;
    self.pixelBufferAdaptor = [[AVAssetWriterInputPixelBufferAdaptor alloc] initWithAssetWriterInput:self.videoAssetWriterInput
                                                                         sourcePixelBufferAttributes:adaptorDict];
//    self.videoAssetWriterInput.transform = CGAffineTransformMakeRotation(M_PI_2);
    if ([self.assetWriter canAddInput:self.videoAssetWriterInput]) {
        [self.assetWriter addInput:self.videoAssetWriterInput];
    }
    
    const AudioStreamBasicDescription *currentASBD = CMAudioFormatDescriptionGetStreamBasicDescription(self.currentAudioFormatDescription);
    size_t aclSize = 0;
    const AudioChannelLayout *currentChannelLayout = CMAudioFormatDescriptionGetChannelLayout(self.currentAudioFormatDescription, &aclSize);
    NSData *currentChannelLayoutData = nil;
    
    // AVChannelLayoutKey must be specified, but if we don't know any better give an empty data and let AVAssetWriter decide.
    if ( currentChannelLayout && aclSize > 0 ) {
        
        currentChannelLayoutData = [NSData dataWithBytes:currentChannelLayout length:aclSize];
    }
    else {
        
        currentChannelLayoutData = [NSData data];
    }
//    NSDictionary *audioOutputSettings = [self.captureAudioDataOutput recommendedAudioSettingsForAssetWriterWithOutputFileType:AVFileTypeAppleM4V];
    NSDictionary *audioOutputSettings = @{AVFormatIDKey : @(kAudioFormatMPEG4AAC),
                                          AVSampleRateKey : @(currentASBD->mSampleRate),
                                          AVEncoderBitRatePerChannelKey : @(64000),
                                          AVNumberOfChannelsKey :@(currentASBD->mChannelsPerFrame),
                                          AVChannelLayoutKey : currentChannelLayoutData
                                          };
    self.audioAssetWriterInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    self.audioAssetWriterInput.expectsMediaDataInRealTime = YES;
    if ([self.assetWriter canAddInput:self.audioAssetWriterInput]) {
        [self.assetWriter addInput:self.audioAssetWriterInput];
    }
}

- (void)configureMovieFileOutput {
    self.movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    
    if ([self.captureSession canAddOutput:self.movieFileOutput]) {
        [self.captureSession addOutput:self.movieFileOutput];
    }
}

- (void)setCameraPreview:(UIView *)cameraPreview {
    _cameraPreview = cameraPreview;
    
    if (!filter) {
        filter = [GPUImageFilter new];
//        GPUImageOverlayBlendFilter *blendFilter = [[GPUImageOverlayBlendFilter alloc] init];
//        [blendFilter useNextFrameForImageCapture];
//        filter = blendFilter;
        [filter setFrameProcessingCompletionBlock:self.finalOutputBlock];
    }

    [gpuCamera addTarget:emptyFilter];
    [emptyFilter addTarget:filter];
    [filter addTarget:(GPUImageView *)_cameraPreview];
}

- (void)switchFilter:(GPUImageOutput<GPUImageInput> *)newFilter {

    [newFilter setFrameProcessingCompletionBlock:self.finalOutputBlock];
    
    [gpuCamera removeAllTargets];
    [self->filter removeAllTargets];
    [self->emptyFilter removeAllTargets];
    
    self->filter = newFilter;

    [gpuCamera addTarget:emptyFilter];
    [emptyFilter addTarget:filter];
    [filter addTarget:(GPUImageView *)_cameraPreview];
}

- (instancetype)initWithSessionPreset:(NSString *)sessionPreset cameraPosition:(AVCaptureDevicePosition)cameraPosition orientation:(UIInterfaceOrientation)orientation
{
    self = [super init];
    if (self) {
        
        self.startEncode = NO;
        
        [self initCaptureWithGPUImage:sessionPreset cameraPosition:cameraPosition orientation:orientation];
    }
    
    self.fpsTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(printFps) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.fpsTimer forMode:NSRunLoopCommonModes];
    
    return self;
}

- (void)initCaptureWithSlowMotion {
    
    if (gpuCamera && gpuCamera.isRunning) {
        [self stop];
        gpuCamera = nil;
    }
    
    [self configureSession];

    [self configureVideoWithCameraPosition:AVCaptureDevicePositionBack];

    [self configureAudio];

//    [self configurePhoto];

    [self configurePreview];

//    [self configureMetadataOutput];

//    [self configureAssetWriter];
    
    [self configureMovieFileOutput];
    
    AVFrameRateRange *slowMotionRateRange = nil;
    AVCaptureDeviceFormat *slowMotionFormat = nil;
    for (AVCaptureDeviceFormat *format in self.videoCaptureDevice.formats) {

        for (AVFrameRateRange *range in format.videoSupportedFrameRateRanges) {
            if (!slowMotionRateRange) {
                slowMotionRateRange = range;
            }
            if (range.maxFrameRate > slowMotionRateRange.maxFrameRate) {
                slowMotionRateRange = range;
                slowMotionFormat = format;
            }
        }
    }

    if ([self.videoCaptureDevice lockForConfiguration:nil]) {
        self.videoCaptureDevice.activeFormat = slowMotionFormat;
        self.videoCaptureDevice.activeVideoMinFrameDuration = CMTimeMake(1, slowMotionRateRange.maxFrameRate);
        self.videoCaptureDevice.activeVideoMaxFrameDuration = CMTimeMake(1, slowMotionRateRange.maxFrameRate);
    }

    
    [self.captureSession startRunning];
}

- (void)initCaptureWithGPUImage:(NSString *)sessionPreset cameraPosition:(AVCaptureDevicePosition)cameraPosition orientation:(UIInterfaceOrientation)orientation{

    gpuCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:sessionPreset cameraPosition:cameraPosition];
    [gpuCamera addAudioInputsAndOutputs];
    gpuCamera.outputImageOrientation = orientation;
    gpuCamera.horizontallyMirrorFrontFacingCamera = YES;
    
    self.currentFormat = gpuCamera.inputCamera.activeFormat;
    
    self.width = CMVideoFormatDescriptionGetDimensions(self.currentFormat.formatDescription).width;
    self.height = CMVideoFormatDescriptionGetDimensions(self.currentFormat.formatDescription).height;
    
    __weak CvCamera *weakSelf = self;
    self.frameProcessingCompletionBlock = ^(GPUImageOutput *output, CMTime time) {
        //            NSLog(@"%s", __func__);
        if (weakSelf.delegate) {
            
            CVPixelBufferRef pixelBuffer = output.framebufferForOutput.pixelBuffer;
            CVPixelBufferLockBaseAddress(pixelBuffer, 0);
            
            uint8_t *bufferAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
            size_t width = CVPixelBufferGetWidth(pixelBuffer);
            size_t height = CVPixelBufferGetHeight(pixelBuffer);
            size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);
            
            // delegate image processing to the delegate
            cv::Mat matImage((int)height, (int)width, CV_8UC4, bufferAddress, bytesPerRow);
            
            CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
            
            cv::resize(matImage, matImage, cv::Size(matImage.cols/10, matImage.rows/10), 0, 0, CV_INTER_NN);
            cv::cvtColor(matImage, matImage, CV_BGRA2BGR);
            [weakSelf.delegate processImage:matImage];
            
        }
    };
    
    emptyFilter = [[GPUImageFilter alloc] init];
    [emptyFilter setFrameProcessingCompletionBlock:self.frameProcessingCompletionBlock];
    
    self.finalOutputBlock = ^(GPUImageOutput *output, CMTime timestape) {
        //            NSLog(@"%lld", timestape.value);
        weakSelf.fps++;
        if (weakSelf.startEncode) {
            
            if (!weakSelf.h264Encoder) {
                weakSelf.h264Encoder = [[H264Encoder alloc] init];
                
            }
            
            CVPixelBufferRef imageBuffer = output.framebufferForOutput.pixelBuffer;
            if (imageBuffer != NULL) {
                [weakSelf.h264Encoder encoding:imageBuffer timestamp:CACurrentMediaTime()*1000];    
            }
            
        } else {
            if (weakSelf.h264Encoder) {
                [weakSelf.h264Encoder stopEncoding];
                weakSelf.h264Encoder = nil;
            }
        }

        if (weakSelf.timeLapseCapture) {
            [weakSelf.timeLapseCapture feedImage:output.framebufferForOutput.pixelBuffer];
        }
    };
    
    gpuCamera.delegate2 = self;
}

- (void)printFps {
    if (self.fps != 0) {
//        NSLog(@"fps: %lu", (unsigned long)self.fps);
    }
    
    self.fps = 0;
}

- (void)startRecord {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmSS"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *tmpPath = [NSString stringWithFormat:@"%@video%@.mov", NSTemporaryDirectory(), dateString];
    self.assetUrl = [NSURL fileURLWithPath:tmpPath];
    
    if (self.currentCvCameraFormat != CvCameraFormatSlowMotion) {
        
        if ([UIScreen mainScreen].bounds.size.width > [UIScreen mainScreen].bounds.size.height) {
            movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:self.assetUrl size:CGSizeMake(self.width, self.height)];
        } else {
            movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:self.assetUrl size:CGSizeMake(self.height, self.width)];
        }
        
        movieWriter.encodingLiveVideo = YES;
        [filter addTarget:movieWriter];
        gpuCamera.audioEncodingTarget = movieWriter;
        
        [movieWriter startRecording];
        
    } else {
//        [self configureAssetWriter];
        
        [self.movieFileOutput startRecordingToOutputFileURL:self.assetUrl recordingDelegate:self];
    }
    

    self.recording = YES;
}

- (void)stopRecord:(void (^)(NSURL *videoTmpUrl))finish {
    self.recording = NO;
    
    if (self.currentCvCameraFormat == CvCameraFormatSlowMotion) {
        
//    if (self.assetWriter.status == AVAssetWriterStatusWriting) {
//#ifdef DEBUG
//        NSLog(@"%ld", (long)self.assetWriter.status);
//#endif
//        [self.videoAssetWriterInput markAsFinished];
//        [self.audioAssetWriterInput markAsFinished];
//        
//        [self.assetWriter finishWritingWithCompletionHandler:^{
//#ifdef DEBUG
//            NSLog(@"finishWritingWithCompletionHandler");
//#endif
//            finish(self.assetUrl);
//        }];
//    }
        self.finishRecordBlock = finish;
        [self.movieFileOutput stopRecording];
        
    } else {
    
        [filter removeTarget:movieWriter];
        gpuCamera.audioEncodingTarget = nil;
        [movieWriter finishRecording];
        finish(self.assetUrl);
    }
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
    self.finishRecordBlock(outputFileURL);
}

- (void)start {
//    if (self.captureSession) {
//        [self.captureSession startRunning];
//    }
    if (self.currentCvCameraFormat == CvCameraFormatSlowMotion) {
        [self changeFormat:CvCameraFormatSlowMotion];
    }
    
    if (gpuCamera) {
        [gpuCamera startCameraCapture];
    }
    
}

- (void)stop {
//    if (self.captureSession) {
//        [self.captureSession stopRunning];
//    }
    if (self.currentCvCameraFormat == CvCameraFormatSlowMotion) {
        if (self.captureSession && self.captureSession.isRunning) {
            [self.captureSession stopRunning];
            self.captureSession = nil;
            [self.capturePreview removeFromSuperview];
        }
    } else {
        [gpuCamera stopCameraCapture];
    }
}

- (void)changeOrientation:(UIInterfaceOrientation)orientation {
    gpuCamera.outputImageOrientation = orientation;
}

- (void)willOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer sampleBufferType:(SampleBufferType)type {
    if (type == SampleBufferTypeVideo) {
//        NSLog(@"willOutputSampleBuffer: video");
        
    } else if (type == SampleBufferTypeAudio) {
        if (self.startEncode) {
            
            if (!self.aacEncoder) {
                self.aacEncoder = [[AACEncoder alloc] init];
                
            }
            
            CMBlockBufferRef blockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer);
            
            CFRetain(blockBuffer);
            
            char *dataPointer;
            size_t totalLength;
            
            OSStatus status =
            CMBlockBufferGetDataPointer(blockBuffer,
                                        0,
                                        NULL,
                                        &totalLength,
                                        &dataPointer);
            
            CFRelease(blockBuffer);
            
            if ((status == 0) && (dataPointer != NULL) && (totalLength > 0)) {
                NSData *data = [NSData dataWithBytes:dataPointer length:totalLength];
                [self.aacEncoder encoding:data timestamp:CACurrentMediaTime()*1000];
            }
        } else {
            
            if (self.aacEncoder) {
                [self.aacEncoder stopEncoding];
                self.aacEncoder = nil;
            }
        }
    }
}

- (void)rotateMat:(cv::Mat &)matImage angle:(double)angle {
    cv::Point2f center(matImage.cols/2, matImage.rows/2);
    
    cv::Mat rot_mat = cv::getRotationMatrix2D(center, angle, 1.0);
    
    cv::Rect bbox = cv::RotatedRect(center, matImage.size(), angle).boundingRect();
    rot_mat.at<double>(0,2) += bbox.width/2.0 - center.x;
    rot_mat.at<double>(1,2) += bbox.height/2.0 - center.y;
    cv::warpAffine(matImage, matImage, rot_mat, bbox.size());
}

#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
    
    if (captureOutput == self.captureVideoDataOutput) {
//        NSLog(@"captureVideoDataOutput");
        if (!self.currentVideoFormatDescription) {
            CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
            self.currentVideoFormatDescription = formatDescription;
        }
        self.fps++;
    } else if (captureOutput == self.captureAudioDataOutput) {
//        NSLog(@"captureAudioDataOutput");
        if (!self.currentAudioFormatDescription) {
            CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
            self.currentAudioFormatDescription = formatDescription;
        }
    }
    if (self.recording) {
//        NSLog(@"%ld", (long)self.assetWriter.status);
        if (!self.movieWritingQueue) {
            self.movieWritingQueue = dispatch_queue_create("movieWritingQueue", DISPATCH_QUEUE_SERIAL);
        } else {
            CFRetain(sampleBuffer);
            dispatch_async(self.movieWritingQueue, ^{
                
//                @synchronized (self) {
                
                    if ((self.assetWriter.status == AVAssetWriterStatusUnknown)) {
                        
                        [self.assetWriter startWriting];
                        //                NSLog(@"%ld", (long)self.assetWriter.status);
                        [self.assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
                        //                NSLog(@"%ld", (long)self.assetWriter.status);
                    }
//                }
                
                if (self.assetWriter.status == AVAssetWriterStatusWriting) {
                    
                    if (connection == self.captureVideoDataOutputConnection) {
#ifdef DEBUG1
                        NSLog(@"video data output");
#endif
                        if (self.videoAssetWriterInput.readyForMoreMediaData) {
                            [self.videoAssetWriterInput appendSampleBuffer:sampleBuffer];
                        }
                        
                    } else if (connection == self.captureAudioDataOutputConnection) {
#ifdef DEBUG1
                        NSLog(@"audio data output");
#endif
                        if (self.audioAssetWriterInput.readyForMoreMediaData) {
                            [self.audioAssetWriterInput appendSampleBuffer:sampleBuffer];
                        }
                        
                    }
                }
                CFRelease(sampleBuffer);
            });
            
        }
    }
    
    if (self.delegate) {
        if (connection == self.captureVideoDataOutputConnection) {
            
            if ([_delegate respondsToSelector:@selector(processImage:)]) {
                
                CVPixelBufferRef imageBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
                
                CVPixelBufferLockBaseAddress(imageBuffer, 0);

                void *bufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
                size_t width = CVPixelBufferGetWidth(imageBuffer);
                size_t height = CVPixelBufferGetHeight(imageBuffer);
                size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
                
                // delegate image processing to the delegate
                cv::Mat matImage((int)height, (int)width, CV_8UC4, bufferAddress, bytesPerRow);
                cv::resize(matImage, matImage, cv::Size(matImage.cols/10, matImage.rows/10), 0, 0, CV_INTER_LINEAR);
                cv::cvtColor(matImage, matImage, CV_BGRA2GRAY);
                cv::equalizeHist(matImage, matImage);
                
                [_delegate processImage:matImage];
                
                CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
            }
        }
    }
    
    if (self.startEncode) {
        if (connection == self.captureVideoDataOutputConnection) {
            if (self.h264Encoder == nil) {
                self.h264Encoder = [[H264Encoder alloc] init];

            }
            
            CVPixelBufferRef imageBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
            if (imageBuffer != NULL) {
                [self.h264Encoder encoding:imageBuffer timestamp:CACurrentMediaTime()*1000];
            }
            
        } else if (connection == self.captureAudioDataOutputConnection) {
            if (self.aacEncoder == nil) {
                self.aacEncoder = [[AACEncoder alloc] init];

            }
            
            CMBlockBufferRef blockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer);
            
            CFRetain(blockBuffer);
            
            char *dataPointer;
            size_t totalLength;
            
            OSStatus status =
            CMBlockBufferGetDataPointer(blockBuffer,
                                        0,
                                        NULL,
                                        &totalLength,
                                        &dataPointer);
            
            CFRelease(blockBuffer);
            
            if ((status == 0) && (dataPointer != NULL) && (totalLength > 0)) {
                NSData *data = [NSData dataWithBytes:dataPointer length:totalLength];
                [self.aacEncoder encoding:data timestamp:CACurrentMediaTime()*1000];
            }
        }
    } else {
        if (self.h264Encoder) {
            [self.h264Encoder stopEncoding];
            self.h264Encoder = nil;
        }
        
        if (self.aacEncoder) {
            [self.aacEncoder stopEncoding];
            self.aacEncoder = nil;
        }
    }
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position{
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in devices)
        
        if (device.position == position ) {
            
            return device;
        }
    
    return nil;
}

- (void)switchCamera {
//    [self.captureSession beginConfiguration];
//    
//    [self.captureSession removeInput:self.videoCaptureDeviceInput];
//    [self.captureSession removeInput:self.audioCaptureDeviceInput];
//    
//    [self.captureSession removeOutput:self.captureVideoDataOutput];
//    [self.captureSession removeOutput:self.captureAudioDataOutput];
//    [self.captureSession removeOutput:self.captureStillImageOutput];
//    
//    if ([self.captureSession.outputs containsObject:self.metadataFaceOutput]) {
//        [self.captureSession removeOutput:self.metadataFaceOutput];
//    }
//    
//    if (self.videoCaptureDevice.position != AVCaptureDevicePositionFront) {
//        [self configureVideoWithCameraPosition:AVCaptureDevicePositionFront];
//    } else {
//        [self configureVideoWithCameraPosition:AVCaptureDevicePositionBack];
//    }
//    
//    [self configureAudio];
//    
//    [self configurePhoto];
//    
////    [self configureAssetWriter];
//    
//    [self configureMetadataOutput];
//    
//    [self.captureSession commitConfiguration];
    
    [gpuCamera rotateCamera];
}

- (void)takePhoto:(void (^)(UIImage *image))success {

//    if (self.captureStillImageOutput) {
//
//        if (self.captureStillImageOutputConnection.enabled) {
//            
//            [self.captureStillImageOutput captureStillImageAsynchronouslyFromConnection:self.captureStillImageOutputConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
//                
//                if (!error) {
//                    NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
//                    UIImage *image = [[UIImage alloc] initWithData:imageData];
//                    success(image);
//                } else {
//#ifdef DEBUG
//                    NSLog(@"%@", error);
//#endif
//                }
//            }];
//        }
//    }
    
    [gpuCamera capturePhotoAsImageProcessedUpToFilter:filter withOrientation:UIImageOrientationUp withCompletionHandler:^(UIImage *processedImage, NSError *error) {
        success(processedImage);
    }];
}

- (void)zoomCamera:(CGFloat)value {
    if (value == 0) {
        return;
    }
    self.currentZoom += value * self.currentZoom * 0.05 + value;
    self.currentZoom = self.currentZoom < 10 ? 10 : self.currentZoom;
    self.currentZoom = self.currentZoom > 100 ? 100 : self.currentZoom;
    
    
    
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
    if ([camera lockForConfiguration:&error]) {
        CGFloat max = MIN(camera.activeFormat.videoMaxZoomFactor, 10);
        
        CGFloat scale = self.currentZoom/10;
        if (scale > max) {
            scale = max;
        }
        if (scale < 1.0) {
            scale = 1.0;
        }
        
//        NSLog(@"%f", scale);
        
        camera.videoZoomFactor = scale;
        
        [camera unlockForConfiguration];
    }

}

- (void)cameraFlashMode:(CvCameraFlashMode)flashMode {
    if (self.cameraPosition == AVCaptureDevicePositionFront) {
        return;
    }
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
    if ([camera lockForConfiguration:&error]) {

        switch (flashMode) {
            case CvCameraFlashModeOff:
                camera.flashMode = AVCaptureFlashModeOff;
                camera.torchMode = AVCaptureTorchModeOff;
                break;
                
            case CvCameraFlashModeOn:
                camera.flashMode = AVCaptureFlashModeOn;
                camera.torchMode = AVCaptureTorchModeOff;
                break;
                
            case CvCameraFlashModeAuto:
                camera.flashMode = AVCaptureFlashModeAuto;
                camera.torchMode = AVCaptureTorchModeAuto;
                break;
                
            case CvCameraFlashModeAlwayOn:
                camera.flashMode = AVCaptureFlashModeOn;
                camera.torchMode = AVCaptureTorchModeOn;
                break;
                
            default:
                break;
        }
        
        [camera unlockForConfiguration];
    }
}

- (void)changeWhiteBalance:(float)temperature {
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;

//    if (temperature == 0) {
//        CMTime exposureDurationCurrent = AVCaptureExposureDurationCurrent;
//        float ISOCurrent = AVCaptureISOCurrent;
//        
//        [self changeWhiteBalanceWithMode:CvCameraWhiteBalanceModeAuto];
//        
//        if ([camera lockForConfiguration:&error]) {
//            
//            [camera setExposureModeCustomWithDuration:exposureDurationCurrent ISO:ISOCurrent completionHandler:nil];
//            
//            [camera unlockForConfiguration];
//        }
//        
//        return;
//    }
    
    AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {temperature, 0};
    AVCaptureWhiteBalanceGains whiteBlanceGains = [camera deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
    
//    NSLog(@"blueGain: %f greenGain: %f redGain: %f",
//          whiteBlanceGains.blueGain, whiteBlanceGains.greenGain, whiteBlanceGains.redGain);
    
    
    if ([camera lockForConfiguration:&error]) {
        
        if (temperature == 0) {
            if ([camera isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance]) {
                camera.whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
            }
            
        } else {
            [camera setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:whiteBlanceGains completionHandler:nil];
        }
        
        [camera unlockForConfiguration];
    }
}

- (void)changeFormat:(CvCameraFormat)newFormat {
    if (self.cameraPosition == AVCaptureDevicePositionFront) {
        return;
    }
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    int32_t height = 0;
    Float64 maxRate = 0;
    switch (newFormat) {
        case CvCameraFormat720p30:
            height = 720;
            maxRate = 30;
            self.currentCvCameraFormat = CvCameraFormat720p30;
            break;
        case CvCameraFormat1080p30:
            height = 1080;
            maxRate = 30;
            self.currentCvCameraFormat = CvCameraFormat1080p30;
            break;
        case CvCameraFormat1080p60:
            height = 1080;
            maxRate = 60;
            self.currentCvCameraFormat = CvCameraFormat1080p60;
            break;
        case CvCameraFormat4kp30:
            self.currentCvCameraFormat = CvCameraFormat4kp30;
            break;
        case CvCameraFormatSlowMotion:
            self.currentCvCameraFormat = CvCameraFormatSlowMotion;
            break;
            
        default:
            break;
    }
    
    AVFrameRateRange *slowMotionRateRange = nil;
    AVCaptureDeviceFormat *slowMotionFormat = nil;
    
    for (AVCaptureDeviceFormat *format in camera.formats) {
        if (newFormat == CvCameraFormatSlowMotion) {
            for (AVFrameRateRange *range in format.videoSupportedFrameRateRanges) {
                if (!slowMotionRateRange) {
                    slowMotionRateRange = range;
                }
                if (range.maxFrameRate > slowMotionRateRange.maxFrameRate) {
                    slowMotionRateRange = range;
                    slowMotionFormat = format;
                }
            }
            
            continue;
        }
        
        if (CMVideoFormatDescriptionGetDimensions(format.formatDescription).height == height &&
            CMFormatDescriptionGetMediaSubType(format.formatDescription) == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {

            
            for (AVFrameRateRange *range in format.videoSupportedFrameRateRanges) {
                if (range.maxFrameRate == maxRate) {
                    
                    NSError *error;
                    
                    if ([camera lockForConfiguration:&error]) {
                        
                        camera.activeFormat = format;
                        camera.activeVideoMinFrameDuration = CMTimeMake(1, range.maxFrameRate);
                        camera.activeVideoMaxFrameDuration = CMTimeMake(1, range.maxFrameRate);
                        
                        [camera unlockForConfiguration];
                        
                        self.currentFrameRateRange = range;
                        
                        NSLog(@"%@", camera.activeFormat);
                        break;
                    }
                }
            }

        }
    }
    
    self.currentFormat = camera.activeFormat;
    self.width = CMVideoFormatDescriptionGetDimensions(self.currentFormat.formatDescription).width;
    self.height = CMVideoFormatDescriptionGetDimensions(self.currentFormat.formatDescription).height;
    
    if (newFormat == CvCameraFormatSlowMotion) {
//        NSLog(@"slowMotionFormat: %@ slowMotionRateRange: %@", slowMotionFormat, slowMotionRateRange);
        
//        NSError *error;
//        
//        if ([camera lockForConfiguration:&error]) {
//            
//            camera.activeFormat = slowMotionFormat;
//            camera.activeVideoMinFrameDuration = CMTimeMake(1, slowMotionRateRange.maxFrameRate);
//            camera.activeVideoMaxFrameDuration = CMTimeMake(1, slowMotionRateRange.maxFrameRate);
//            
//            [camera unlockForConfiguration];
//        }
//        
//        NSLog(@"%@", camera.activeFormat);
        
        self.currentFrameRateRange = slowMotionRateRange;
        
        [self initCaptureWithSlowMotion];
    }
    
}

- (void)changeWhiteBalanceWithMode:(CvCameraWhiteBalanceMode)whitebalanceMode {

    if (whitebalanceMode == CvCameraWhiteBalanceModeAuto) {
        AVCaptureDevice *camera = gpuCamera.inputCamera;
        
        NSError *error;
        
        if ([camera lockForConfiguration:&error]) {
            
            if ([camera isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance]) {
                camera.whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
            }
            
            [camera unlockForConfiguration];
        }
    } else {
        [self changeWhiteBalance:whitebalanceMode];
    }
}

- (void)changeISO:(float)iso {
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
//    if (iso == 0) {
//        
//        CMTime exposureDurationCurrent = AVCaptureExposureDurationCurrent;
//        AVCaptureWhiteBalanceGains whiteBalanceGainsCurrent = camera.deviceWhiteBalanceGains;
//        
//        [self changeExposureMode:AVCaptureExposureModeAutoExpose];
//        
//        if ([camera lockForConfiguration:&error]) {
//            
//            [camera setExposureModeCustomWithDuration:exposureDurationCurrent ISO:AVCaptureISOCurrent completionHandler:nil];
//            [camera setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:whiteBalanceGainsCurrent completionHandler:nil];
//            
//            [camera unlockForConfiguration];
//        }
//        
//        return;
//    }
    
    if ([camera lockForConfiguration:&error]) {
        if (iso == 0) {
            if ([camera isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
                camera.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            }
            
        } else {
            [camera setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:iso completionHandler:nil];
        }
        
        [camera unlockForConfiguration];
    }
}

- (void)changeShutter:(float)value {
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
//    if (value == 0) {
//        float ISOCurrent = AVCaptureISOCurrent;
//        AVCaptureWhiteBalanceGains whiteBalanceGainsCurrent = camera.deviceWhiteBalanceGains;
//        
//        [self changeExposureMode:AVCaptureExposureModeAutoExpose];
//        
//        if ([camera lockForConfiguration:&error]) {
//            
//            [camera setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:ISOCurrent completionHandler:nil];
//            [camera setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:whiteBalanceGainsCurrent completionHandler:nil];
//            
//            [camera unlockForConfiguration];
//        }
//        
//        return;
//    }
    
//    double p = pow(value, 5);
//    
//    double minDurationSeconds = 1.0/500; //CMTimeGetSeconds(camera.activeVideoMinFrameDuration);
//    double maxDurationSeconds = 1.0/10; //CMTimeGetSeconds(camera.activeVideoMaxFrameDuration);
//    
//    double newDurationSeconds = p * ( maxDurationSeconds - minDurationSeconds ) + minDurationSeconds;
//    
//    NSLog(@"%f %f %f", minDurationSeconds, maxDurationSeconds, newDurationSeconds);
    
    if ([camera lockForConfiguration:&error]) {
        
        if (value == 0) {
            if ([camera isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
                camera.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            }
        } else {
            [camera setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(1.0/value, 1000*1000*1000) ISO:AVCaptureISOCurrent completionHandler:nil];
        }
        
        [camera unlockForConfiguration];
    }
}

- (void)changeExposureMode:(AVCaptureExposureMode)newMode {
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
    if ([camera lockForConfiguration:&error]) {
        
        camera.exposureMode = newMode;
        
        [camera unlockForConfiguration];
    }
}

- (void)changeToDefault {
    AVCaptureDevice *camera = gpuCamera.inputCamera;
    
    NSError *error;
    
    if ([camera lockForConfiguration:&error]) {
        
        if ([camera isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
            camera.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
        }
        
        if ([camera isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance]) {
            camera.whiteBalanceMode = AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance;
        }
        
        [camera unlockForConfiguration];
    }
}

- (void)startTimeLapseWithFinish:(void (^)(NSURL *url))finish {

    self.timeLapseCapture = [[TimeLapseCapture alloc] initWithShutter:self.timeLapseShutter duration:self.timeLapseDuration];
    self.timeLapseCapture.width = self.width;
    self.timeLapseCapture.height = self.height;
    self.recording = YES;

    [self.timeLapseCapture startWithFinish:^(NSURL *url) {
        self.recording = NO;
        finish(url);
    }];
}

- (void)terminateTimeLapseWithFinish:(void (^)(NSURL *url))finish {
    [self.timeLapseCapture terminate:^(NSURL *url) {
        self.recording = NO;
        finish(url);
    }];
}

- (void)startTimeLapseWithInterval:(float)interval duration:(float)duration andFinish:(void (^)(NSURL *url))finish {

    self.timeLapseShutter = interval;
    self.timeLapseDuration = duration;
        
    self.timeLapseCapture = [[TimeLapseCapture alloc] initWithShutter:self.timeLapseShutter duration:self.timeLapseDuration];
    self.timeLapseCapture.width = self.width;
    self.timeLapseCapture.height = self.height;

    self.recording = YES;
    
    [self.timeLapseCapture startWithFinish:^(NSURL *url) {
        finish(url);
        
        self.recording = NO;
    }];
}

- (NSArray *)isoRange {
    if (!_isoRange) {
        NSMutableArray *range = [NSMutableArray array];
        
        AVCaptureDevice *camera = gpuCamera.inputCamera;
        
        float minISO = camera.activeFormat.minISO;
        float maxISO = camera.activeFormat.maxISO;
        
        [range addObject:@(0)];
        for (int i = 0; i <= 10; i++) {
            float iso = (maxISO - minISO) / 10 * i + minISO;
            [range addObject:@(iso)];
        }
        
        _isoRange = range;
    }
    
    return _isoRange;
}

- (NSArray *)shutterRange {
    if (!_shutterRange) {
        NSMutableArray *range = [NSMutableArray array];
        
        float min = 10.f;
        float max = 500.f;
        
        [range addObject:@(0)];
        for (int i = 0; i <= 10; i++) {
            float value = (10 * max) / ((1/min - 1/ max) * i * max + 10);
            [range addObject:@(value)];
        }
        
        _shutterRange = range;
    }
    
    return _shutterRange;
}

- (NSArray *)whiteBalanceRange {
    if (!_whiteBalanceRange) {
        NSMutableArray *range = [NSMutableArray array];
        
        AVCaptureDevice *camera = gpuCamera.inputCamera;
        
        AVCaptureWhiteBalanceTemperatureAndTintValues minWhiteBalanceTemperatureAndTintValues =
        [camera temperatureAndTintValuesForDeviceWhiteBalanceGains:{1.f, 1.f, camera.maxWhiteBalanceGain}];
        
        AVCaptureWhiteBalanceTemperatureAndTintValues maxWhiteBalanceTemperatureAndTintValues =
        [camera temperatureAndTintValuesForDeviceWhiteBalanceGains:{camera.maxWhiteBalanceGain, 1.f, 1.f}];
        
        float min = minWhiteBalanceTemperatureAndTintValues.temperature;
        float max = MIN(8000, maxWhiteBalanceTemperatureAndTintValues.temperature);
        
        [range addObject:@(0)];
        for (int i = 0; i <= 10; i++) {
            float value = (max - min) / 10 * i + min;
            [range addObject:@(value)];
        }
        
        _whiteBalanceRange = range;
    }
    
    return _whiteBalanceRange;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detectedFaces:)]) {
        NSMutableArray *faceRects = [NSMutableArray array];
        
        for (AVMetadataObject *metadata in metadataObjects) {
            
            if ([metadata.type isEqualToString:AVMetadataObjectTypeFace]) {
                
                AVMetadataFaceObject *faceMetadata = (AVMetadataFaceObject *)metadata;
                
                CGRect rect = CGRectMake((int)(faceMetadata.bounds.origin.x*self.captureVideoPreviewLayer.bounds.size.width),
                                         (int)(faceMetadata.bounds.origin.y*self.captureVideoPreviewLayer.bounds.size.height),
                                         (int)(faceMetadata.bounds.size.width*self.captureVideoPreviewLayer.bounds.size.width),
                                         (int)(faceMetadata.bounds.size.height*self.captureVideoPreviewLayer.bounds.size.height));
                
//                NSDictionary *faceInfo = @{@"faceId" : [NSNumber numberWithInteger:faceMetadata.faceID], @"rect" : [NSValue valueWithCGRect:rect]};
                [faceRects addObject:[NSValue valueWithCGRect:rect]];
            }
        }
        
        [self.delegate detectedFaces:faceRects];
    }
}

@end
