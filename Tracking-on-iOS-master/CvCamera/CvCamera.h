//
//  CvCamera.h
//  AVCaptureSession
//
//  Created by qq on 21/10/2016.
//  Copyright © 2016 GitHub. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GPUImage.h"

typedef NS_ENUM(NSInteger, CvCameraFlashMode) {
    CvCameraFlashModeOff  = 0,
    CvCameraFlashModeOn   = 1,
    CvCameraFlashModeAuto = 2,
    CvCameraFlashModeAlwayOn = 3
};

typedef NS_ENUM(NSInteger, CvCameraWhiteBalanceMode) {
    CvCameraWhiteBalanceModeAuto = 0,
    CvCameraWhiteBalanceModeSunny = 6400,
    CvCameraWhiteBalanceModeCloudy = 6800,
    CvCameraWhiteBalanceModeIncandescent = 2500,
    CvCameraWhiteBalanceModeFluorescent = 4900
};

typedef NS_ENUM(NSInteger, CvCameraFormat) {
    CvCameraFormat720p30 = 0,
    CvCameraFormat1080p30 = 1,
    CvCameraFormat1080p60 = 2,
    CvCameraFormat4kp30 = 3,
    CvCameraFormatSlowMotion = 4
};

#ifdef __cplusplus

#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>

#endif

@protocol CvCameraDelegate <NSObject>

@optional
#ifdef __cplusplus
- (void)processImage:(cv::Mat &)matImage;
#endif
- (void)detectedFaces:(NSArray *)faceRects;

@end

@class AVCaptureVideoPreviewLayer;
@interface CvCamera : NSObject

- (instancetype)initWithSessionPreset:(NSString *)sessionPreset cameraPosition:(AVCaptureDevicePosition)cameraPosition orientation:(UIInterfaceOrientation)orientation;

- (void)start;
- (void)stop;

- (void)startRecord;
- (void)stopRecord:(void (^)(NSURL *videoTmpUrl))finish;

- (void)switchCamera;

- (void)changeOrientation:(UIInterfaceOrientation)orientation;

- (void)takePhoto:(void (^)(UIImage *image))success;

- (void)zoomCamera:(CGFloat)value;

- (void)switchFilter:(GPUImageOutput<GPUImageInput> *)newFilter;

- (void)cameraFlashMode:(CvCameraFlashMode)flashMode;

- (void)changeWhiteBalance:(float)temperature;

- (void)changeWhiteBalanceWithMode:(CvCameraWhiteBalanceMode)whitebalanceMode;

- (void)changeFormat:(CvCameraFormat)newFormat;

- (void)changeISO:(float)iso;

- (void)changeShutter:(float)value;

- (void)changeExposureMode:(AVCaptureExposureMode)newMode;

- (void)changeToDefault;

- (void)startTimeLapseWithFinish:(void (^)(NSURL *url))finish;

- (void)terminateTimeLapseWithFinish:(void (^)(NSURL *url))finish;

@property (nonatomic, weak) id<CvCameraDelegate> delegate;

@property (nonatomic, strong) NSString *captureSessionPreset;
@property (nonatomic, assign) AVCaptureDevicePosition cameraPosition;
@property (nonatomic, assign) UIInterfaceOrientation previewOrientation;

@property (nonatomic, assign) CvCameraFormat currentCvCameraFormat;

// preview
//@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;

@property (nonatomic, strong) UIView *cameraPreview;

@property (nonatomic, assign) BOOL recording;

@property (nonatomic, strong) NSURL *assetUrl;

@property (nonatomic, assign) BOOL startEncode;

@property (nonatomic, strong) NSArray *isoRange;

@property (nonatomic, strong) NSArray *shutterRange;

@property (nonatomic, strong) NSArray *whiteBalanceRange;

@property (nonatomic, assign) Float32 timeLapseShutter;
@property (nonatomic, assign) NSUInteger timeLapseDuration;

@end
