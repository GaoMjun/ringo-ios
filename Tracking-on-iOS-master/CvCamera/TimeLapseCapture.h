//
//  TimeLapseCapture.h
//  Ringo-iOS
//
//  Created by qq on 6/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TimeLapseCapture : NSObject

@property (nonatomic, assign) Float32 shutter;
@property (nonatomic, assign) NSUInteger duration;

@property(nonatomic, assign) NSUInteger height;
@property(nonatomic, assign) NSUInteger width;

- (instancetype)initWithShutter:(Float32)shutter duration:(NSUInteger)duration;

- (void)startWithFinish:(void(^)(NSURL *url))finish;
- (void)terminate:(void(^)(NSURL *url))finish;

- (void)start;
- (void)stop;

- (void)feedImage:(CVPixelBufferRef)imageBuffer;

@end
