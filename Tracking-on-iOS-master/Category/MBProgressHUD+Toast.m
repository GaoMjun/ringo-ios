//
//  MBProgressHUD+Toast.m
//  
//
//  Created by qq on 4/19/16.
//
//

#import "MBProgressHUD+Toast.h"

@implementation MBProgressHUD (Toast)

+ (void) toast:(UIView *)view toastStr:(NSString *)toastStr {
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = toastStr;
        hud.labelFont = [UIFont systemFontOfSize:14];
        [hud hide:YES afterDelay:1];
    });
}

+ (void) toast:(UIView *)view toastStr:(NSString *)toastStr xOffset:(float)xOffset yOffset:(float)yOffset {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = toastStr;
    hud.xOffset = xOffset;
    hud.yOffset = yOffset;
    [hud hide:YES afterDelay:1];
}

@end
