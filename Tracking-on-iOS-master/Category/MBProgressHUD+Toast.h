//
//  MBProgressHUD+Toast.h
//  
//
//  Created by qq on 4/19/16.
//
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Toast)

+ (void) toast:(UIView *)view toastStr:(NSString *)toastStr;

+ (void) toast:(UIView *)view toastStr:(NSString *)toastStr xOffset:(float)xOffset yOffset:(float)yOffset;

@end
