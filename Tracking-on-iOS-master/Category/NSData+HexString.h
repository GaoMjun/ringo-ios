//
//  NSData+HexString.h
//  Tracking
//
//  Created by qq on 29/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (HexString)

- (NSString *)hexString;

@end
