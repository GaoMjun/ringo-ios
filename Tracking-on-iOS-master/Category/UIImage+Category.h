//
//  UIImage+Category.h
//  Ringo-iOS
//
//  Created by qq on 19/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Category)

- (BOOL)isEqualToImage:(UIImage *)image;

@end
