//
//  UIImage+Category.m
//  Ringo-iOS
//
//  Created by qq on 19/6/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "UIImage+Category.h"

@implementation UIImage (Category)

- (BOOL)isEqualToImage:(UIImage *)image {
    NSData *data1 = UIImagePNGRepresentation(self);
    NSData *data2 = UIImagePNGRepresentation(image);
    
    return [data1 isEqual:data2];
}

@end
