//
//  UIView+GCLibrary.h
//  Tracking
//
//  Created by qq on 13/9/2016.
//  Copyright © 2016 FloodSurge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GCLibrary)

@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;

@end
