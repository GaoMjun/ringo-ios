//
//  FrontCameraCaptureModesView.m
//  Ringo-iOS
//
//  Created by qq on 27/7/2017.
//  Copyright © 2017 FloodSurge. All rights reserved.
//

#import "FrontCameraCaptureModesView.h"
#import "Masonry.h"

@interface FrontCameraCaptureModesView ()

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIView *singleModesView;

@end

@implementation FrontCameraCaptureModesView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self addSubview:self.singleModesView];
    [self.singleModesView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (IBAction)switchToSingleCaptureModes:(UIButton *)sender {
}

- (IBAction)switchToLongExpodureCaptureModes:(UIButton *)sender {
}

@end
